#!/bin/env python3
'''
Main tests of the Ithildin Python module
'''
import hashlib
import ithildin as ith
import matplotlib.pyplot as plt
import numpy as np
import os
import shutil
import tempfile
import unittest
import zipfile
import re

SIMSERIES = 'spiral'
SERIALNR = 0
STEM = f'{SIMSERIES}_{SERIALNR}'

class SimDataTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.results_path = tempfile.mkdtemp()
        os.makedirs(self.results_path, exist_ok=True)
        with zipfile.ZipFile(f'{STEM}.zip', 'r') as zip:
            zip.extractall(self.results_path)
        self.sd = ith.SimData.from_stem(f'{self.results_path}/{STEM}')

    def tearDown(self) -> None:
        shutil.rmtree(self.results_path)

    def test_log_get(self):
        log = self.sd.log
        log.stem = log.stem
        self.assertEqual(log.stem, STEM)
        self.assertEqual(log.path, f'{self.results_path}/{STEM}_log.yaml')
        self.assertEqual(log.version, 3)
        self.assertEqual(log.nproc, 1)
        self.assertEqual(len(log.inhoms), 1)
        self.assertEqual(log.inhoms[0]['Type'], 'no-flux BC')
        self.assertIsInstance(log.geometry, dict)
        self.assertEqual(log.geomtype, 'Isotropic')
        self.assertEqual(log.geomfile, None)
        self.assertEqual(log.boundary_condition, 'no-flux as inhomogeneity')
        self.assertEqual(len(log.sources), 2)
        np.testing.assert_array_almost_equal(log.sourcetimes, [0.0, 32.9])
        self.assertEqual(log.sourcetypes, ['Rectangular area', 'Rectangular area'])
        self.assertEqual(log.model, log.models[0])
        self.assertEqual(log.varnames, ['u', 'v'])
        self.assertEqual(log.nvars, 2)
        self.assertEqual(log.domainsize, (1, 60, 60))
        self.assertEqual(log.shape, (16, 1, 60, 60))
        self.assertEqual(log.shape_space, (1, 60, 60))
        self.assertEqual(log.nframes, 16)
        self.assertEqual(log.nsteps, 1801)
        self.assertEqual(log.tfirst, 0)
        self.assertAlmostEqual(log.tend, 60.0332733)
        self.assertAlmostEqual(log.tframe, 4)
        self.assertAlmostEqual(log.dt, 0.0333333)
        np.testing.assert_array_almost_equal(log.dzyx, [1, 2, 2])
        np.testing.assert_array_almost_equal(log.deltas, [4, 1, 2, 2])
        np.testing.assert_array_almost_equal(log.origin, [0, 0, 0, 0])
        np.testing.assert_array_almost_equal(log.physical_size, [64, 1, 120, 120])
        self.assertEqual(log.tiplinevars, [0, 1])
        self.assertEqual(log.tiplineisovalues, [0.5, 0.5])
        self.assertEqual(log.diffusmain, [1, 1, 1])
        self.assertEqual(log.diffusmat, [20, 0])

    def test_log_set(self):
        log = ith.Log()
        log.full_data['Ithildin log version'] = 2
        log.ndim = 2
        log.nproc = 1
        log.add_model({})
        log.nvars = 2
        log.modeltype = 'example model'
        log.diffusmat = [10, 0]
        log.model['Variable names'] = ['u', 'v']
        log.diffusmain = [1, 1]
        log.dzyx = (1., 1., 1.)
        log.domainsize = (1, 20, 20)
        log.tframe = 1.
        log.dt = 0.1

    def test_simdata(self):
        sd = self.sd
        log = sd.log
        str(sd)
        sd.check_all_shapes()
        self.assertEqual(sd.dtype, np.float32)
        self.assertEqual(sd.dim, 2)
        self.assertEqual(sd.shape, log.shape)
        x = sd.linspace(-1)
        self.assertAlmostEqual(x[1] - x[0], log.deltas[-1])
        t = sd.times
        self.assertAlmostEqual(t[1] - t[0], log.tframe)
        t, x = sd.meshgrid(0, -1)
        self.assertEqual(t.shape, x.shape)
        self.assertEqual(t.shape[0], log.shape[-1])
        self.assertEqual(t.shape[1], log.nframes)
        self.assertAlmostEqual(t[0,1] - t[0,0], log.tframe)
        self.assertAlmostEqual(x[1,0] - x[0,0], log.deltas[-1])

    def test_simdata_plot(self):
        fig, ax = plt.subplots()
        self.sd.plot('u', it=-1, ax=ax)
        fig.savefig(f'{self.results_path}/test.png')
        plt.close(fig)

        fig, ax = plt.subplots()
        self.sd.plot('u', iz=0, iy=0, ix=0, ax=ax)
        plt.close(fig)

        fig, ax = plt.subplots()
        self.assertRaises(AssertionError, lambda: self.sd.plot('u', ax=ax))
        plt.close(fig)

        fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))
        self.sd.plot('u', ax=ax)
        plt.close(fig)

    def test_simdata_write(self):
        self.sd.to_stem(f'{self.results_path}/write{STEM}')

    # def test_xdmf(self):
    #     self.sd.to_xdmf(f'{self.results_path}/xdmf{STEM}')
    #     xdmf = ith.xdmf.xdmf_document(ith.xdmf.xdmf_simdata(self.sd))
    #     xdmf = re.sub(f'.*{self.results_path}/*', '', xdmf)
    #     self.assertEqual(hashlib.md5(xdmf.encode()).hexdigest(), 'a15a1754f7e7291e607736f1b54daf05')

    def test_pulse_phase_pd(self):
        sd = self.sd
        t_phase = 30
        u_iso = 0.5
        u = sd.vars['u']
        t = np.array(sd.times)
        x, y = sd.meshgrid(-1, -2)

        dur, tact, tdeact = ith.pulse.duration(u=u, u_iso=u_iso, t=t)
        np.testing.assert_allclose(tact[-1,0], ith.pulse.time_arrival_frame(u=u, u_iso=u_iso, t=t).squeeze())

        tact_two = ith.pulse.time_arrival_two(u=u, u_iso=(u_iso, u_iso+0.2), t=t)
        tact_two[~np.isfinite(tact_two)] = np.nan
        self.assertAlmostEqual(np.nanmean(tact_two[-1,0]), 31.246247915508615)
        self.assertAlmostEqual(np.nanstd(tact_two[-1,0]), 19.1694400834961)

        apd = np.where(np.isfinite(dur)*(dur > 0), dur, np.nan)
        self.assertAlmostEqual(np.nanmean(apd[-1,0]), 16.662299854439592)
        self.assertAlmostEqual(np.nanstd(apd[-1,0]), 8.23231230516122)

        di = np.where(np.isfinite(dur)*(dur < 0), -dur, np.nan)
        self.assertAlmostEqual(np.nanmean(di[-1,0]), 23.84037558685446)
        self.assertAlmostEqual(np.nanstd(di[-1,0]), 1.7796090328043235)

        lat = t.reshape((-1, 1, 1, 1)) - tact
        lat_ = np.where(np.isfinite(lat), lat, np.nan)
        self.assertAlmostEqual(np.nanmean(lat_), 19.410420108040537)
        self.assertAlmostEqual(np.nanstd(lat_), 14.117373586205373)

        phi = ith.phase.phase_elapsed(u, dur=t_phase, time_elapsed=lat)
        self.assertAlmostEqual(np.nanmean(phi), 3.321068904413672)
        self.assertAlmostEqual(np.nanstd(phi), 2.5822998570989424)
        np.testing.assert_allclose(phi, sd.phase('arr', dur=t_phase, u_iso=u_iso))

        rho = sd.phase_defect('pc', phi)
        self.assertAlmostEqual(np.nanmean(rho), 0.016727201259245886)
        self.assertAlmostEqual(np.nanstd(rho), 0.07291096263757699)

        sigma = sd.phase_edge_defect('cos', phi)
        self.assertEqual(len(sigma), 2)
        self.assertAlmostEqual(np.nanmean(sigma[0]), 0.0033174933395265904)
        self.assertAlmostEqual(np.nanstd(sigma[0]), 0.04329598308616376)
        self.assertAlmostEqual(np.nanmean(sigma[1]), 0.012650029998874431)
        self.assertAlmostEqual(np.nanstd(sigma[1]), 0.08016806315370154)

        cv, _ = ith.pulse.velocity(t_act=tact[-1,0], x=x, y=y, dist=10)
        self.assertAlmostEqual(np.nanmean(cv), 6.096141414358789)
        self.assertAlmostEqual(np.nanstd(cv), 0.19240827935188468)

if __name__ == '__main__':
    unittest.main()
