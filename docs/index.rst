.. Ithildin Python Module documentation master file, created by
   sphinx-quickstart on Tue Feb  2 14:26:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation of the Ithildin Python Module
===========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   intro
   cli
   api/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
