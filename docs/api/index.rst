API Documentation
=================

.. automodule:: ithildin
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   log
   varfile
   simdata
   pulse
   phase
   defect
   topology
   points
   filament
   pdl
   plot
   xdmf
   graph
