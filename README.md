# Python Module for Ithildin

Python code to analyse data from [anisotropic reaction-diffusion solver `ithildin`](https://gitlab.com/heartkor/ithildin).

Up-to-date documentation to get started can be found at:
- https://heartkor.gitlab.io/py_ithildin/ (public version)
- https://heartkor.gitlab.io/dev/py_ithildin/ (development version)
- https://heartkor.pages.gitlab.kuleuven.be/py_ithildin/ (development version)
- or [in this repository](docs/intro.rst).

The official locations of this repository are:
- https://gitlab.com/heartkor/py_ithildin/ (public version)
- https://gitlab.com/heartkor/dev/py_ithildin/ (development version)
- https://gitlab.kuleuven.be/heartkor/py_ithildin (development version)

## Citation

Please cite these papers when using the implementation:

- **first release, version 0.4.0**:
  Kabus, D., Arno, L., Leenknegt, L., Panfilov, A. V., & Dierckx, H. (2022).
  Numerical methods for the detection of phase defect structures in excitable
  media. PLOS One, 17(7), e0271351.
  https://doi.org/10.1371/journal.pone.0271351

- **second release, version 0.6.1:**
  Kabus, D., De Coster, T., de Vries, A. A., Pijnappels, D. A., and
  Dierckx, H. (2024). Fast creation of data-driven low-order predictive
  cardiac tissue excitation models from recorded activation patterns.
  Computers in Biology and Medicine, 107949.
  https://doi.org/10.1016/j.compbiomed.2024.107949

BibTeX entries:

```{bib}
@article{kabus2022numerical,
  title={Numerical methods for the detection of phase defect structures in excitable media},
  author={Kabus, Desmond and Arno, Louise and Leenknegt, Lore and Panfilov, Alexander V and Dierckx, Hans},
  journal={PLOS One},
  volume={17},
  number={7},
  pages={e0271351},
  year={2022},
  publisher={Public Library of Science San Francisco, CA USA}
}
@article{kabus2024fast,
  doi={10.1016/j.compbiomed.2024.107949},
  url={https://doi.org/10.1016/j.compbiomed.2024.107949},
  title={Fast creation of data-driven low-order predictive cardiac tissue excitation models from recorded activation patterns},
  author={Kabus, Desmond and De Coster, Tim and de Vries, Antoine AF and Pijnappels, Dani{\"e}l A and Dierckx, Hans},
  journal={Computers in Biology and Medicine},
  pages={107949},
  year={2024},
  publisher={Elsevier}
}
```
