#!/bin/env python3
"""
Find and analyse the trajectories of PDLs
"""
import csv
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
import yaml
from matplotlib import cm

FORCE_CALC = True

stem = "results/burst_61"
# stem = "results/burst_43"
# stem = "results/burst_57"
sd = ith.SimData.from_stem(stem)
Nt, Nz, Ny, Nx = sd.shape
dt, dz, dy, dx = sd.deltas
t = sd.linspace(0)
x = sd.linspace(3)
y = sd.linspace(2)
u = sd.vars["u"]

print(sd.log.modeltype)
if "Smooth Karma" in sd.log.modeltype:
    dur = 80 # SmooKa model
else:
    dur = 200 # FK model

if FORCE_CALC or "phiarr" not in sd.vars:
    sd.vars["phiarr"] = sd.phase("arr", dur=dur)
    np.save(stem+"_phiarr.npy", sd.vars["phiarr"])
phi = sd.vars["phiarr"]

if FORCE_CALC or "phiarrrhopc" not in sd.vars:
    sd.vars["phiarrrhopc"] = sd.phase_defect("pc", phi)
    np.save(stem+"_phiarrrhopc.npy", sd.vars["phiarrrhopc"])
rho = sd.vars["phiarrrhopc"]

rho_iso = 0.1
# it_ = Nt - 80
it_ = 0

# === PLOT PSs ===
fig, ax = plt.subplots(dpi=180)
ax.set_xlim(x[0], x[-1])
ax.set_ylim(y[0], y[-1])
cmap = plt.get_cmap("viridis")
sm = cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=t[0], vmax=t[-1]))
cbar = fig.colorbar(sm, ax=ax)

for fil in sd.filaments:
    for point in fil.points:
        ax.plot(*point[:2], c=cmap(fil.time/t[-1]), zorder=fil.time, marker='.')

plt.savefig(f"{stem}_filaments.pdf")

# === FIND PDLS ===
pdls = []
for it in np.arange(it_, Nt):
    print(f'Finding PDLs in frame {it} of {Nt}...', end='\r')
    pdls_ = ith.pdl.pdls_ridge(rho[it,0], x, y, dx=dx, dy=dy, threshold=rho_iso, time=t[it])
    for pdl in pdls_:
        if len(pdl) > 5:
            pdl.it = it
            pdls.append(pdl)
print()

trajectories = ith.pdl.find_trajectories(pdls, maxdist=10)
trajectories.sort(key=lambda t: -len(t))

print(f"found {len(trajectories)} trajectories, lengths ranging from {len(trajectories[-1])} to {len(trajectories[0])}")

# === PLOT PDLS ===
fig, ax = plt.subplots(dpi=180)
ax.set_xlim(x[0], x[-1])
ax.set_ylim(y[0], y[-1])
cmap = plt.get_cmap("viridis")
sm = cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=t[0], vmax=t[-1]))
cbar = fig.colorbar(sm, ax=ax)

for pdl in pdls:
    ax.plot(*pdl.points.T[:2], c=cmap(pdl.time/t[-1]), zorder=pdl.time)

plt.savefig(f"{stem}_pdls.pdf")
