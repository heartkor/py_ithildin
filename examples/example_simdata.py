#!/bin/env python3
"""
Example script to test ithildin.simdata
"""

import ithildin as ith
import matplotlib.pyplot as plt

# stem = "results/pdl2D_6"
# stem = "results/PDL_12"
stem = "results/PDS_21"
# stem = "results/edu_95"
# stem = "results/phase_68"

# read simulation results
simdata = ith.SimData.from_stem(stem)
Nt, Nz, Ny, Nx = simdata.shape

# plot inhom at given z
if "inhom" in simdata.vars:
    simdata.plot("inhom", iz=Nz//2)
    plt.show()

# plot u at given point
simdata.plot("u", ix=Nx//2, iy=Ny//2, iz=Nz//2)
plt.show()

# plot u at given time and z
simdata.plot("u", it=Nt//2, iz=Nz//2)
plt.show()

# make a 2d movie
fig = plt.figure(dpi=200)
ax = fig.add_subplot()
kwplots = [dict(it=i, iz=Nz//2, vmin=0., vmax=1.) for i in range(simdata.shape[0])] # movie over time
# kwplots = [dict(it=Nt//2, iz=i) for i in range(simdata.shape[1])] # scan through space
aplots = [["u"]]*len(kwplots)
ith.plot.movie(fig, ax, simdata.plot, aplots, kwplots)

if simdata.dim == 3:
    plt.set_cmap("gist_gray")

    # plot u and filaments at given time
    fig = plt.figure(dpi=200)
    ax = fig.add_subplot(projection="3d")
    ith.plot.plot3d_var(simdata, "u", it=Nt//2, ax=ax)
    plt.show()

    # make a 3d movie
    fig = plt.figure(dpi=200)
    ax = fig.add_subplot(projection="3d")
    kwplots = [dict(it=i) for i in range(simdata.shape[0])] # movie over time
    aplots = [[simdata, "u"]]*len(kwplots)
    ith.plot.movie(fig, ax, ith.plot.plot3d_var, aplots, kwplots)
