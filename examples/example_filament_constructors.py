#!/bin/env python3
'''
Test constructor for filament
'''
import numpy as np
import ithildin as ith

# empty filament, faces optional
print(1)
try:
    f = ith.Filament()
    print(f)
    print(f.faces)
except Exception as e:
    print(e.__class__.__name__, e)

# this one should be illegal
print(2)
try:
    f = ith.Filament([], faces=[])
    print(f)
    print(f.faces)
except Exception as e:
    print(e.__class__.__name__, e)

# empty filament
print(3)
try:
    f = ith.Filament([[]], faces=[0])
    print(f)
    print(f.faces)
except Exception as e:
    print(e.__class__.__name__, e)

# normal
print(4)
try:
    f = ith.Filament(np.zeros((0, 2)), res=np.zeros((2,)), time=0.1, ordered=True, faces=[])
    print(f)
    print(f.faces)
except Exception as e:
    print(e.__class__.__name__, e)

# test
print(5)
try:
    f = ith.Filament(np.zeros((0, 2)), faces=np.zeros((0,)))
    print(f)
    print(f.faces)
except Exception as e:
    print(e.__class__.__name__, e)

# empty filament, faces optional
print(6)
try:
    f = ith.Filament(np.zeros((0, 10)))
    print(f)
    print(f.faces)
except Exception as e:
    print(e.__class__.__name__, e)
