#!/bin/env python3
"""
Find and analyse the trajectories of PDLs
"""
import csv
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
from matplotlib import cm

sd = ith.SimData.from_stem("results/pdl-detection/optical_20200204114234")
# sd = ith.SimData.from_stem("results/pdl-detection/phase_68")
# sd = ith.SimData.from_stem("results/smooka_3")
Nt, Nz, Ny, Nx = sd.shape
dt, dz, dy, dx = sd.deltas
it_ = Nt - 80
# it_ = Nt//2
t = sd.linspace(0)
x = sd.linspace(3)
y = sd.linspace(2)
u = sd.vars["u"]
phi = sd.vars["phi"] = sd.vars["phiarr"] if "phiarr" in sd.vars else sd.phase("arr", dur=60)
phi_min = 1e-20
phi_max = 5.5
active = sd.vars["active"] = phi < phi_max
rho = sd.vars["rho"] = np.where(sd.mask, sd.vars["phiarrrhopc"] if "phiarrrhopc" in sd.vars else sd.phase_defect("pc", phi), 0)
rho_iso = 0.1

# === FIND PDLS AND TRAJECTORIES ===
pdls = []
for it in np.arange(it_, Nt):
    pdls_ = ith.pdl.pdls_ridge(rho[it,0], x, y, dx=dx, dy=dy, threshold=rho_iso, time=t[it])
    for pdl in pdls_:
        if len(pdl) > 5:
            pdl.it = it
            pdls.append(pdl)

trajectories = ith.pdl.find_trajectories(pdls, maxdist=10)
trajectories.sort(key=lambda t: -len(t))

print(f"found {len(trajectories)} trajectories, lengths ranging from {len(trajectories[-1])} to {len(trajectories[0])}")

# === CALCULATE PROPRETIES OF PDLS ===
for trajectory in trajectories:
    print(f"duration {len(trajectory)}, lengths = {[len(pdl) for pdl in trajectory]}")
    for pdl in trajectory:
        it = pdl.it = min([Nt-1, int(pdl.time/dt + 0.2)])
        n = pdl.n = len(pdl)
        pdl.q = pdl.l = 0
        # q = pdl.q = pdl.charge(phi[it,0])
        l = pdl.l = pdl.arc_length()
        beta = pdl.beta = pdl.angle()

# === PLOT ANGLE AND LENGTH OVER TIME ===
if True:
    for i, trajectory in enumerate(trajectories[:3]):
        times = np.array([p.time for p in trajectory])
        lengths = np.array([p.l for p in trajectory])
        # betas = np.unwrap([p.beta*2 for p in trajectory])/2
        betas = np.array([p.beta for p in trajectory])
        means = np.array([np.nanmean(p.points, axis=0) for p in trajectory])

        import matplotlib.pyplot as plt
        from matplotlib.path import Path
        import matplotlib.patches as patches

        codes = [
            Path.MOVETO,
            *((len(means)-1))*[Path.CURVE3],
        ]

        path = Path(means, codes)

        # fig, ax = plt.subplots()
        # patch = patches.PathPatch(path, fill=False, lw=2)
        # plt.plot(*means.T, "b.")
        # ax.add_patch(patch)
        # mins = np.nanmin(means, axis=0) - 0.5
        # maxs = np.nanmax(means, axis=0) + 0.5
        # ax.set_xlim(mins[0], maxs[0])
        # ax.set_ylim(mins[1], maxs[1])
        # plt.show()
        # sys.exit(0)

        ith.plot.colorline(*means.T, times, setlims=True)

        vels = np.roll(means, -1, axis=0) - means
        vels[-1,:] = 0

        # for (mx, my), (vx, vy), (i, _) in zip(means, vels, enumerate(betas)):
        #     b = betas[i]
        #     bx, by = 0.1*np.sin(b), 0.1*np.cos(b)
        #     plt.arrow(mx, my, bx, by)
        #     bb = np.arctan2(vx, vy)
        #     if abs(ith.phase.difference_phases(b, bb)) > 0.8*np.pi:
        #         plt.plot(mx, my, "ro")
        #         betas[i:] += np.pi
        #     else:
        #         plt.plot(mx, my, "k.")

        betas = np.arctan2(*vels.T)

        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("centre of mass of PDL over time")
        plt.show()

        betas = np.unwrap(betas)
        # betas = np.unwrap(betas*2)/2

        beta_fit = np.poly1d(np.polyfit(times, betas, deg=1))
        omega_mean = np.mean(beta_fit.deriv()(times))
        omegas = (betas[1:] - betas[:-1])/dt
        tau = 2*np.pi

        # plt.plot(times, lengths)
        # plt.xlabel("time")
        # plt.ylabel("length of PDL")
        # plt.show()

        # plt.plot(times, betas/tau)
        # plt.plot(times, betas/tau, ".-")
        ith.plot.colorline(times, betas/tau, times)
        plt.plot(times, beta_fit(times)/tau)
        plt.xlabel("time")
        plt.ylabel("angle of PDL in revolutions")
        plt.show()

        # ith.plot.periodic_plot(times, betas)
        # t = np.linspace(times[0], times[-1], 40)
        # ith.plot.periodic_plot(t, beta_fit(t))
        # plt.ylim(0, tau)
        # plt.xlabel("time")
        # plt.ylabel("angle of PDL in radians")
        # plt.show()

        # visual of line fit
        if False:
            points = [p.points for p in trajectory]
            X = [p[:,0] for p in points]
            Y = [p[:,1] for p in points]
            x0, x1 = min(np.min(x) for x in X), max(np.max(x) for x in X)
            y0, y1 = min(np.min(y) for y in Y), max(np.max(y) for y in Y)

            fig, ax = plt.subplots()
            ax.set_xlim(x0, x1)
            ax.set_ylim(y0, y1)
            ax.set_aspect(1)

            def visual_of_pdl_with_fit(x:np.ndarray, y:np.ndarray, angle:float, ax:plt.Axes):
                ret = ax.plot(x, y, "b.-")
                xm, ym = np.mean(x), np.mean(y)
                p = np.poly1d([np.tan(0.5*np.pi-angle), ym])
                ret.extend(ax.plot([x0, x1], [p(x0-xm), p(x1-xm)], "k-"))
                return ret

            ith.plot.movie(fig, ax, visual_of_pdl_with_fit, [[*p.points.T, p.beta] for p in trajectory], filename=sys.argv[0]+f".fil{i}.mp4", fps=7)

sys.exit(0)

# === WRITE TABLE OF PDL TRAJECTORIES ===
if True:
    with open(sys.argv[0]+".csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(["trajectory", "pdl", "it", "time", "charge", "num_points", "length", "angle"])
        for i, trajectory in enumerate(trajectories):
            betas = np.unwrap([p.beta*2 for p in trajectory])/2
            for a, p in zip(betas, trajectory):
                p.beta = a
            for j, pdl in enumerate(trajectory):
                writer.writerow([i, j, pdl.it, pdl.time, pdl.q, pdl.n, pdl.l, pdl.beta])

# === PLOT A MOVIE OF TRAJECTORIES ===
if True:
    pdls_per_frame = dict()
    for idx_traj, trajectory in enumerate(trajectories):
        if len(trajectory) < 3:
            continue
        for pdl in trajectory:
            pdl.idx_traj = idx_traj
            it = pdl.it = min([Nt-1, int(pdl.time/dt + 0.2)])
            if it not in pdls_per_frame:
                pdls_per_frame[it] = []
            pdls_per_frame[it].append(pdl)

    cmap_traj = cm.get_cmap("tab10")
    N_traj = len(trajectories)

    cmap_active = cm.get_cmap("binary_r")
    cmap_active.set_bad(color="black")

    charge_label = {0:"", 1:"+", -1:"—"}
    charge_colors = {0:"k", 1:"r", -1:"b"}

    def plot_pdl_trajectories(it, ax):
        ret = []
        cb, qm = sd.plot("active", it=it, ax=ax, cmap=cmap_active, vmin=-2, vmax=1)
        cb.remove()
        ret.append(qm)
        if it in pdls_per_frame:
            for pdl in pdls_per_frame[it]:
                c = cmap_traj((pdl.idx_traj % 10)/10)
                q = np.sign(pdl.q)
                ret.extend(ax.plot(*pdl.points.T, "-", c=c))
                ret.append(ax.text(*np.mean(pdl.points, axis=0), f"{pdl.idx_traj}", ha="right", va="center"))
                ret.append(ax.text(*np.mean(pdl.points, axis=0), f"{charge_label[q]}", ha="left", va="center", c=charge_colors[q]))
        return ret

    fig = plt.figure(figsize=(6,6), dpi=200)
    ax = fig.add_subplot()
    ith.plot.movie(fig, ax, plot_pdl_trajectories, [[it] for it in np.arange(it_, Nt)], filename=sys.argv[0]+".mp4")
