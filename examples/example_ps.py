#!/bin/env python3
"""
Plot wave front, wave back and phase singularity
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy.interpolate import RectBivariateSpline

sd = ith.SimData.from_stem("results/pdl-detection/phase_68")
it = -40
x = sd.linspace(3)
y = sd.linspace(2)
u = sd.vars["u"]
du = sd.vars["du"] = u - np.roll(u, 1, axis=0)
active = sd.vars["active"] = u > 0.8

def plot_ps(it, ax):
    drawn = list(sd.plot("active", it=it, ax=ax, alpha=0.3, cmap="binary"))

    PSs, contours_deriv, contours_wave = ith.topology.intersection_of_contours_of_masks(du[it,0] > 0, active[it,0], x, y)

    for line in contours_deriv:
        drawn.extend(ax.plot(*line.T, "k:"))

    for line in contours_wave:
        interpol = RectBivariateSpline(x, y, du[it,0] > 0, kx=1, ky=1)
        color = interpol(line[:,1], line[:,0], grid=False)
        drawn.append(ith.plot.colorline(*line.T, color, vmin=0, vmax=2, cmap="plasma", ax=ax))

    drawn.extend(ax.plot(*PSs.T, "yo", markersize=5))
    return drawn

fig, ax = plt.subplots(dpi=300)
plot_ps(it, ax)
fig.savefig(sys.argv[0]+".png")

fig, ax = plt.subplots(dpi=180)
ith.plot.movie(fig, ax, plot_ps, np.arange(178, sd.shape[0]).reshape((-1, 1)), filename=sys.argv[0]+".mp4")
