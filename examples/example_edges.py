#!/bin/env python3
"""
Draw coloured grid lines using matplotlib
"""
import numpy as np
import matplotlib.pyplot as plt
import ithildin as ith
from typing import List

# load any simulation and calculate a phase
sd = ith.SimData.from_stem("results/smooka_5")
apd = sd.log.sources[1]['S2 stimulus delivered at t'] -  sd.log.sources[1]['S2 sensor excited at t']
sd.vars["phi"] = sd.phase("arr", dur=0.4*apd)

# # load any simulation and calculate a phase
# sd = ith.SimData.from_stem("results/pdl-detection/phase_68")
# # sd.vars["phi"] = sd.vars["phiskew"]
# # sd.vars["phi"] = sd.vars["phiarr"]
# sd.vars["phi"] = sd.phase("arr")

# preparations for plotting
it = -20
f = phase = sd.vars["phi"]
x, y = sd.linspace(-1), sd.linspace(-1)
X, Y = np.meshgrid(ith.plot.add_points(x), ith.plot.add_points(y))

for name in ["mpg", "cos", "pc", "glat", "ip"]:

    # calculate phase defect on points and edges
    rho = sd.phase_defect(name, phase=phase)
    sigma = sd.phase_edge_defect(name, phase=phase)

    # plot image
    fig, ax = plt.subplots(dpi=200)
    ax.set_aspect(1)
    # qm1 = ax.pcolormesh(X, Y, ith.plot.merge(*[None]*3, phase[it,0]), cmap="twilight", alpha=0.5)
    qm1 = ax.pcolormesh(X, Y, ith.plot.merge(*[None]*3, rho[it,0]), cmap="Greens", alpha=0.5)
    fig.colorbar(qm1)
    qm2 = ax.pcolormesh(X, Y, ith.plot.merge(None, sigma[0][it,0], sigma[1][it,0]), cmap="Purples")
    fig.colorbar(qm2)
    plt.tight_layout()
    fig.savefig(f"edge_{name}.png")

    # # make movie
    # def draw(it, ax, cb=True):
    #     # qm1 = ax.pcolormesh(X, Y, ith.plot.merge(*[None]*3, phase[it,0]), cmap="twilight", alpha=0.5)
    #     qm1 = ax.pcolormesh(X, Y, ith.plot.merge(*[None]*3, rho[it,0]), cmap="Greens", alpha=0.5)
    #     qm2 = ax.pcolormesh(X, Y, ith.plot.merge(None, sigma[0][it,0], sigma[1][it,0]), cmap="Purples")
    #     return [qm2, qm1]
    # fig, ax = plt.subplots(dpi=300)
    # ax.set_aspect(1)
    # ith.plot.movie(fig, ax, draw, [[i] for i in np.arange(sd.shape[0]//3*2, sd.shape[0])], filename=f"edge_{name}.mp4")
