#!/bin/env python3
"""
Visualise the process to detect PDLs
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
from typing import Union, Dict, List

sd = ith.SimData.from_stem("results/pdl-detection/phase_68")
dt, dz, dy, dx = sd.deltas
Nt, Nz, Ny, Nx = sd.shape
t = sd.times
x, y = sd.linspace(3), sd.linspace(2)

u = sd.vars["u"]
# phi = sd.vars["phi"] = sd.vars["phiact"]
phi = sd.vars["phi"] = sd.vars["phiarr"]
# phi = sd.vars["phi"] = sd.vars["phiskew"]
rho = sd.vars["rho"] = sd.vars["phiarrrhopc"]

fig, ax = plt.subplots(figsize=(6, 6), dpi=360)
# fig, ax = plt.subplots(figsize=(6, 6), dpi=180)

cmaps = dict(
        phi="twilight",
        rho="Greens",
)
labels = dict(
        u="transmembrane voltage",
        phi="phase",
        rho="phase defect",
        pdls="PDLs",
)
vmin = dict(u=0, phi=0, rho=0)
vmax = dict(u=1, phi=2*np.pi, rho=np.nanmax(rho))

def process_frame(it=-10, ax=ax, **alphas):
    iy, ix = int(Ny*0.7), int(Nx*0.1)
    drawn = []

    for varname, alpha in alphas.items():
        if varname in sd.vars:
            label = labels.get(varname, "???")
            # drawn.extend(sd.plot(varname, it=it, ax=ax, vmin=vmin.get(varname, None), vmax=vmax.get(varname, None), cmap=cmaps.get(varname, "inferno"), alpha=alpha))
            drawn.append(ax.imshow(sd.vars[varname][it,0], vmin=vmin.get(varname, None), vmax=vmax.get(varname, None), cmap=cmaps.get(varname, "inferno"), alpha=alpha, extent=(x[0],x[-1],y[0],y[-1]), origin="lower", interpolation="none", label=label))
        elif varname == "pdls" and alpha > 0.01:
            pdls = ith.pdl.pdls_ridge(rho[it,0], x, y, threshold=0.1, dx=sd.deltas[-1], dy=sd.deltas[-2])
            for pdl in pdls:
                q = pdl.charge(phi[it,0])
                drawn.extend(plt.plot(*pdl.points.T, ("k" if q == 0 else ("r" if q < 0 else "b"))+"-", alpha=alpha))

    ax.set_aspect(1)
    ax.set_title(" and ".join(labels[v] for v, a in alphas.items() if a > 0.01 and v in labels))

    # drawn.append(ax.legend())

    return drawn

ax.set_xlabel("$x$ in mm")
ax.set_ylabel("$y$ in mm")

it0 = Nt - 300
steps = np.linspace(it0, Nt, 8)
its = np.arange(it0, Nt)
# its = np.arange(it0, Nt, 2)
args = its.reshape((-1, 1))

names = ["pdls", "rho", "phi", "u"]
kwargs = [np.interp(its, steps, (len(names) - np.arange(2*len(names))//2 == n + 1).astype(float)) for n in range(len(names))]
kwargs = [dict(zip(names, row)) for row in zip(*kwargs)]

ith.plot.movie(fig, ax, process_frame, args, kwargs, filename=sys.argv[0]+".mp4", fps=30)
# ith.plot.movie(fig, ax, process_frame, args, kwargs, filename=sys.argv[0]+".mp4", fps=15)
