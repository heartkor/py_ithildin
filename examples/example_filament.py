#!/bin/env python3
"""
Example script to test ithildin.filament
"""

import ithildin as ith
import ithildin.filament as fil
import matplotlib.pyplot as plt
import sys
from itertools import chain

# filaments = fil.read_filaments("results/pdl2D_6_tipdata.txt")
# filaments = fil.read_filaments("results/ratchet_20_tipdata.txt")
filaments = fil.read_filaments("results/PDL_12_tipdata.txt")
# filaments = fil.read_filaments("results/PDS_21_tipdata.txt")
# filaments = fil.read_filaments("results/pdl-detection/phase_68_tipdata.txt")

print(f"read {len(filaments)} filaments")


trajectories = fil.filaments_to_trajectories(filaments, maxdist=10)
filaments = list(chain(*trajectories))
filaments.sort(key=lambda f: len(f), reverse=True)
trajectories.sort(key=lambda f: len(f), reverse=True)

trajectories[0] = fil.trajectory_add_times(trajectories[0], [30., 40., 50., 190.])

print(f"found {len(filaments)} filaments, lengths ranging from {len(filaments[-1])} to {len(filaments[0])}")
print(f"found {len(trajectories)} trajectories, lengths ranging from {len(trajectories[-1])} to {len(trajectories[0])}")

fil = filaments[0][:2]

print(fil)
print(fil.tans)
print(fil.faces)
print(ith.filament.interpret_faces(fil.faces))
print(ith.filament.interpret_faces([1,2,3,11,12,13,21,22,23,-11,-2,-23]))

ith.xdmf.xdmf_write("filaments.xmf", ith.xdmf.xdmf_trajectories(trajectories))

trajectories[0][0].plot()
plt.show()

fig = plt.figure(figsize=(6,6), dpi=200)
ax = fig.add_subplot(projection="3d")
ax.set_xlim(40, 60)
ax.set_ylim(100, 120)
ax.set_zlim(0, 6)
ith.plot.movie(fig, ax, ith.plot.plot_filament, [[f] for f in trajectories[0]], filename=sys.argv[0]+".mp4")
