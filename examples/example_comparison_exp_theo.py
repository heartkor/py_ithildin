#!/bin/env python3
"""
Compare different kinds of heart rhythms for experiment and simulation
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt

def compare_exp_theo(it, ax):
    drawn = []

    drawn.append(ax[0,0].imshow(sd.vars["u"][it+52,0],   vmin=-0.5*3, vmax=1.3*3))
    drawn.append(ax[0,1].imshow(sd.vars["u"][it+127,0],  vmin=-0.5*3, vmax=1.3*3))
    drawn.append(ax[0,2].imshow(sd.vars["u"][it+431,0],  vmin=-0.5*3, vmax=1.3*3))

    drawn.append(ax[1,0].imshow(od.vars["u"][it+205,0],  vmin=-0.5, vmax=1.3))
    drawn.append(ax[1,1].imshow(od.vars["u"][it+650,0],  vmin=-0.5, vmax=1.3))
    drawn.append(ax[1,2].imshow(od.vars["u"][it+1500,0], vmin=-0.5, vmax=1.3))

    return drawn

od = ith.SimData.from_stem("results/pdl-detection/optical_20200204114234")
sd = ith.SimData.from_stem("results/smooka_3")

od.vars["u"] = np.where(od.mask, od.vars["u"], np.nan)
sd.vars["u"] = np.where(sd.mask, sd.vars["u"], np.nan)

fig, ax = plt.subplots(2, 3, figsize=(3*2, 2*2), dpi=300)

for a in ax.flatten():
    a.set_aspect(1)
    a.axis(False)

compare_exp_theo(0, ax)

ax[0,0].set_title("Normal rhythm")
ax[0,1].set_title("Tachycardia")
ax[0,2].set_title("Fibrillation")

ax[0,0].text(-0.1, 0.5, "Simulation", va="center", ha="center", rotation="vertical", transform=ax[0,0].transAxes)
ax[1,0].text(-0.1, 0.5, "Experiment", va="center", ha="center", rotation="vertical", transform=ax[1,0].transAxes)

ith.plot.movie(fig, ax, compare_exp_theo, [[it] for it in range(25)], fps=10)
