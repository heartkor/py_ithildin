#!/bin/env python3
"""
Example script: calculate phases and find phase defect lines (PDLs)
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt

# plotting preparations: subplots in 2 rows and 2 cols
fig, ((ax00, ax01), (ax10, ax11)) = plt.subplots(2, 2, figsize=(11,9), dpi=360)

# load data
print("loading data...")
simdata = ith.SimData.from_stem("results/phase_68")
print(simdata)

# reminder: indexing in simdata: 0 -> t, 1 -> z, 2 -> y, 3 -> x
dt, dz, dy, dx = simdata.deltas
Nt, Nz, Ny, Nx = simdata.shape
t = simdata.times
x, y = np.meshgrid(simdata.linspace(3), simdata.linspace(2))

# plot data at one point in time and at certain z
it, iz = -50, 0
simdata.plot("u", it=it, iz=iz, ax=ax00)

algo_phase = "arr"

print(f"calculating phase {algo_phase}...")
if algo_phase == "sig":
    phase = simdata.phase("act")
    phase = 3.21*phase - 7.79
    phase = 2*np.pi/(1+np.exp(-phase))
else:
    phase = simdata.phase(algo_phase)

# continue at only one point in time and one slice in z
u = simdata.vars["u"][it,iz]
v = simdata.vars["v"][it,iz]
phase = phase[it,iz]

# plot phase space of cell model
plt.set_cmap("twilight")
sc01 = ax01.scatter(u.flatten(), v.flatten(), c=phase.flatten(), marker=".")
ax01.set_title(f"phase {algo_phase} in phase space")
ax01.set_xlabel("u")
ax01.set_ylabel("v")
cb01 = fig.colorbar(sc01, ax=ax01)

# plot phase
ax10.set_title(f"phase {algo_phase} in physical space")
ax10.set_xlabel(ax00.get_xlabel())
ax10.set_ylabel(ax00.get_ylabel())
qm10 = ax10.pcolormesh(x, y, phase, shading="nearest")
cb10 = fig.colorbar(qm10, ax=ax10)

algo_defect = "pc"

print(f"calculating defect {algo_defect}...")
if algo_defect == "topo_charge":
    defect = ith.defect.defect_topo_charge(u, v, axes=[-2, -1], deltas=[dy, dx])
else:
    defect = simdata.phase_defect(algo_defect, phase, axes=[-2, -1], deltas=[dy, dx], neighbours=1.9)

print("plotting...")
plt.set_cmap("Greens")
ax11.set_title("PDLs")
ax11.set_xlabel(ax00.get_xlabel())
ax11.set_ylabel(ax00.get_ylabel())
qm11 = ax11.pcolormesh(x, y, defect, shading="nearest")
cb11 = fig.colorbar(qm11, ax=ax11)

plt.tight_layout()
plt.title(f"phase defect using '{algo_defect}' algorithm")
fig.savefig(f"phase.{algo_phase}.{algo_defect}.png")
