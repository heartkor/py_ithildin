#!/bin/env python3
"""
Phase is like a clock
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
from typing import Union, Dict, List

sd = ith.SimData.from_stem("results/pdl-detection/phasealpa_7")
# sd = ith.SimData.from_stem("results/pdl-detection/phase_68")

dt, dz, dy, dx = sd.deltas
Nt, Nz, Ny, Nx = sd.shape
t = sd.times
x, y = sd.linspace(3), sd.linspace(2)

# phasealpa_7
u = sd.vars["u"] = np.flip(sd.vars["u"], axis=-1)
# phi = sd.vars["phi"] = sd.phase("arr")
phi = sd.vars["phi"] = np.flip(sd.vars["phiact"], axis=-1)
# phi = sd.vars["phi"] = np.flip(sd.vars["phiarr"], axis=-1)
# phi = sd.vars["phi"] = np.flip(sd.vars["phiskew"], axis=-1)
it0 = Nt - 66
it1 = Nt - 1
iy, ix = int(Ny*0.7), int(Nx*0.1)
st = slice(it0, it1)

# # phase_68
# u = sd.vars["u"]
# phi = sd.vars["phi"] = sd.vars["phiarr"]
# it0 = Nt - 210
# it1 = Nt - 65
# iy, ix = int(Ny*0.7), int(Nx*0.65)
# st = slice(it0, it1)

fig, ax = plt.subplots(2, 2, figsize=(11,9), dpi=360)

def add_labels(ax:plt.Axes, radius:float, labels:Union[Dict[float,str],List[str]], rotation:bool=False, **kwargs):
    if isinstance(labels, list):
        labels = {i: v for i, v in zip(np.linspace(0, 2*np.pi, len(labels)+1), labels)}
    drawn = []
    for phi, l in labels.items():
        rot = 0 if not rotation else -np.mod(phi/np.pi*180, 180)
        drawn.append(ax.text(radius*np.sin(phi), radius*np.cos(phi), l, ha="center", va="center", rotation=rot, **kwargs))
    return drawn

def clock(ax:plt.Axes, arrow:float=None):
    drawn = []
    clockco = np.meshgrid(*(np.linspace(-1, 1, 500) for _ in range(2)))
    clock = np.flip(ith.phase.phase_state_space(*clockco, u_iso=0, v_iso=-1e-10), axis=1)
    clockr = np.linalg.norm(clockco, axis=0)
    clock[clockr < 0.5] = np.nan
    clock[clockr > 0.9] = np.nan
    drawn.append(ax.imshow(clock, vmin=0, vmax=2*np.pi, cmap="twilight", interpolation="none", origin="lower", extent=(-1,1,-1,1)))
    ax.axis(False)
    ax.add_artist(plt.Circle((0, 0), 0.5, fill=False))
    ax.add_artist(plt.Circle((0, 0), 0.9, fill=False))

    if arrow is not None:
        radius = 0.7
        drawn.append(ax.arrow(0, 0, radius*np.sin(arrow), radius*np.cos(arrow), fc="k", lw=0, width=0.03))

    twelve = list(range(12))
    twelve[0] = "12 = 0"
    drawn.extend(add_labels(ax, 1., twelve))

    radians = ["$2\\pi = 0$", "$\\frac{1}{2}\\pi$", "$\\pi$", "$\\frac{3}{2}\\pi$"]
    drawn.extend(add_labels(ax, 0.4, radians))

    drawn.extend(add_labels(ax, 0.23, ["360° = 0°", "90°", "180°", "270°"], color="gray"))

    # drawn.extend(add_labels(ax, 0.7, ["resting", "excited", "plateau", "recover"], backgroundcolor=(1.,1.,1.,0.5)))
    drawn.extend(add_labels(ax, 0.7, {0: "resting", 0.25*np.pi: "excitation", 0.5*np.pi: "plateau", 1.2*np.pi: "recovery"}, backgroundcolor=(1.,1.,1.,0.5), rotation=False))

    return drawn

def draw_clock(it=it0, ax=ax, arrow:bool=False):
    drawn = []

    drawn.extend(sd.plot("u", it=it, ax=ax[0,0], vmin=0, vmax=1))
    ax[0,0].set_aspect(1)
    ax[0,0].set_title("transmembrane voltage in space")

    cbar, img = sd.plot("phi", it=it, ax=ax[0,1], cmap="twilight", vmin=0, vmax=2*np.pi)
    drawn.extend([cbar, img])
    ax[0,1].set_aspect(1)
    ax[0,1].set_title("phase in physical space")

    radians = ["0", "$\\frac{1}{2}\\pi$", "$\\pi$", "$\\frac{3}{2}\\pi$", "$2\\pi$"]
    cbar.set_ticks(np.linspace(0, 2*np.pi, len(radians)))
    cbar.set_ticklabels(radians)

    drawn.extend(ax[0,0].plot(x[ix], y[iy], "wo", mec="k"))
    drawn.extend(ax[0,1].plot(x[ix], y[iy], "wo", mec="k"))

    drawn.append(ith.plot.colorline(t[st], u[st,0,iy,ix], phi[st,0,iy,ix], ax=ax[1,0], setlims=True, cmap="twilight", vmin=0, vmax=2*np.pi))

    if arrow:
        drawn.append(ax[1,0].axvline(t[it], c="k"))

    ax[1,0].set_ylim((-0.1, 1.1))
    ax[1,0].set_title("transmembrane voltage at one point, coloured by phase")
    ax[1,0].set_xlabel("$t$ in ms")
    ax[1,0].set_ylabel("$u$ rescaled to [0,1]")

    drawn.extend(clock(ax[1,1], arrow=phi[it,0,iy,ix] if arrow else None))

    return drawn

draw_clock()
plt.tight_layout()
fig.savefig(sys.argv[0]+".png")

def draw_clock_movie(*a, **kw): return draw_clock(*a, **kw, arrow=True)
fig, ax = plt.subplots(2, 2, figsize=(11,9), dpi=180)
ith.plot.movie(fig, ax, draw_clock_movie, [[it] for it in np.arange(it0, it1)], filename=sys.argv[0]+".mp4", fps=20)
