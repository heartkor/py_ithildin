#!/bin/env python3
"""
Plot wave front, wave back and phase defect line
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
from typing import List
from scipy.interpolate import RectBivariateSpline
Line = np.ndarray # shape (N, D), N number of points, D spatial dimensions

sd = ith.SimData.from_stem("results/pdl-detection/phase_68")
Nt, Nz, Ny, Nx = sd.shape
dt, dz, dy, dx = sd.deltas
it = -40
t = sd.linspace(0)
x = sd.linspace(3)
y = sd.linspace(2)
u = sd.vars["u"]
du = sd.vars["du"] = u - np.roll(u, 1, axis=0)
phi = sd.vars["phi"] = sd.vars["phiarr"] if "phiarr" in sd.vars else sd.phase("arr", dur=128)
phi_min = 1e-20
phi_max = 5.5
active = sd.vars["active"] = phi < phi_max
rho = sd.vars["rho"] = sd.vars["phiarrrhopc"] if "phiarrrhopc" in sd.vars else sd.phase_defect("pc", phi)
rho_iso = 0.1
if "phiarrsigma3pc" in sd.vars:
    sigma = sd.vars["sigmay"], sd.vars["sigmax"] = sd.vars["phiarrsigma2pc"], sd.vars["phiarrsigma3pc"]
else:
    sigma = sd.vars["sigmay"], sd.vars["sigmax"] = sd.phase_edge_defect("pc", phi)[-2:]
sd.vars["sigma"] = sum(sigma)
sd.vars["regions"] = active + 2*(rho > rho_iso)

fig, ax = plt.subplots()

# sd.plot("u", it=it, ax=ax, alpha=0.6)
# sd.plot("phi", it=it, ax=ax, alpha=0.6, cmap="twilight", vmin=0, vmax=2*np.pi)
sd.plot("rho", it=it, ax=ax, cmap="Greens", vmin=0)
# sd.plot("sigma", it=it, ax=ax, cmap="Purples", vmin=0)
sd.plot("active", it=it, ax=ax, alpha=0.3, cmap="binary")
# sd.plot("regions", it=it, ax=ax, alpha=0.3, cmap="viridis")

print("# harp")
pdls = ith.pdl.pdls_harp(rho[it,0] > rho_iso, active[it,0], x, y)
pdls.sort(key=lambda pdl: len(pdl), reverse=True)
print("found", len(pdls), "PDLs")
for i, pdl in enumerate(pdls):
    print(f"{pdl}, charge {pdl.charge(phi[it,0])}, arc length {pdl.arc_length()}")
    ax.plot(*pdl.points.T, "k-", label="harp" if i == 0 else None)
print()

print("# edge")
pdls = ith.pdl.pdls_edge([s[it,0] > rho_iso for s in sigma], x, y, dx=sd.deltas[-1], dy=sd.deltas[-2])
pdls = [p for p in pdls if len(p) > 2]
pdls.sort(key=lambda pdl: len(pdl), reverse=True)
print("found", len(pdls), "PDLs")
for i, pdl in enumerate(pdls):
    print(f"{pdl}, charge {pdl.charge(phi[it,0])}, arc length {pdl.arc_length()}")
    ax.plot(*pdl.points.T, "m-", label="edge" if i == 0 else None)
print()

print("# ridge")
pdls = ith.pdl.pdls_ridge(rho[it,0], x, y, threshold=rho_iso, dx=sd.deltas[-1], dy=sd.deltas[-2])
pdls.sort(key=lambda pdl: len(pdl), reverse=True)
print("found", len(pdls), "PDLs")
for i, pdl in enumerate(pdls):
    print(f"{pdl}, charge {pdl.charge(phi[it,0])}, arc length {pdl.arc_length()}")
    ax.plot(*pdl.points.T, "b-", label="ridge" if i == 0 else None)
print()

#
# extra method: fit b-spline where rho is above threshold
#
print("# spline")
import spline
mask = rho[it,0] > rho_iso
xg, yg = sd.meshgrid(3, 2)
points = np.transpose((xg[mask], yg[mask]))
points, color = spline.sort(points)
s = max(np.std(points, axis=0))
pdl = spline.fit_spline(points, s)
print("only one PDL")
ax.plot(*pdl.T, "c-", label="spline")

plt.legend()
# plt.show()
fig.savefig(sys.argv[0]+".png", dpi=300)
