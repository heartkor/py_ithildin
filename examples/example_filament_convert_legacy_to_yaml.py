#!/bin/env python3
'''
Convert an old tipdata file to the new YAML format

Pass input filename and output filename as command line arguments:

    python3 example_filament_convert_legacy_to_yaml.py PDL_12_tipdata.txt PDL_12_tipdata.yaml
'''
import ithildin as ith
import sys
import yaml

fils = ith.filament.read_filaments(sys.argv[1])
d = {'Ithildin tipdata version': 2, 'domain': dict(res=fils[0].res.tolist())}
d['tipdata'] = []
for f in fils:
    d['tipdata'].append(dict(time=f.time, points_grid=f.points_grid.tolist(), faces=f.faces.tolist()))

with open(sys.argv[2], 'w') as f:
    f.write(yaml.safe_dump(d))
