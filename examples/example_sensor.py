#!/bin/env python3
'''
Example script to plot a sensor
'''

import ithildin as ith
import matplotlib.pyplot as plt
import numpy as np
import sys

# read simulation results
stem = sys.argv[1] if len(sys.argv) > 1 else 'results/bayes_26'
sd = ith.SimData.from_stem(stem)

label_time = 't'
for idxs, sensor in sd.sensors.items():
    time = sensor[label_time]
    print(idxs)
    pos = [sd.linspace(-dim-1)[idx-1] for dim, idx in enumerate(idxs)]
    pos_str = ', '.join(f'{x:.1f}' for x in pos)
    for label in sensor.columns:
        if label != label_time:
            plt.plot(time, sensor[label], label=f'{label}({label_time}, {pos_str})')

plt.legend()
plt.show()
