#!/bin/env python3
"""
Example script to test ithildin.filament
"""
import ithildin as ith
import matplotlib.pyplot as plt

filaments = ith.filament.read_filaments("example_filament.yaml")

print(f"read {len(filaments)} filaments")

for fil in filaments:
    print(fil.points)
    print(fil.payload.get('color'))
    print(ith.filament.interpret_faces(fil.faces))
    fil.plot()
    plt.show()
