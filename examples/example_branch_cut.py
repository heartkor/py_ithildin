#!/bin/env python3
"""
Calculate topological charge for a branch cut
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt

x = y = np.linspace(-2, 2, 50)
xg, yg = np.meshgrid(x, y)
dx = x[1] - x[0]
dy = y[1] - y[0]
zg = xg + 1j*yg

# fg = (zg**2 - 1)**0.5
arg1 = np.angle(zg-1)/2
arg2 = np.angle(zg+1)/2
phi = np.mod(arg1 + arg2, 2*np.pi)

plt.pcolormesh(xg, yg, phi, cmap="twilight", vmin=0, vmax=2*np.pi, shading="nearest")
plt.colorbar()
plt.show()

rho = ith.defect.defect_cpg(phi, deltas=[dy,dx])

plt.pcolormesh(xg, yg, rho, cmap="Greens", shading="nearest")
plt.colorbar()
plt.show()

plt.pcolormesh(xg, yg, phi, cmap="twilight", vmin=0, vmax=2*np.pi, shading="nearest")
plt.colorbar()

rho0 = 0.2*np.nanmax(rho)
print(f"rho_0 = {rho0}")
pdls = ith.pdl.pdls_ridge(rho, x, y, dx=dx, dy=dy, threshold=rho0)
for i, pdl in enumerate(pdls):
    if len(pdl) < 2:
        continue

    c = pdl.contour()
    print(c)
    ith.plot.colorline(*c.points.T, np.arange(len(c)))

    clockwise = ith.pdl.clockwise(c)

    charge = ith.pdl.charge(c, phi)

    print(f"{pdl}, clockwise {clockwise}, charge {charge}, arc length {pdl.arc_length()}")
    plt.plot(*pdl.points.T, "g-")

print()
plt.show()
