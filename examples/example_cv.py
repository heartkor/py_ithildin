#!/bin/env python3
"""
Example script to calculate conduction velocity and action potential duration
"""

import ithildin as ith
import matplotlib.pyplot as plt
import numpy as np

# read simulation results
sd = ith.SimData.from_stem("results/phase_68")

apd, tarr, tdea = ith.pulse.duration(sd.vars["u"], t=sd.times, u_iso=0.3)
apd = np.where(apd>0, apd, np.nan)
sd.vars["apd"] = apd
sd.vars["lat"] = tarr
apd, tarr, tdea = apd[-1,0], tarr[-1,0], tdea[-1,0]
apdm = np.nanmean(apd)
apds = np.nanstd(apd)
print(f"APD = ({apdm} +/- {apds}) ms")

sd.plot("lat", it=-1, iz=0)
plt.gca().set_aspect(1)
plt.show()

sd.plot("apd", it=-1, iz=0)
plt.gca().set_aspect(1)
plt.show()

# Caution: This function is expensive! Only run for a single 2D frame.
cv, approx = ith.pulse.velocity(tarr, *sd.meshgrid(-2, -1), dist=10, maxdt=20.1*sd.deltas[0])
sd.vars["latapprox"] = np.reshape(approx, (1,1,*approx.shape))
sd.vars["cv"] = np.reshape(cv, (1,1,*cv.shape))
cvm = np.nanmean(cv)
cvs = np.nanstd(cv)
print(f"CV = ({cvm} +/- {cvs}) mm/ms")

sd.plot("latapprox", it=-1, iz=0)
plt.gca().set_aspect(1)
plt.show()

sd.plot("cv", it=-1, iz=0)
plt.gca().set_aspect(1)
plt.show()
