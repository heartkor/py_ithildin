#!/bin/env python3
"""
Plot PS
"""
import numpy as np
import matplotlib.pyplot as plt
import ithildin
import sys

def plot(it:int, ax:plt.Axes=None) -> list[plt.Artist]:
    if ax is None:
        ax = plt.gca()
    artists = []
    dt = sd.deltas[0]
    t = sd.linspace(0)[it]

    filaments = sd.filaments
    filament_times = [f.time for f in filaments]
    idx_min = np.argmin((filament_times - t)**2)
    fil = filaments[idx_min]

    # artists.extend(sd.plot("phi", it=it, iz=0, cmap="twilight", vmin=0, vmax=2*np.pi, ax=ax))
    artists.extend(sd.plot("rho", it=it, iz=0, cmap="Greens", vmin=0, vmax=1, ax=ax))
    # artists.extend(sd.plot("pdl", it=it, iz=0, cmap="binary", vmin=0, vmax=1, ax=ax))

    if t - dt/2 <= fil.time < t + dt/2:
        artists.extend(ax.plot(*fil.points[:,:2].T, 'ko', label="PS"))

    return artists

if __name__ == "__main__":
    sd = ithildin.sd("results/phase_68")

    if "phiarr" not in sd.vars:
        sd.vars["phiarr" ] = sd.phase("arr", dur=50)
    phi = sd.vars["phiarr"]

    if "phiarrrhopc" not in sd.vars:
        sd.vars["phiarrrhopc" ] = sd.phase_defect("pc", phi)
    rho = sd.vars["phiarrrhopc"]

    pdl = sd.vars["pdl"] = rho > 0.2

    # plot(-1)
    # plt.show()

    ithildin.plot.movie(plt.gcf(), plt.gca(), plot, [(it,) for it in range(sd.shape[0])])
