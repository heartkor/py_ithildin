#!/bin/env python3
"""
Filaments
---------

Filaments are the (instantaneous) rotation centres of rotors in 3D. They are
also called "tip lines". In 2D, we call them "tip" or "filament point".

The class :py:class:`Filament` describes a filament as a collection of points.

.. note::
    A :py:class:`Filament` might not necessarily describe an actual, physical
    filament! Member functions of the class can be used to convert one Filament
    into actual Filaments.

We call a list of :py:class:`Filament` objects at different times of the same
physical filament structure evolving over time "trajectory"
"""

from copy import deepcopy
from networkx.algorithms.dag import dag_longest_path
from scipy.spatial.distance import cdist, pdist, squareform
from typing import List, Tuple
from enum import Enum
import networkx as nx
import numpy as np

from .plot import plot_filament
from .xdmf import xdmf_filament
from .points import Points, break_apart_all, longest_path_all, Maybe1DArray, Maybe2DArray

class Filament(Points):
    """
    A filament or tip line in 3D consisting of points.
    A tip or filament point in 2D consisting of one point.
    At only one point in time.

    This class adds these parameters and variables to :py:class:`Points`:

    :param faces: list of which face the points were found in, use the function :py:meth:`interpret_faces` to interpret those integers
    :var faces: list of which face the points were found in
    :vartype faces: numpy.ndarray(shape=len(self), dtype=int)

    :param grads_u: gradient of the first variable :math:`\\nabla u` at each point in space and time
    :var grads_u: gradient of the first variable :math:`\\nabla u` at each point in space and time
    :vartype grads_u: numpy.ndarray(shape=(len(self), 3), dtype=np.float32)

    :param grads_v: gradient of the second variable :math:`\\nabla v` at each point in space and time
    :var grads_v: gradient of the second variable :math:`\\nabla v` at each point in space and time
    :vartype grads_v: numpy.ndarray(shape=(len(self), 3), dtype=np.float32)

    :param tans: normalised cross product :math:`\\nabla u \\times \\nabla v`
    :var tans: normalised cross product :math:`\\nabla u \\times \\nabla v`
    :vartype tans: numpy.ndarray(shape=(len(self), 3), dtype=np.float32)

    :param args: all arguments are passed on to :py:class:`Points`
    :param kwargs: all other keyword arguments are passed on to :py:class:`Points` as additional point based data
    """
    def __init__(self, *args, faces:Maybe1DArray=None, grads_u:Maybe2DArray=None, grads_v:Maybe2DArray=None, tans:Maybe2DArray=None, **kwargs):
        Points.__init__(self, *args, faces=faces, grads_u=grads_u, grads_v=grads_v, tans=tans, **kwargs)
        if self.faces is None and len(self) == 0:
            self.faces = np.zeros((0, self.ndim), dtype=np.float32)

    def __str__(self) -> str:
        return ("unordered " if not self.ordered else "") + f"Filament with {len(self)} points in {self.ndim}D" + ("" if self.time is None else f" at time {self.time}") + (" with gradient data " if self.grads_u is not None or self.grads_v is not None or self.tans is not None else "")

    @property
    def faces(self) -> Maybe2DArray:
        return self.payload.get("faces", None)

    @faces.setter
    def faces(self, values:np.ndarray):
        self.payload["faces"] = values

    @property
    def grads_u(self) -> Maybe2DArray:
        return self.payload.get("grads_u", None)

    @grads_u.setter
    def grads_u(self, values:np.ndarray):
        self.payload["grads_u"] = values

    @property
    def grads_v(self) -> Maybe2DArray:
        return self.payload.get("grads_v", None)

    @grads_v.setter
    def grads_v(self, values:np.ndarray):
        self.payload["grads_v"] = values

    @property
    def tans(self) -> Maybe2DArray:
        return self.payload.get("tans", None)

    @tans.setter
    def tans(self, values:np.ndarray):
        self.payload["tans"] = values

    def plot(self, *args, **kwargs):
        """
        Plot self using :py:func:`ithildin.plot.plot_filament`,
        where all arguments are passed on.
        """
        return plot_filament(self, *args, **kwargs)

    def to_xdmf(self, *args, **kwargs) -> str:
        """
        Convert to XDMF self using :py:func:`ithildin.xdmf.xdmf_filament`,
        where all arguments are passed on.
        """
        return xdmf_filament(self, *args, **kwargs)

    def sub(self, indices:List[int], ordered:bool=False) -> "Filament":
        """
        Construct a filament only based on the points with the given indices.

        :param indices: list of indices of the points to consider
        :param ordered: specifies whether the sub filament is already ordered

        :return: sub filament for only the selected points
        """
        return super().sub(indices=indices, ordered=ordered, cls=Filament, time=self.time)

Filament.yaml_register()

def read_filaments(path:str, format:str='') -> List[Filament]:
    '''
    Read a tipdata file generated by ithildin.

    .. note::
        For each point in time, ALL points in space may be merged to ONE filament,
        i.e. they are not split yet and do not describe an actual physical filament.

    :param path: filename to read
    :param format: the format of the file to read in, one of: ['yaml', 'legacy'], if empty, detect format automatically

    :return: list of one Filament per time t

    .. seealso::  :py:meth:`read_filaments_yaml`, :py:meth:`read_filaments_legacy`
    '''
    if len(format) == 0:
        with open(path, 'r') as f:
            line = f.readline()
            if ':' in line:
                format = 'yaml'
            else:
                format = 'legacy'

    format = format.lower()
    if format == 'yaml':
        return read_filaments_yaml(path)
    elif format == 'legacy':
        return read_filaments_legacy(path)
    else:
        raise ValueError(f'Invalid format: "{format}"!')

def read_filaments_yaml(path:str) -> List[Filament]:
    """
    Read a tipdata file in the YAML tipdata format generated by ithildin.

    .. note::
        This is designed to work with arbitrary tipdata.

    .. note::
        For each point in time, ALL points in space may be merged to ONE filament,
        i.e. they are not split yet and do not describe an actual physical filament.

    The YAML tipdata file format:

        1. The file is a valid YAML file.
        2. The field 'Ithildin tipdata version' is at the root of the YAML and its value is equal to '2'.
        3. Optionally, arbitrary metadata may be included in the root.
        4. Optionally, at 'domain.res', the resolution of the grid ``res`` is stored. This is the default value for the grid resolution for each :py:class:`Filament` that is read.
        5. The field 'tipdata' is a list of :py:class:`Filament`s: Each field in the elements in this list corresponds to an argument of the constructor of the class :py:class:`Filament`.

    Example for the contents of a YAML tipdata file::

        Ithildin tipdata version: 2
        domain:
          res:
            - 0.1
            - 0.1
            - 1.0
        tipdata:
        - time: 2.234
          points_grid:
          - [97.9062, 108.677, 187]
          - [107.808, 19.2872, 187]
          faces:
          - [3, 2]
          - [1, 2]
          grads_u: # optional
          - [0.1, 0.4, -0.3]
          - [0.0, 0.5, -0.3]
        - time: 1.234
          ordered: true # optional
          points:
          - [97.9062, 108.677, 187]
          - [107.808, 19.2872, 187]
          - [96.406, 106.7, 188]

    :param path: filename to read

    :return: list of one Filament per time t
    """
    import yaml
    with open(path, 'r') as f:
        y = yaml.safe_load(f)

    version = y['Ithildin tipdata version']
    assert(version == 2)

    dom = dict()
    if 'domain' in y and 'res' in y['domain']:
        dom['res'] = y['domain']['res']

    if 'tipdata' not in y or y['tipdata'] is None:
        y['tipdata'] = []

    filaments = []
    for fil in y['tipdata']:
        fil = {**dom, **fil} # merge data from domain and filament, prefer data from filament
        filaments.append(Filament(**fil))

    return filaments

def read_filaments_legacy(path:str) -> List[Filament]:
    """
    Read a tipdata file in the old tipdata format generated by ithildin.

    .. note::
        This is designed to work with tipdata of both 4 or 13 columns.

    .. note::
        For each point in time, ALL points in space may be merged to ONE filament,
        i.e. they are not split yet and do not describe an actual physical filament.

    :param path: filename to read

    :return: list of one Filament per time t
    """
    with open(path, "r") as f:
        # read header
        for i, line in enumerate(f):
            line = line.rstrip().split(" ")
            if i == 0:
                _ = np.int_(line) # number of header lines
            elif i == 1:
                _ = np.array(line, dtype=np.float32) # first print, delay between print
            elif i == 2:
                domsiz = np.array(line, dtype=np.int32) # N
            elif i == 3:
                domres = np.array(line, dtype=np.float32) # dx
            elif i == 4:
                _ = np.array(line, dtype=np.float32) # ignore
            elif i == 6: # after header
                break

        # read body
        mode = 0
        filaments = []
        for i, line in enumerate(f):
            if len(line.strip()) == 0:
                if mode == 2:
                    mode = 0
                    if Lfil == 0:
                        continue
                    assert(len(points) == Lfil)
                    points = np.array(points)
                    points.shape = (-1, 3)
                    filaments.append(Filament(points_grid=points, res=domres, time=t, faces=faces,
                        grads_u=grads_u if len(grads_u) > 0 else None,
                        grads_v=grads_v if len(grads_v) > 0 else None,
                        tans=tans if len(tans) > 0 else None,
                    ))
            elif mode == 0:
                # time, number of filaments (now: always -1)
                t, _ = [ty(s) for ty, s in zip([float, int], line.split())]
                mode = 1
                faces = []
                points = []
                grads_u = []
                grads_v = []
                tans = []
            elif mode == 1:
                # index of filament (now: always 1), number of points in the filaments
                _, Lfil = [ty(s) for ty, s in zip([int, int], line.split())]
                mode = 2
            elif mode == 2:
                # label, position
                words = line.split()
                faces.append(int(words[0]))
                points.append((float(words[1]), float(words[2]), float(words[3])))
                if len(words) == 13 or len(tans) > 0:
                    grads_u.append((float(words[4]), float(words[5]), float(words[6])))
                    grads_v.append((float(words[7]), float(words[8]), float(words[9])))
                    tans.append((float(words[10]), float(words[11]), float(words[12])))
    return filaments

Filaments = List[Filament]
Trajectory = List[Filament]
Trajectories = List[Trajectory]

def find_trajectories_spacetime(filaments:List[Filament], maxdist:float=1.5) -> List[List[Filament]]:
    """
    Turn list of ordered filaments into list of trajectories.

    :param filaments: list of ordered filaments
    :param maxdist: maximum distance in 4D space to consider as same filament
        (in units of 1 mm in space = 1 ms in time)

    :return: list of trajectories, i.e. list of lists of filaments
    """
    # turn each filament into a point in a 4D space
    points = np.transpose([
        np.array([f.time for f in filaments]),
        np.array([np.average(f.points[:,0]) for f in filaments]),
        np.array([np.average(f.points[:,1]) for f in filaments]),
        np.array([np.average(f.points[:,2]) for f in filaments]),
    ])

    # connect points (each corresponding to a filament) in 4D space by distance
    pd = pdist(points)
    graph = nx.Graph(squareform((pd<maxdist)*pd))

    # those points (== filaments) that are close enough together form a trajectory
    # trajectories is a list of trajectory
    # i.e. trajectories is a list of list of filament
    return [[filaments[i] for i in indices] for indices in nx.connected_components(graph)]

def find_trajectories(filaments:List[Filament], maxdist:float=np.inf, minlen:int=1) -> List[List[Filament]]:
    """
    Turn list of ordered filaments into list of trajectories.

    Filaments are connected to trajectories, if:

    - filament :math:`A` is in the time step before filament :math:`B`
    - the distance :math:`D(A,B)` between the two filaments is shorter than ``maxdist``
    - this distance is shorter than the distances to all other filaments in that time step

    The distance between filaments is defined as:

    .. math::
        D(A, B)
        = \\frac{1}{\\#A} \\sum_{a \\in A} \\min_{b \\in B} d(a, b)
        + \\frac{1}{\\#B} \\sum_{b \\in B} \\min_{a \\in A} d(a, b)

    where :math:`d(a, b)` is the L2 distance on the grid that the filament is stored on,
    and :math:`\\#A` is the number of points in filament :math:`A`.

    .. note:: This function also works for :py:class:`PDL` s!

    :param filaments: list of ordered filaments
    :param maxdist: maximum distance in spatial grid units to consider as same filament
    :param minlen: discard filaments with fewer nodes than this

    :return: list of trajectories, i.e. list of lists of filaments
    """

    t = [f.time for f in filaments]
    t.sort()
    t = np.array(t)
    dt = t[1:] - t[:-1]
    dt = np.min(dt[dt > 1e-10])

    def distance(fi: Filament, fj: Filament) -> float:
        if 0.5*dt < abs(fi.time - fj.time) < 1.5*dt:
            D = cdist(fi.points_grid, fj.points_grid)
            d = np.mean(np.min(D, axis=0)) + np.mean(np.min(D, axis=1))
            return d.tolist()
        return np.inf

    # measure distances between filaments and
    # connect points (each corresponding to a filament) in 4D space by distance
    graph = nx.DiGraph()
    for i, fi in enumerate(filaments):
        for j, fj in enumerate(filaments[:i]):
            if i != j:
                dist = distance(fi, fj)
                if dist < maxdist:
                    if fi.time < fj.time:
                        graph.add_edge(i, j, dist=dist)
                    else:
                        graph.add_edge(j, i, dist=dist)

    for node in graph.nodes:
        ein = list(graph.in_edges(node, data=True))
        eout = list(graph.out_edges(node, data=True))

        if len(ein) > 1:
            ein.sort(key=lambda e: e[2]["dist"])
            for (i, j, _) in ein[1:]:
                graph.remove_edge(i, j)

        if len(eout) > 1:
            eout.sort(key=lambda e: e[2]["dist"])
            for (i, j, _) in eout[1:]:
                graph.remove_edge(i, j)

    def longest_paths(graph: nx.DiGraph) -> List[List[int]]:
        if len(graph) == 0:
            return []

        paths = []
        for comp in nx.weakly_connected_components(graph):
            subgraph = graph.subgraph(comp).copy()
            while len(subgraph) > 0:
                path = dag_longest_path(subgraph)
                paths.append(path)
                subgraph.remove_nodes_from(path)

        return paths

    return [[filaments[i] for i in path] for path in longest_paths(graph)]

def filaments_to_trajectories(filaments:List[Filament], **kwargs) -> List[List[Filament]]:
    """
    Convenience function. Turn list of any type of filament into trajectories.
    Sort trajectories by length, sort filaments in each trajectory by time.

    1. break_apart_all
    2. longest_path_all
    3. find_trajectories

    :param filaments: list of any type of filaments
    :param kwargs: passed on to :py:meth:`find_trajectories`

    :return: list of trajectories, i.e. list of lists of filaments
    """
    trajectories = find_trajectories(longest_path_all(break_apart_all(filaments)), **kwargs)
    trajectories.sort(key=lambda t: len(t), reverse=True)
    for trajectory in trajectories:
        trajectory.sort(key=lambda f: f.time)
    return trajectories

def trajectory_at_times(trajectory:List[Filament], times:List[float]=[]) -> List[Filament]:
    """
    Match times of filaments in trajectory to the given times.

    .. note::
        If a point in time is before (or after) the first (or last) filament
        in the trajectory, an empty filament is created for that time

    :param trajectory: list of filaments at some times, will be sorted
    :param times: list of times at which to find the closest following filament
        in the trajectory

    :return: list of filaments at given times
    """
    times.sort()
    trajectory.sort(key=lambda f: f.time)

    filaments = []
    for j, time in enumerate(times):
        for i in range(1, len(trajectory)):
            if trajectory[i-1].time <= time <= trajectory[i].time:
                f = deepcopy(trajectory[i])
                f.time = time
                filaments.append(f)
                break
        if len(filaments) <= j:
            filaments.append(Filament(np.zeros((0, 2)), res=np.zeros((2,)), time=time, ordered=True, faces=[]))
    filaments.sort(key=lambda f: f.time)
    return filaments

def trajectory_add_times(trajectory:List[Filament], times:List[float]=[]) -> List[Filament]:
    """
    Extend trajectory with filaments at the given times

    :param trajectory: list of filaments at some times, will be sorted
    :param times: list of times at which to find the closest following filament
        in the trajectory

    :return: list of filaments at original times AND the given times
    """
    trajectory.extend(trajectory_at_times(trajectory, times))
    trajectory.sort(key=lambda f: f.time)
    return trajectory

class Face(Enum):
    """
    Enumeration of the possible face values that :py:meth:`interpret_faces` can return.
    It can be seen as the closest direction the filament faces at a given point.

    1. face normal to :py:attr:`X` (the yz-plane)
    2. face normal to :py:attr:`Y` (the xz-plane)
    3. face normal to :py:attr:`Z` (the xy-plane)

    .. warning:: The numbering of the axes is different here ``(X=1, Y=2, Z=3)`` than the typical convention used in this module ``(T=0, Z=1, Y=2, X=3)``.

    .. seealso::
        - :py:class:`Boundary`: enumeration of possible boundary values returned by :py:meth:`interpret_faces`
        - :py:class:`Filament`: class that contains ``faces`` data
    """
    X = 1
    Y = 2
    Z = 3

class Boundary(Enum):
    """
    Enumeration of the possible boundary values that :py:meth:`interpret_faces` can return.

    0. :py:attr:`INSIDE`: The point is inside the medium.
    1. :py:attr:`LOWER_BOUNDARY`: The point is at the boundary with the lowest index in the dimension the filament is facing.
    2. :py:attr:`UPPER_BOUNDARY`: The point is at the boundary with the highest index in the dimension the filament is facing.

    .. seealso::
        - :py:class:`Face`: enumeration of possible face values returned by :py:meth:`interpret_faces`
        - :py:class:`Filament`: class that contains integer ``faces`` data
    """
    INSIDE = 0
    LOWER_BOUNDARY = 1
    UPPER_BOUNDARY = 2

def interpret_faces(numbers:np.ndarray) -> Tuple[np.ndarray,np.ndarray,np.ndarray]:
    """
    The ``faces`` data in the first column of ``tipdata`` files generated by the C++ Ithildin framework describes how each segment of a filament is located in the voxel where it was found.
    Depending on the version of the framework, more or less information is stored in this integer value.
    This data is read and stored in instances of :py:class:`Filament` in the member variable ``faces``.

    This function converts an array of these integer values into more easy to interpret quantities.

    :param numbers: integer array of ``faces`` data, as stored in ``tipdata`` files
    :return: tuple of:

        * ``faces``, integer array of same shape as ``numbers``. The only possible values are described by the enumeration :py:class:`Face`.
        * ``signs``, integer array of same shape as ``numbers``. The only possible values are:

            - ``+1`` in positive direction described by the corresponding face
            - ``-1`` in negative, opposite direction

        * ``boundaries``, integer array of same shape as ``numbers``. The only possible values are described by the enumeration :py:class:`Boundary`.

    .. note:: This function also supports tuples describing a face in three dimensions. If the dimension of ``numbers`` is 2, these tuples get converted to the enumeration :py:class:`Face`::

        (2, 3) -> 1
        (1, 3) -> 2
        (1, 2) -> 3

    .. seealso::
        - :py:class:`Filament`: class that contains integer ``faces`` data
        - :py:class:`Face`: enumeration of possible face values
        - :py:class:`Boundary`: enumeration of possible boundary values
    """
    numbers = np.array(numbers, dtype=np.int16)
    if numbers.ndim == 1:
        signs = np.where(numbers > 0, 1, -1)
        i = numbers*signs
        faces = i % 10
        boundaries = i // 10
        assert(1 <= np.min(faces))
        assert(np.max(faces) <= 3)
        assert(np.max(boundaries) <= 2)
        return faces, signs, boundaries
    elif numbers.ndim == 2 and numbers.shape[1] == 2:
        vals = np.unique(numbers)
        if (1 <= vals).all() and (vals <= 3).all():
            return interpret_faces((5 - numbers[:,0] - numbers[:,1]) % 3 + 1)
        else:
            raise NotImplementedError("Only three dimensions are supported.")
    else:
        raise ValueError("Faces must either be a list[int] or list[tuple[int,int]]!")

def find_filament_2d(phi:np.ndarray, **kwargs) -> Filament:
    '''
    Find phase singularities in 2D phase data based on the Kuklik method [1].

    :param phi: 2D array of the phase.
    :param kwargs: other keyword-arguments are passed on to the constructor of :py:class:`Filament`
    :return: the phase singularities as a :py:class:`Filament` object

    [1]: Kuklik, P., Zeemering, S., Van Hunnik, A., Maesen, B., Pison, L., Lau,
         D., et al. (2017). Identification of rotors during human atrial
         fibrillation using contact mapping and phase singularity detection:
         technical considerations. IEEE Trans. Biomed. Eng. 64, 310–318. doi:
         10.1109/TBME.2016.2554660
    '''
    assert phi.ndim == 2
    shifts_list = [
        [(0,0),(-1,0),(-1,-1),(0,-1)],
        [(1,0),(0,1),(-1,1),(-2,0),(-2,-1),(-1,-2),(0,-2),(1,-1)],
    ]
    rings = [np.array([np.roll(phi, shift, axis=(-2, -1)) for shift in shifts]) for shifts in shifts_list]
    is_filament = np.all([1 == np.sum(
        np.abs(np.roll(r, -1, axis=0) - r) > np.pi,
    axis=0) for r in rings], axis=0)
    assert is_filament.shape == phi.shape
    iy, ix = np.where(is_filament)
    iy = iy.astype(float) - 0.5
    ix = ix.astype(float) - 0.5
    return Filament(points_grid=np.transpose([ix, iy]), **kwargs)
