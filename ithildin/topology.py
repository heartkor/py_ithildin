#!/bin/env python3
"""
Topology
--------

This submodule contains lots of functions to find the intersection of different kinds of sets.
"""
import numpy as np
from itertools import groupby
from typing import List, Tuple, Union
from scipy.interpolate import RegularGridInterpolator
from intersect import intersection

from .graph import marching_squares

Points = np.ndarray # shape (N, D), N number of points, D spatial dimensions
Line = Points
Lines = List[Line]
Mask = np.ndarray # shape (Ny, Nx), Ny number of points in y dimension, Nx, number of points in x direction
Coords = List[float] # coordinates of points in one dimension

def intersection_lines_and_lines(lines_a:Lines, lines_b:Lines) -> Points:
    """
    For a region :math:`M` and some lines :math:`L` in 2D space,
    find the intersection :math:`M \\cap L`.

    :param lines_a: list of lines :math:`A`
    :param lines_b: list of lines :math:`B`
    :return: the intersection :math:`A \\cap B`, a list of points
    """
    points = []
    for a in lines_a:
        for b in lines_b:
            points.append(np.array(intersection(*a.T, *b.T)))
    if len(points) == 0:
        return np.zeros((0, 2))
    else:
        return np.concatenate(points, axis=1).T

def intersection_lines_and_mask(lines:Lines, mask:Mask, *coords:Coords) -> Lines:
    """
    For a region :math:`M` and some lines :math:`L` in 2D space,
    find the intersection :math:`M \\cap L`.

    :param lines: list of lines to find the intersection with :math:`M` for
    :param mask: mask describing the first region :math:`M` on a regular rectangular grid
    :param coords: the positions of the grid points for each of the dimensions
    :return: the intersection :math:`M \\cap L`, a list of lines
    """
    assert(mask.ndim == len(coords) == 2)
    assert(mask.shape == tuple(len(c) for c in reversed(coords)))
    interpol = RegularGridInterpolator(coords, mask.T, method="nearest")
    new = []
    for line in lines:
        assert(line.ndim == 2)
        assert(line.shape[1] == mask.ndim)
        inside = interpol(line) > 0.5
        new.extend(np.array([el[1] for el in group]) for m, group in groupby(zip(inside, line), key=lambda el: el[0]) if m)
    return new

def contours_of_mask_in_other_mask(mask:Mask, other_mask:Mask, *coords:Coords) -> Tuple[Lines,Lines]:
    """
    For two regions :math:`M` and :math:`M'` in 2D space
    which are encoded in ``mask`` and ``other_mask``, respectively,
    find the first regions contour :math:`\\partial M` (``mask``),
    and the part of this contour :math:`M' \\cap \\partial M`
    which is also inside of the other region (``other_mask``).

    :param mask: mask describing the first region :math:`M` on a regular rectangular grid
    :param other_mask: mask for the other region :math:`M'`
    :param coords: the positions of the grid points for each of the dimensions
    :return: list of lines :math:`M' \\cap \\partial M`, and contours of the first mask :math:`\\partial M`

    Example: Two overlapping balls::

        import numpy as np
        import matplotlib.pyplot as plt
        import ithildin as ith

        # generate two overlapping balls in 2D
        xs = np.linspace(0, 6, 40)
        ys = np.linspace(0, 5, 30)
        x, y = np.meshgrid(xs, ys)
        mask = ((x - 2)**2 + (y - 3)**2)**.5 < 1.5
        other_mask = ((x - 3)**2 + (y - 2)**2)**.5 < 1.8

        # plot the balls, value 0: nothing, 1: first ball, 2: second ball, 3: overlap of both balls
        plt.pcolormesh(x, y, 1*mask + 2*other_mask, cmap="binary", shading="nearest")
        plt.colorbar()

        # find the contours of the first ball, and the part of the contour which is also in the second ball
        lines, contours = ith.topology.contours_of_mask_in_other_mask(mask, other_mask, xs, ys)

        # plot the contours and lines
        for c in contours:
            plt.plot(*c.T, "b-")
        for l in lines:
            plt.plot(*l.T, "m--")
        plt.show()

    """
    assert(mask.shape == other_mask.shape)
    assert(mask.ndim == len(coords) == 2)
    assert(mask.shape == tuple(len(c) for c in reversed(coords)))
    contours = marching_squares(*coords, mask, 0.5)
    return intersection_lines_and_mask(contours, other_mask, *coords), contours

def intersection_of_contours_of_masks(mask_a:Mask, mask_b:Mask, x:Union[Coords,None]=None, y:Union[Coords,None]=None) -> Tuple[Points,Lines,Lines]:
    """
    For two regions :math:`A` and :math:`B` in 2D space
    which are encoded in ``mask_a`` and ``mask_b``, respectively,
    find the both regions contours :math:`\\partial A` and :math:`\\partial B`
    and the intersection points where these contours cross :math:`\\partial A \\cap \\partial B`.

    :param mask_a: mask describing the first region :math:`A` on a regular rectangular grid
    :param mask_b: mask for the other region :math:`B`
    :param x: the positions of the grid points in :math:`x` direction
    :param y: the positions of the grid points in :math:`y` direction
    :return: list of points :math:`\\partial A \\cap \\partial B`, contours :math:`\\partial A`, and :math:`\\partial B`

    Example: Two overlapping balls::

        import numpy as np
        import matplotlib.pyplot as plt
        import ithildin as ith

        # generate two overlapping balls in 2D
        xs = np.linspace(0, 6, 40)
        ys = np.linspace(0, 5, 30)
        x, y = np.meshgrid(xs, ys)
        mask_a = ((x - 2)**2 + (y - 3)**2)**.5 < 1.5
        mask_b = ((x - 3)**2 + (y - 2)**2)**.5 < 1.8

        # plot the balls, value 0: nothing, 1: first ball, 2: second ball, 3: overlap of both balls
        plt.pcolormesh(x, y, 1*mask_a + 2*mask_b, cmap="binary", shading="nearest")
        plt.colorbar()
        points, contours_a, contours_b = ith.topology.intersection_of_contours_of_masks(mask_a, mask_b, xs, ys)

        # plot the contours and lines
        for c in contours_a:
            plt.plot(*c.T, "g-")
        for c in contours_b:
            plt.plot(*c.T, "b-")
        plt.plot(*points.T, "ro")
        plt.show()

    """
    if x is None: x = np.arange(mask_a.shape[1])
    if y is None: y = np.arange(mask_a.shape[0])
    assert(mask_a.ndim == 2 == mask_b.ndim)
    assert(mask_a.shape == mask_b.shape == (*y.shape, *x.shape))

    contours_a = marching_squares(x, y, mask_a, 0.5)
    contours_b = marching_squares(x, y, mask_b, 0.5)
    return intersection_lines_and_lines(contours_a, contours_b), contours_a, contours_b
