#!/bin/env python3
"""
Properties of pulses
--------------------

This submodule contains tools to compute properties of the impulses
propagating across the medium.

In cardiology, these are for instance *action potential duration* (APD) and
*conduction velocity* (CV).

"""
import numpy as np
from typing import List, Tuple, Union
from .graph import neighbourhood, delete_border

def time_elapsed_frame(u:np.ndarray, u_iso:float=0.65, t:Union[List[float],None]=None, it:int=-1, _return_arrival:bool=False) -> np.ndarray:
    """
    Calculate elapsed time for just one frame in :math:`u`.

    Elapsed time is the time since the last arrival of the activation front at
    each element in the variable :math:`u`.

    The arrival time is defined as the point in time, when the activation front
    pushes the variable :math:`u` above the threshold :math:`u_\\mathrm{iso}`.

    The first index of the array in ``u`` is interpreted as the index in time.
    The calculation of the elapsed time involves iteration over this index.

    .. note::
        The value ``-np.inf`` is used before any wave arrives.

    :param u: variable describing the activity of the medium. In the context of the bi-domain description in cardiology, this is the transmembrane voltage.
    :param u_iso: threshold for :math:`u` to be considered active/excited
    :param t: list of points in time, corresponding to the first index of ``u``. If this is zero, the temporal index will be used as time.
    :param it: index of the frame to calculate elapsed time for
    :param _return_arrival: internal switch, if True, return arrival time instead
    :return: the arrival time, an array of the same shape as ``u[0]``, one frame of ``u``

    .. seealso::
        :py:func:`time_arrival_frame`: calculate arrival time for just one frame (slightly faster)

        :py:func:`time_elapsed`: calculate elapsed time for all frames (much slower)

        :py:func:`time_elapsed_two`: calculate elapsed time for all frames with two thresholds ``u_iso`` (even slower)
    """
    if u.shape[0] <= 2:
        raise ValueError(f"The number of steps in time Nt=u.shape[0]={u.shape[0]} is too small!")
    t = t if t is not None else np.arange(u.shape[0])
    arrival = np.full(u.shape[1:], -np.inf)
    it = it % u.shape[0]
    isfin = [False]
    while it > 0 and not np.all(isfin):
        isfin = np.isfinite(arrival)
        arrival[~isfin & (u[it] > u_iso) & ~(u[it-1] > u_iso)] = t[it]
        it -= 1
    if _return_arrival:
        return arrival # arrival time
    else:
        t = np.reshape(t, (u.shape[0], *[1]*(u.ndim - 1)))
        with np.errstate(invalid="ignore"):
            return t - arrival # elapsed time

def time_arrival_frame(u:np.ndarray, *args, **kwargs) -> np.ndarray:
    """
    Calculate the arrival time for one frame of the variable ``u``. This uses
    the same algorithm as outlined in :py:func:`time_elapsed_frame`.

    :param u: passed on to :py:func:`time_elapsed`
    :param args: passed on to :py:func:`time_elapsed`
    :param kwargs: passed on to :py:func:`time_elapsed`
    :return: the arrival time, an array of the same shape as u[0], one frame of u
    """
    return time_elapsed_frame(u, *args, **kwargs, _return_arrival=True)

def time_elapsed(u:np.ndarray, u_iso:float=0.65, t:Union[List[float],None]=None, _return_arrival:bool=False) -> np.ndarray:
    """
    Calculate elapsed time for all frames of ``u``
    when ``u`` rises above threshold ``u_iso``.

    Elapsed time is the time since the last arrival of the activation front at
    each element in the variable :math:`u`.

    The arrival time is defined as the point in time, when the activation front
    pushes the variable :math:`u` above the threshold :math:`u_\\mathrm{iso}`.

    The first index of the array in ``u`` is interpreted as the index in time.
    The calculation of the elapsed time involves iteration over this index.

    .. note::
        The value ``-np.inf`` is used before any wave arrives.

    :param u: variable describing the activity of the medium. In the context of the bi-domain description in cardiology, this is the transmembrane voltage.
    :param u_iso: threshold for :math:`u` to be considered active/excited
    :param t: list of points in time, corresponding to the first index of ``u``. If this is zero, the temporal index will be used as time.
    :param _return_arrival: internal switch, if True, return arrival time instead
    :return: the elapsed time, an array of the same shape as u

    .. seealso::
        :py:func:`time_arrival`: calculate arrival time for just one frame (slightly faster)

        :py:func:`time_elapsed_frame`: calculate elapsed time for just one frame (much faster)

        :py:func:`time_elapsed_two`: calculate elapsed time for all frames with two thresholds ``u_iso`` (slower)
    """
    if u.shape[0] <= 2:
        raise ValueError(f"The number of steps in time Nt=u.shape[0]={u.shape[0]} is too small!")
    t = np.reshape(t if t is not None else np.arange(u.shape[0]), (u.shape[0], *[1]*(u.ndim - 1)))
    act = u > u_iso # region where above threshold
    act = act & ~np.roll(act, 1, axis=0) # line where jumping above threshold
    act[0] = False
    arrival = np.maximum.accumulate(np.where(act, t, -np.inf), axis=0)
    if _return_arrival:
        return arrival # arrival time
    else:
        with np.errstate(invalid="ignore"):
            return t - arrival # elapsed time

def time_arrival(u:np.ndarray, *args, **kwargs) -> np.ndarray:
    """
    Calculate the arrival time for the variable ``u``. This uses the same
    algorithm as outlined in :py:func:`time_elapsed`.

    :param u: passed on to :py:func:`time_elapsed`
    :param args: passed on to :py:func:`time_elapsed`
    :param kwargs: passed on to :py:func:`time_elapsed`
    :return: the arrival time, an array of the same shape as u
    """
    return time_elapsed(u, *args, **kwargs, _return_arrival=True)

def time_elapsed_two(u:np.ndarray, u_iso:Tuple[float,float], t:Union[List[float],None]=None, _return_arrival:bool=False) -> np.ndarray:
    """
    Calculate elapsed time for all frames of ``u`` where (re-)excitation takes
    place when ``u`` rises above threshold ``u_iso[1]`` only if ``u`` has gone
    below the lower threshold ``u_iso[0]`` since the last excitation.

    Elapsed time is the time since the last arrival of the activation front at
    each element in the variable :math:`u`.

    The arrival time is defined as the point in time, when the activation front
    pushes the variable :math:`u` above the threshold :math:`u_\\mathrm{iso}`.

    The first index of the array in ``u`` is interpreted as the index in time.
    The calculation of the elapsed time involves iteration over this index.

    .. note::
        The value ``-np.inf`` is used before any wave arrives.

    :param u: variable describing the activity of the medium. In the context of the bi-domain description in cardiology, this is the transmembrane voltage.
    :param u_iso: thresholds for :math:`u` to be considered active/excited. The tuple will be orderd such that ``u_iso[0]`` is the lower value and ``u_iso[1]`` the higher value.
    :param t: list of points in time, corresponding to the first index of ``u``. If this is zero, the temporal index will be used as time.
    :param _return_arrival: internal switch, if True, return arrival time instead
    :return: the elapsed time, an array of the same shape as u

    .. seealso::
        :py:func:`time_arrival_two`: calculate arrival time in the same way

        :py:func:`time_elapsed`: calculate elapsed time for all frames with just one threshold (faster)

        :py:func:`time_elapsed_frame`: calculate elapsed time for just one frame (even faster)
    """
    t = np.reshape(t if t is not None else np.arange(u.shape[0]), (u.shape[0], *[1]*(u.ndim - 1)))

    if not u_iso[0] < u_iso[1]:
        u_iso = u_iso[1::-1]
    time_inact = tuple(time_elapsed(-u, u_iso=-u_iso[i], t=t) for i in range(2))

    act = u > u_iso[1]
    act[~act & (time_inact[0] > time_inact[1])] = True # suppress false re-excitation due to noise
    act = act & ~np.roll(act, 1, axis=0) # line where jumping above threshold
    act[0] = False
    arrival = np.maximum.accumulate(np.where(act, t, -np.inf), axis=0)

    if _return_arrival:
        return arrival # arrival time
    else:
        with np.errstate(invalid="ignore"):
            return t - arrival # elapsed time

def time_arrival_two(u:np.ndarray, *args, **kwargs) -> np.ndarray:
    """
    Calculate the arrival time for the variable ``u``. This uses the same
    algorithm as outlined in :py:func:`time_elapsed_two`.

    :param u: passed on to :py:func:`time_elapsed_two`
    :param args: passed on to :py:func:`time_elapsed_two`
    :param kwargs: passed on to :py:func:`time_elapsed_two`
    :return: the arrival time, an array of the same shape as u
    """
    return time_elapsed_two(u, *args, **kwargs, _return_arrival=True)

def duration(u:np.ndarray, t:Union[np.ndarray,None]=None, u_iso:float=0.3):
    """
    Calculate the duration between last activation and deactivation for each frame in time of the field ``u``.
    This corresponds to the *action potential duration* in the case of cardiac electrophysiology, where ``u`` is the transmembrane potential.

    :param u: field of activity; its first index corresponds to time
    :param t: list of points in time, i.e. ``t.shape[0] == u.shape[0]``. By default, the first index is used as ``t``.
    :param u_iso: activation threshold for ``u``
    :return: three fields of same shape as ``u``: duration, activation times, deactivation times

    .. note::
        The sign of the returned duration corresponds to whether activation is followed by deactivation (+) or the other way around (-).
        For cardiac transmembrane potentials ``u``, positive sign therefore denotes APD and negative sign *diastolic interval* DI.
        To discard DI and obtain APD wherever possible, use this::

            import numpy as np
            from ithildin.pulse import duration
            dur = duration(u)[0]
            apd = np.where(dur>0, dur, np.nan)

    """
    t_act = time_arrival(u, t=t, u_iso=u_iso)
    t_deact = time_arrival(-u, t=t, u_iso=-u_iso)
    with np.errstate(invalid="ignore"):
        return t_deact - t_act, t_act, t_deact

def velocity(t_act:np.ndarray, x:np.ndarray, y:np.ndarray, dist:float=10, maxdt:float=5):
    """
    Calculate the velocity of a pulse based on a local arrival time map using the method published by Bayly et al. [1].

    :param t_act: local arrival time or elapsed time in 2D at just one point in time, i.e. an array with just two dimensions, see :py:meth:`time_elapsed`
    :param x: grid coordinates for the x direction, same shape as ``t_act``
    :param y: grid coordinates for the y direction, same shape as ``t_act``
    :param dist: determines the neighbourhood in space that is used for smoothing, see :py:meth:`neighbourhood`
    :param maxdt: exclude areas with jumps in ``t_act`` greater than this duration
    :return: two fields of same shape as ``t_act``: velocity, smoothed approximation of t_act

    Sources:

    1. Bayly, Philip V., et al. "Estimation of conduction velocity vector fields from epicardial mapping data." IEEE transactions on biomedical engineering 45.5 (1998): 563-571.
    """
    assert(t_act.ndim == 2)
    assert(t_act.shape == x.shape == y.shape)
    offsets = np.array([(0,0), *neighbourhood(dim=2, dist=dist)])

    deltas = np.multiply((y[1,1] - y[0,0], x[1,1] - x[0,0]), offsets)
    a = np.stack([np.ones_like(deltas[:,0]).flatten(), deltas[:,1].flatten(), deltas[:,0].flatten()], axis=-1)
    b = np.stack([np.roll(np.where(np.isfinite(t_act), t_act, np.nan), offset, axis=(-2, -1)).flatten() for offset in offsets], axis=0)

    params = np.linalg.lstsq(a=a, b=b, rcond=None)[0]

    approx = params[0]
    vel = np.linalg.norm(params[1:], axis=0)**-1

    mask = np.all(np.isfinite(b), axis=0) * (np.nanmax(b, axis=0) - np.nanmin(b, axis=0) <= maxdt)
    approx[~mask] = np.nan
    vel[~mask] = np.nan

    approx.shape = vel.shape = t_act.shape

    dist = int(np.max(np.abs(offsets)))
    approx = delete_border(approx, [-2, -1], dist)
    vel = delete_border(vel, [-2, -1], dist)
    return vel, approx
