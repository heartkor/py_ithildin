#!/bin/env python3
"""
Command Line  Interface
-----------------------

Perform some common tasks for analysing ``ithildin`` results from the command line.
"""

import click
import warnings
from typing import Union

@click.group()
def tools():
    pass

@tools.command()
@click.argument("stem", required=True, type=str)
@click.argument("outfile", required=True, type=str)
@click.option("-a", "--algo", default="arr", type=str, help="phase algorithm to use")
@click.option("-k", "--kwargs", default="{}", type=str, help="JSON string encoding a dictionary of keyword algorithms to pass to ``simdata.phase``")
def phase(stem:str, outfile:str, algo:str="arr", var1:str="u", var2:str="1-v", kwargs:str="{}"):
    """
    Calculate phase based on SimData and write to ``OUTFILE``.

    The argument ``STEM`` is the path and beginning of filenames, i.e.
    ``results/myseries_7`` for ``results/myseries_7_log.txt``.
    """
    from . import SimData
    import numpy as np
    import json
    simdata = SimData.from_stem(stem)
    print("loaded", simdata)
    phase = np.lib.format.open_memmap(outfile, dtype=np.float32, mode="w+", shape=simdata.shape)
    print(f"calculating phase and writing to file {outfile}...")
    phase[:] = simdata.phase(algo=algo, u=var1, v=var2, **json.loads(kwargs))

@tools.command()
@click.argument("stem", required=True, type=str)
@click.argument("outfile", required=True, type=str)
@click.option("-a", "--algo", default="mpg", type=str, help="phase defect algorithm to use")
@click.option("-p", "--phase", default="phiarr", type=str, help="name of variable containing the phase")
@click.option("-k", "--kwargs", default="{}", type=str, help="JSON string encoding a dictionary of keyword algorithms to pass to ``simdata.phase_defect``")
@click.option("-e", "--edge", default=0, type=int, help="If bigger than zero, calculate sigma for the axis with the given index. 0->rho, 1->sigma_z, 2->sigma_y, 3->sigma_x.")
@click.option("-n", "--nan", default=None, type=float, help="If not None, non-finite values will be replaced by this value.")
def phase_defect(stem:str, outfile:str, algo:str="mpg", phase:str="phiarr", edge:int=0, kwargs:str="{}", nan:Union[float,None]=None):
    """
    Calculate phase defect field rho or sigma based on SimData and write to ``OUTFILE``.

    The argument ``STEM`` is the path and beginning of filenames, i.e.
    ``results/myseries_7`` for ``results/myseries_7_log.txt``.
    """
    from . import SimData
    import numpy as np
    import json
    simdata = SimData.from_stem(stem)
    print("loaded", simdata)
    defect = np.lib.format.open_memmap(outfile, dtype=np.float32, mode="w+", shape=simdata.shape)
    if edge > 0:
        print(f"calculating phase defect sigma_{edge} and writing to file {outfile}...")
        defect[:] = simdata.phase_edge_defect(algo=algo, phase=simdata.vars.get(phase), axes=[edge], **json.loads(kwargs))[0]
    else:
        print(f"calculating phase defect rho and writing to file {outfile}...")
        defect[:] = simdata.phase_defect(algo=algo, phase=simdata.vars.get(phase), **json.loads(kwargs))

    if nan is not None:
        defect[~np.isfinite(defect)] = nan

@tools.command()
@click.argument("stem", required=True, type=str)
@click.argument("varname", default="u", type=str)
@click.option("--it", default=None, type=int, help="index in time")
@click.option("--iz", default=None, type=int, help="index in z-direction")
@click.option("--iy", default=None, type=int, help="index in y-direction")
@click.option("--ix", default=None, type=int, help="index in x-direction")
@click.option("-o", "--outfile", default=None, type=str, help="filename to save plot to")
@click.option("-d", "--dpi", default=180, type=int, help="dots per inch for the figure")
@click.option("-c", "--colormap", default="inferno", type=str, help="name of Matplotlib colormap")
@click.option("--vmin", default=None, type=float, help="minimum value for colouring")
@click.option("--vmax", default=None, type=float, help="maximum value for colouring")
def plot(stem:str, outfile:Union[str,None]=None, dpi:int=180, colormap:str="inferno", *args, **kwargs):
    """
    Plot one variable of simulation data in 1D, 2D, or 3D, depending on how many indices are given.

    The argument ``STEM`` is the path and beginning of filenames, i.e.
    ``results/myseries_7`` for ``results/myseries_7_log.txt``.

    Use ``VARNAME`` to pick the variable of ``simdata.vars`` to draw, defaults to ``"u"``.
    """
    import matplotlib.pyplot as plt
    plt.set_cmap(colormap)
    from . import SimData
    simdata = SimData.from_stem(stem)
    print("loaded", simdata)
    simdata.plot(*args, **kwargs)
    if outfile:
        plt.savefig(outfile, dpi=dpi)
    else:
        plt.show()

@tools.command()
@click.argument("stem", required=True, type=str)
@click.argument("varname", default="u", type=str)
@click.option("-o", "--outfile", default=None, type=str, help="filename to save plot to")
@click.option("-d", "--dpi", default=180, type=int, help="dots per inch for the figure")
@click.option("-c", "--colormap", default="inferno", type=str, help="name of Matplotlib colormap")
@click.option("--vmin", default=None, type=float, help="minimum value for colouring")
@click.option("--vmax", default=None, type=float, help="maximum value for colouring")
@click.option("--start-frame", "--it0", help="from which frame to start", type=int, default=0)
@click.option("--end-frame",   "--it1", help="to which frame", type=int, default=None)
@click.option("--frame-step",  "--its", help="how many frames to step between plots", type=int, default=1)
@click.option("-q", "--quiet", is_flag=True, show_default=True, default=False, help="less output")
def movie(stem:str, outfile:Union[str,None]=None, dpi:int=180, colormap:str="inferno", varname:str="u", start_frame:int=0, end_frame:Union[int,None]=None, frame_step:int=1, quiet:bool=False, **kwargs):
    """
    Make a 2D movie over time at the slice ``iz=Nx//2``.

    The argument ``STEM`` is the path and beginning of filenames, i.e.
    ``results/myseries_7`` for ``results/myseries_7_log.txt``.

    Use ``VARNAME`` to pick the variable of ``simdata.vars`` to draw, defaults to ``"u"``.
    """
    import matplotlib.pyplot as plt
    plt.set_cmap(colormap)
    from . import SimData
    from .plot import movie
    simdata = SimData.from_stem(stem)
    if not quiet: print("loaded", simdata)
    fig = plt.figure(dpi=dpi)
    ax = fig.add_subplot()
    if len(simdata.shape) >= 4:
        kwargs['iz'] = simdata.shape[1]//2

    Nfr = simdata.shape[0]

    if start_frame is not None:
        if start_frame <= -Nfr:
            start_frame = 0
        elif start_frame < 0:
            start_frame = Nfr + start_frame
        elif start_frame >= Nfr:
            start_frame = Nfr-1

    if end_frame is not None:
        if end_frame <= -Nfr:
            end_frame = 0
        elif end_frame < 0:
            end_frame = Nfr + end_frame
        elif end_frame >= Nfr:
            end_frame = Nfr-1

    kwplots = [dict(it=i, **kwargs) for i in range(start_frame, end_frame or simdata.shape[0], frame_step)]
    aplots = [[varname]]*simdata.shape[0]
    movie(fig, ax, simdata.plot, aplots, kwplots, filename=outfile, verbose=not quiet)

@tools.command()
@click.argument("infile", required=True, type=str)
@click.argument("outfile", required=True, type=str)
def convert(infile:str, outfile:str):
    """
    Convert a var file in IJK order to a npy file in KJI order.
    """
    import numpy as np
    from .varfile import var_to_npy

    if infile[-4:] != ".var": warnings.warn("The input filename does not end with '.var'!")
    if outfile[-4:] != ".npy": warnings.warn("The input filename does not end with '.npy'!")

    dtype = np.int32 if infile.find("_inhom.") >= 0 else np.float32

    print(f"converting {infile} -> {outfile}, dtype={dtype}...")
    var_to_npy(infile, outfile, dtype=dtype)

@tools.command()
@click.argument("stem", required=True, type=str)
@click.argument("outfile", required=True, type=str)
@click.option("-v", "--vars", "subject", flag_value="vars", default=True, help="export variables / fields")
@click.option("-f", "--filaments", "subject", flag_value="filaments", help="export filaments")
@click.option("-t", "--trajectories", "subject", flag_value="trajectories", help="export trajectories")
def xdmf(stem:str, outfile:str, subject:str):
    """
    Generate XDMF files for the simulation data with the given stem.

    The argument ``STEM`` is the path and beginning of filenames, i.e.
    ``results/myseries_7`` for ``results/myseries_7_log.txt``.
    """
    from . import SimData, filament, xdmf
    simdata = SimData.from_stem(stem)
    print("loaded", simdata)

    if subject == "vars":
        print("exporting SimData as XDMF...")
        from os.path import dirname
        content = simdata.to_xdmf(dirname(outfile))
    elif subject == "filaments":
        print("converting filaments to XDMF...")
        content = xdmf.xdmf_filaments(simdata.filaments)
    elif subject == "trajectories":
        print("finding trajectories...")
        simdata.calc_trajectories()
        simdata.trajectories = [filament.trajectory_add_times(traj, simdata.times) for traj in simdata.trajectories]
        print("converting trajectories to XDMF...")
        content = xdmf.xdmf_trajectories(simdata.trajectories)
    else:
        raise ValueError("Invalid subject to export to XDMF!")

    print(f"writing to file {outfile}...")
    xdmf.xdmf_write(outfile, content)

@tools.command()
@click.argument("stem", required=True, type=str)
@click.argument("varname", required=False, default=None, type=str)
def info(stem:str, outfile:Union[str,None]=None, varname:Union[str,None]=None):
    """
    Plot one variable of simulation data in 1D, 2D, or 3D, depending on how many indices are given.

    The argument ``STEM`` is the path and beginning of filenames, i.e.
    ``results/myseries_7`` for ``results/myseries_7_log.txt``.

    Use ``VARNAME`` to pick the variable of ``simdata.vars`` to draw, defaults to ``"u"``.
    """
    from . import SimData
    from .varfile import ndarray_info
    import yaml
    simdata = SimData.from_stem(stem)
    if varname is None:
        s = str(simdata)
    else:
        v = simdata.vars[varname]
        info = ndarray_info(v)
        s = yaml.safe_dump({varname: info})
    if outfile:
        with open(outfile, 'w') as f:
            f.write(s)
    else:
        print(s)

if __name__ == "__main__":
    tools()
