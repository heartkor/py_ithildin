#!/bin/env python3
"""
Ithildin module for Python
==========================

This module offers tools to read, analyse and draw the simulation results of
the C++ computational cardiology framework ithildin.
"""

from . import defect, filament, graph, log, pdl, phase, plot, pulse, points, simdata, topology, varfile, xdmf
from .filament import Filament
from .pdl import PDL
from .points import Points, Line, Lines
from .log import Log
from .simdata import SimData

def sd(*args, **kwargs) -> SimData:
    """
    Construct SimData from stem by calling ``SimData.from_stem(*args, **kwargs)``

    :param args: arguments are passed on to ``SimData.from_stem``
    :param kwargs: keyword arguments are passed on to ``SimData.from_stem``
    :return: new :py:class:`SimData` instance
    """
    return SimData.from_stem(*args, **kwargs)
