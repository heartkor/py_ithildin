#!/bin/env python3
"""
Phase of cyclic wave-like processes
-----------------------------------

When studying periodic/cyclic processes, the term *phase* refers to an angle
:math:`\\phi \\in [0,2\\pi)` that increases as the process progresses. The
angle 0 often refers to the initial/resting state. As the process is periodic,
it returns to (roughly) the initial state. This return is captured by a phase
angle increasing past :math:`2\\pi`, and therefore returning to 0, too.

This submodule contains algorithms to calculate different kinds of phases and
related functions to work with such phase angles. The two main algorithms are:

    * :py:func:`phase_state_space` angle in the 2D state space
    * :py:func:`phase_elapsed` phase based on the time since the last activation

"""
import numpy as np
from typing import Union, Callable

from . import pulse
time_elapsed = pulse.time_elapsed
time_arrival = pulse.time_arrival

def normalise(phase:np.ndarray) -> np.ndarray:
    """
    Transform phase to interval [0,2pi) element-wise for all entries in the
    input array.

    :param phase: an array containing phases in radians
    :return: phase in radians in the interval [0,2pi)
    """
    return np.mod(phase, 2*np.pi)

def phase_state_space(u:np.ndarray, v:np.ndarray, u_iso:float=0.65, v_iso:float=0.8) -> np.ndarray:
    """
    Calculate the phase described by the 2D state space spanned by the
    variables :math:`u` and :math:`v`.  This phase is the angle around the
    point :math:`(u_\\mathrm{iso}, v_\\mathrm{iso})`.  The point :math:`(u,v) =
    (0, 0)` corresponds to phase angle 0.

    This function acts element-wise, so any shapes for ``u`` and ``v`` can be
    converted to an array describing phase of the same shape.

    :param u: first variable
    :param v: second variable
    :param u_iso: iso-value of the first variable
    :param v_iso: iso-value of the second variable
    :return: phase in radians in the interval [0, 2pi).
    """
    phase_zero_offset = np.arctan2(v_iso, u_iso) + np.pi
    phase = np.arctan2(v - v_iso, u - u_iso) - phase_zero_offset
    return normalise(phase)

def phase_activation(*args, **kwargs) -> np.ndarray:
    """
    The state space phase is sometimes referred to as the activation phase, see
    :py:func:`phase_state_space` for details.

    :param args: passed on to :py:func:`phase_state_space`
    :param kwargs: passed on to :py:func:`phase_state_space`
    :return: the state space phase returned by :py:func:`phase_state_space`
    """
    return phase_state_space(*args, **kwargs)

def phase_rate_angle(u:np.ndarray, v:np.ndarray, axis:int=0, offset:float=0.) -> np.ndarray:
    """
    Calculate the phase defined by the between angle of the rates of change of
    the state variables:

    .. math::

            \\phi_\\mathrm{ran}(u, v) = \\arg(\\partial_t u + \\mathrm i \\partial_t v) + \\phi_0

    This definition was used by Marcotte *et al.* [1]_ for the Karma model of
    cardiac excitation [2]_ [3]_.

    :param u: first variable
    :param v: second variable
    :param axis: index of the temporal axis to calculate the derivative along
    :param offset: a constant offset :math:`\\phi_0` for the phase
    :return: phase in radians in the interval [0, 2pi).

    .. [1] Marcotte, Christopher D., and Roman O. Grigoriev. "Dynamical
        mechanism of atrial fibrillation: A topological approach." Chaos: An
        Interdisciplinary Journal of Nonlinear Science 27.9 (2017): 093936.

    .. [2] Karma, Alain. "Spiral breakup in model equations of action potential
        propagation in cardiac tissue." Physical review letters 71.7 (1993): 1103.

    .. [3] Karma, Alain. "Electrical alternans and spiral wave breakup in
        cardiac tissue." Chaos: An Interdisciplinary Journal of Nonlinear Science
        4.3 (1994): 461-472.
    """
    du = (np.roll(u, -1, axis) - u)
    dv = (np.roll(v, -1, axis) - v)
    phase = np.angle(du + 1j*dv) + offset
    return normalise(phase)

def phase_elapsed_linear(u:np.ndarray, dur:float=100, time_elapsed:Union[Callable,np.ndarray]=time_elapsed, **kwargs) -> np.ndarray:
    """
    Calculate the elapsed time phase :math:`\\phi_\\mathrm{arr}` which is based on the
    time :math:`t_\\mathrm{elapsed}` since the last jump over a threshold as follows:

    .. math::

            \\phi_\\mathrm{arr}(u) = 2 \\pi \\mathrm{clip}_{[0,1]}(t_\\mathrm{elapsed}(u) / \\tau)

    with the characteristic time of the cyclic process :math:`\\tau = 0.5 \\mathrm{APD}`
    using the action potential duration APD ``dur``.

    :param u: variable describing the activity of the medium. In the context of the bi-domain description in cardiology, this is the transmembrane voltage rescaled to the interval [0, 1].
    :param dur: Typical duration between activation and deactivation. In the context of cardiology, this is the typical action potential duration in the medium.
    :param time_elapsed: function for calculating :math:`t_\\mathrm{elapsed}`, by default :py:func:`time_elapsed`. If this is not a callable, this is an array of the elapsed time. ``u`` is not used in this case.
    :param kwargs: will be passed to ``time_elapsed`` for calculating :math:`t_\\mathrm{elapsed}`
    :return: phase in radians in the interval :math:`[0, 2\\pi]`.
    """
    tela = time_elapsed(u, **kwargs) if callable(time_elapsed) else time_elapsed
    return 2*np.pi*np.clip(tela/(0.5*dur), 0, 1)

def phase_elapsed_arctan(u:np.ndarray, tau:float=0.1, time_elapsed:Union[Callable,np.ndarray]=time_elapsed, **kwargs) -> np.ndarray:
    """
    Calculate the elapsed time phase :math:`\\phi_\\mathrm{arr}` which is based on the
    time :math:`t_\\mathrm{elapsed}` since the last jump over a threshold as follows:

    .. math::

            \\phi_\\mathrm{arr}(u) = 4 \\arctan(\\tau \\, t_\\mathrm{elapsed}(u))

    :param u: variable describing the activity of the medium. In the context of the bi-domain description in cardiology, this is the transmembrane voltage rescaled to the interval [0, 1].
    :param tau: characteristic frequency of the cyclic process. In the context of cardiology, this is related to the typical action potential duration in the medium.
    :param time_elapsed: function for calculating :math:`t_\\mathrm{elapsed}`, by default :py:func:`time_elapsed`. If this is not a callable, this is an array of the elapsed time. ``u`` is not used in this case.
    :param kwargs: will be passed to :py:func:`time_elapsed` for calculating :math:`t_\\mathrm{elapsed}`
    :return: phase in radians in the interval :math:`[0, 2\\pi)`.
    """
    tela = time_elapsed(u, **kwargs) if callable(time_elapsed) else time_elapsed
    return normalise(4*np.arctan(tau*tela))

def phase_elapsed(u:np.ndarray, dur:float=100, time_elapsed:Union[Callable,np.ndarray]=time_elapsed, **kwargs) -> np.ndarray:
    """
    Calculate the elapsed time phase :math:`\\phi_\\mathrm{arr}` which is based on the
    time :math:`t_\\mathrm{elapsed}` since the last jump over a threshold as follows:

    .. math::

            \\phi_\\mathrm{arr}(u) = 2 \\pi \\tanh(t_\\mathrm{elapsed}(u) / \\tau)

    with the characteristic time of the cyclic process :math:`\\tau = 0.5 \\mathrm{APD}`
    using the action potential duration APD ``dur``.

    Source: Arno L, Quan J, Nguyen NT, Vanmarcke M, Tolkacheva EG, Dierckx H. A Phase Defect Framework for the Analysis of Cardiac Arrhythmia Patterns. Front Physiol. 2021;12. doi:10.3389/fphys.2021.690453.

    :param u: variable describing the activity of the medium. In the context of the bi-domain description in cardiology, this is the transmembrane voltage rescaled to the interval [0, 1].
    :param dur: Typical duration between activation and deactivation. In the context of cardiology, this is the typical action potential duration in the medium.
    :param time_elapsed: function for calculating :math:`t_\\mathrm{elapsed}`, by default :py:func:`time_elapsed`. If this is not a callable, this is an array of the elapsed time. ``u`` is not used in this case.
    :param kwargs: will be passed to ``time_elapsed`` for calculating :math:`t_\\mathrm{elapsed}`
    :return: phase in radians in the interval :math:`[0, 2\\pi)`.
    """
    tela = time_elapsed(u, **kwargs) if callable(time_elapsed) else time_elapsed
    return normalise(2*np.pi*np.tanh(tela/(0.5*dur)))

def phase_arrival(*args, **kwargs) -> np.ndarray:
    """
    The elapsed time phase is sometimes referred to as the arrival time phase,
    see :py:func:`phase_elapsed` for details.

    :param args: passed on to :py:func:`phase_elapsed`
    :param kwargs: passed on to :py:func:`phase_elapsed`
    :return: the elapsed time phase returned by :py:func:`phase_elapsed`
    """
    return phase_elapsed(*args, **kwargs)

def skew(phase:np.ndarray, p0:float=0.35, p1:float=0.55) -> np.ndarray:
    """
    Skew a phase by setting all phases below :math:`2 \\pi p_0` to 0 and all
    above :math:`2 \\pi p_1` to :math:`2\\pi`. Transition linearly between
    those upper and lower values.

    One can skew the state space phase this way to obtain a computationally
    cheap approximation of the arrival time phase.

    Example: Calculate phase for one frame of a simulation::

        phase = phase_elapsed(u)[it] # expensive!
        phase = skew(phase_state_space(u[it], v[it])) # fast

    :param phase: the phase to be skewed
    :param p0: lower boundary, :math:`0 \\le p_0 \\le 1`
    :param p1: upper boundary, :math:`0 \\le p_1 \\le 1`
    :return: skewed phase, same shape as input phase
    """
    return 2*np.pi*np.clip((phase/(2*np.pi) - p0)/(p1 - p0), 0., 1.)

def phase_simdata(simdata:"SimData", algo:Union[str,None]=None, u:Union[str,np.ndarray]="u", v:Union[str,np.ndarray]="1-v", **kwargs) -> np.ndarray:
    """
    Calculate the phase :math:`\\phi(t, \\vec x)` based on simulation data.

    :param simdata: simulation data for which to calculate the phase
    :param algo: the algorithm to use to calculate the phase
    :param u: first variable to use, if string, use it to index ``simdata.vars``
    :param v: first variable to use, if string, use it to index ``simdata.vars`` (ignored for some algorithms). If ``v="1-v"``, set ``v=1-simdata.vars["v"]``
    :param kwargs: passed on to the method to calculate the phase
    :return: the phase field :math:`\\phi(t, \\vec x)`, an array of the same size as ``u``
    """
    algorithms = dict(
            default="skew", skew="skew",
            arr="arr", arrival="arr", elapsed="arr",
            arrlin="arrlin",
            act="act", activation="act", state="act",
            ran="ran", rate_angle="ran", rateangle="ran", marcotte="ran", marcotte2017="ran",
    )
    if algo not in algorithms:
        raise ValueError(f"Invalid algorithm {algo}! Must be one of {list(algorithms.keys())}.")
    algo = algorithms[algo or "default"]

    if algo.startswith("arr"):
        phase = dict(arr=phase_arrival, arrlin=phase_elapsed_linear)[algo]
        return phase(
                simdata.vars[u] if isinstance(u, str) else u,
                t=simdata.times,
                time_elapsed=pulse.time_elapsed_two if "u_iso" in kwargs and isinstance(kwargs["u_iso"], (tuple, list)) else pulse.time_elapsed,
                **kwargs,
        )
    elif algo in ["act", "ran"]:
        u = (1 - simdata.vars[u[2:]] if u[:2] == "1-" else simdata.vars[u]) if isinstance(u, str) else u
        v = (1 - simdata.vars[v[2:]] if v[:2] == "1-" else simdata.vars[v]) if isinstance(v, str) else v
        if algo == "act":
            return phase_activation(u, v, **kwargs)
        elif algo == "ran":
            return phase_rate_angle(u, v, **kwargs)
    elif algo == "skew":
        kwskew = dict()
        if "p0" in kwargs:
            kwskew["p0"] = kwargs["p0"]
            del kwargs["p0"]
        if "p1" in kwargs:
            kwskew["p1"] = kwargs["p1"]
            del kwargs["p1"]
        return skew(phase_simdata(simdata, algo="act", u=u, v=v, **kwargs), **kwskew)
    raise ValueError

def difference_phases(a:np.ndarray, b:np.ndarray):
    """
    Calculate the phase difference:

    .. math::

        U(\\phi_a,\\phi_b) = \\mod(\\phi_a - \\phi_b + \\pi, 2\\pi) - \\pi

    :param a: the minuend phase :math:`\\phi_a`
    :param b: the subtrahend phase :math:`\\phi_b`
    :return: the difference, an array of the same size as ``a``
    """
    return np.mod(a - b + np.pi, 2*np.pi) - np.pi
