#!/bin/env python3
"""
Visualisation with Matplotlib
-----------------------------

Collection of functions related to visualisation of various ithildin data.

These functions are supposed to be minimal. For more advanced plotting, create
a dedicated script.

Design of drawing functions is such that they can be plugged into the
movie-function, i.e. they can take an :py:class:`matplotlib.axes._axes.Axes` object
to draw on.
"""

from matplotlib.animation import FFMpegWriter
from matplotlib.collections import LineCollection
from typing import List, Dict, Tuple, Union, Callable
import cmcrameri
import colorsys
import datetime
import matplotlib
import matplotlib.patheffects
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import numpy as np
import sys
import warnings

for name, cmap in cmcrameri.cm.cmaps.items():
    if name not in plt.colormaps:
        matplotlib.colormaps.register(name=name, cmap=cmap)

plt.rc("image", cmap="inferno")
# plt.rc("text", usetex=True)
# plt.rc("font", family="serif")

def is_unpackable(obj) -> bool:
    try:
        _ = (*obj, )
        return True
    except TypeError:
        return False

def movie_cf(*args, fig:Union[plt.Figure,None]=None, ax:Union[plt.Axes,None]=None, **kwargs):
    """
    Generate a movie on the current figure. All arguments are passed on to :py:meth:`movie`.
    """
    if fig is None:
        fig = plt.gcf()
    if ax is None:
        ax = fig.gca()
    return movie(fig, ax, *args, **kwargs)

def movie(fig:plt.Figure, ax:plt.Axes, func:Callable, args:Union[List[List],None]=None, kwargs:Union[List[Dict],None]=None, fps:int=15, filename:Union[str,None]=None, title:str="{n}", artist:str="Python Ithildin", comment:str="automatically generated on {d}", verbose:bool=True, **additional_kwargs):
    """
    Generate a movie where each frame is a plot generated with :py:func:`func`.

    :param fig: figure to export as movie
    :param ax: axes to draw on, i.e. to pass to func
    :param func: this function is run for each frame
    :param args: an element in this list is the list of positional arguments
        to send to the plotting function at a frame
    :param kwargs: each element in this list is the dict of keyword arguments
        to send to the plotting function at a frame
    :param fps: number of frames per second
    :param filename: path, where to save the mp4 file
    :param title: mp4 metadata
    :param artist: mp4 metadata
    :param comment: mp4 metadata
    :param verbose: if True, show progress
    :param additional_kwargs: will be passed on to func

    Example: movie of a filament trajectory in 3D::

        import ithildin
        import matplotlib.pyplot as plt

        filaments = ithildin.filaments.read_filaments("tipdata.txt")
        trajectory = ithildin.filaments.filaments_to_trajectories(filaments)

        fig = plt.figure()
        ax = fig.add_subplot(projection="3d")
        ax.set_xlim(40, 60)
        ax.set_ylim(100, 120)
        ax.set_zlim(0, 6)
        ithildin.plot.movie(fig, ax, ith.plot.plot_filament, [[f] for f in trajectory])

    Example: movie of a slice of a variable in 2D::

        import ithildin
        import matplotlib.pyplot as plt

        simdata = ithildin.SimData.from_stem("series_7")

        fig = plt.figure(dpi=200)
        ax = fig.add_subplot()
        kwplots = [dict(it=i, iz=30) for i in range(simdata.shape[0])] # movie over time
        aplots = [[simdata, "u"]]*len(kwplots)
        ithildin.plot.movie(fig, ax, ith.plot.plot2d_var, aplots, kwplots)

    """
    assert(kwargs is not None or args is not None)
    if kwargs is None:
        kwargs = [dict()]*len(args)
    elif args is None:
        args = [[]]*len(kwargs)

    if not is_unpackable(args[0]):
        args = [[a] for a in args]

    name = func.__name__
    now = datetime.datetime.now()
    date = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}:{now.minute:02}:{now.second:02}"
    writer = FFMpegWriter(fps=fps, metadata=dict(title=title.format(n=name, d=date), artist=artist, comment=comment.format(n=name, d=date)))
    date = date.replace("-","").replace(":","").replace(" ","_")

    filename = filename or f"movie_{name}_{date}.mp4"
    if verbose: print(f"Saving movie to {filename}...", file=sys.stderr)
    with writer.saving(fig, filename, dpi=fig.dpi):
        frame = 0
        for a, kw in zip(args, kwargs):
            if verbose: print(f"frame {frame}, {name}(*{a}, **{kw})...", file=sys.stderr)
            ret = func(*a, **kw, **additional_kwargs, ax=ax)
            writer.grab_frame()
            for r in ret:
                if hasattr(r, "remove"):
                    r.remove()
                elif hasattr(r, "collections"):
                    for ri in r.collections:
                        ri.remove()
            frame += 1
    if verbose: print(f"Saved movie to {filename}.", file=sys.stderr)

def plot_filament(filament:"Filament", *args, ax:Union[plt.Axes,None]=None, color:str="c", marker:str="o", label:str="{o} filament of length {l} at $t$={t} ms", **kwargs) -> List["plt.Line2D"]:
    """
    Plot a filament in 2D or 3D depending on given ax object.
    Ordered filaments have lines joining them.
    Title, x-, y-, and z-label will be automatically set.

    :param filament: to be drawn
    :param args: will be passed to ax.plot
    :param ax: matplotlib 2D or 3D axes for plotting
    :param kwargs: will be passed to ax.plot
    :param color: which color to use for the line and markers
    :param marker: will be passed to ax.plot
    :param label: for legends

    :return: the lines that have been drawn

    .. note::

        some string substitutions will be done in :py:attr:`label`::

            {f} -> filament
            {t} -> filament.time
            {l} -> len(filament)
            {o} -> filament.ordered and "ordered" or ""

    Example: plot one filament in 3D::

        import ithildin
        import matplotlib.pyplot as plt
        filaments = ithildin.filaments.read_filaments("tipdata.txt")
        ith.plot.plot_filament(filaments[0])
        plt.show()
    """
    if not ax:
        ax = plt.gca()

    points = filament.points.T

    ax.set_title(format_dim_label(0, "${u}=$ {v} {u}", v=filament.time))
    ax.set_xlabel(format_dim_label(3))
    ax.set_ylabel(format_dim_label(2))

    if ax.name == "3d":
        ax.set_zlabel(format_dim_label(1))
        points = points[:3]
    else:
        points = points[:2]

    return ax.plot(*points, *args, color=color, marker=marker, ls="-" if filament.ordered else "", **kwargs, label=label.format(f=filament, t=filament.time, l=len(filament), o="ordered" if filament.ordered else ""))

def plot_pdl(pdl:"PDL", *args, ax:Union[plt.Axes,None]=None, color:str="k", marker:str=".", head_marker:str="o", plot_phase:bool=True, ordered:Union[int,None]=None, label:Union[str,None]=None, **kwargs) -> Tuple["plt.Line2D", "plt.Line2D", LineCollection]:
    """
    Plot a PDL in 2D depending on given ax object.
    Ordered PDLs have lines joining them.
    If phase information is stored, draw head and the phase around it.
    Title, x-, y-, and z-label will be automatically set.

    :param pdl: to be drawn
    :param args: will be passed to ax.plot
    :param ax: matplotlib 2D or 3D axes for plotting
    :param kwargs: will be passed to ax.plot
    :param color: which color to use for the line and markers
    :param marker: the marker to use if drawn as a point cloud
    :param head_marker: the marker to use for the head, set to ``None`` to disable
    :param plot_phase: switch whether to draw the nearby phase values on a contour around the PDL
    :param ordered: choose how to draw the PDL (``None``: follow ``ordered`` property of PDL,
        0: point cloud, 1: line, 2: line with coloured contour around it according to phase)
    :param label: for legends

    :return: the lines that have been drawn

    Example: plot one PDL in three different ways::

        import ithildin as ith
        import matplotlib.pyplot as plt
        pdl = ith.PDL([
                [0.5, 0.], [7., 4.5], [12.5, 9.], [17., 14.5],
                [20.5, 21.], [23., 28.5], [24., 36.5]
            ],
            res=[0.3, 0.3], time=125.5, ordered=2,
            phase_head=0.62, phase_tail=5.41,
            phase_left=[6.28, 6.28, 6.28, 6.28, 6.28, 6.28, 6.28],
            phase_right=[1.24, 2.38, 3.37, 4.17, 4.78, 5.02, 5.41],
        )
        for ordered in [None, 1, 0]:
            pdl.plot(ordered=ordered)
            plt.show()

    """
    if not ax:
        ax = plt.gca()
    if ordered is None:
        ordered = pdl.ordered
    x, y = pdl.points.T
    ret = ax.plot(x, y, *args, color=color, marker=(marker or "") if not ordered else "", ls="-" if ordered else "", **kwargs)
    if ordered >= 2:
        ret.extend(ax.plot(x[0], y[0], "ko"))
        contour = pdl.contour().points
        ret.append(colorline(
            *np.concatenate([contour, contour[:1]], axis=0).T,
            np.concatenate([pdl.nearby_phase, [pdl.phase_head]]),
            vmin=0, vmax=2*np.pi, cmap="twilight", ax=ax, setlims=False))
    return ret

def format_dim_label(dim:int, label:str="${name}$ in {unit}", **kwargs) -> str:
    """
    Format label as appropriate for given dimension:

    * ``{n} = {name}``: name of the dimension (t, z, y, x)
    * ``{u} = {unit}``: unit of the dimension (ms or mm)
    * additional replacements can be defined with ``kwargs``

    :param dim: dimension t, z, y, or x (KJI order)
    :param label: string to format
    :param kwargs: additional replacements
    :return: formatted string

    (TODO move this function to better place)
    """
    name = "tzyx"[dim]
    unit = ["ms", "mm", "mm", "mm"][dim]
    return label.format(n=name, name=name, u=unit, unit=unit, **kwargs)

def match_indices_with_shape(ind:List[int], shape:List[int]) -> Tuple[List[int], List[int], List[int]]:
    """
    Helper function that turns a list of indices [Nt, Nz, Ny, Nx] into:

    1. a list of index slices for use with a Numpy array
    2. which index dimensions are fixed
    3. which index dimensions are variable

    :param ind: list of indices, any of which may be None
    :param shape: shape of the array, the indices correspond to
    :return: list of index slices, list of fixed dimensions, list of loose dimensions
    """
    assert(len(ind) == len(shape))
    ind = [0 if (i is None and s == 1) else i for i, s in zip(ind, shape)]
    loose_dims = [d for d, i in enumerate(ind) if i is None]
    fix_dims = [d for d, i in enumerate(ind) if i is not None]
    ind = [i is None and slice(i) or i for i in ind]
    return ind, fix_dims, loose_dims

def plot_var(simdata:"SimData", varname:str, it:Union[int,None]=None, iz:Union[int,None]=None, iy:Union[int,None]=None, ix:Union[int,None]=None, ax:Union[plt.Axes,None]=None, *args, **kwargs):
    """
    Plot one variable of simulation data in 1D, 2D, or 3D, depending on how many indices are given.

    If defined, use ``simdata.mask`` to mask the data.

    :param simdata: simulation log needed for metadata
    :param varname: which variable of :py:attr:`simdata.vars` to draw
    :param it: index in time
    :param iz: index in z
    :param iy: index in y
    :param ix: index in x
    :param ax: matplotlib axes for plotting
    :param args: will be passed on
    :param kwargs: will be passed on

    :return: the objects that have been drawn
    """
    nplot = 0
    idx = [it, iz, iy, ix]
    for v in simdata.vars.values():
        shape = list(v.shape)
        while len(shape) < 4:
            shape.insert(1, 1)
        v.shape = shape
    for i, s in zip(idx, shape):
        if s > 1 and i is None:
            nplot += 1
    if nplot == 0:
        warnings.warn("0 dimensions selected for plot => skipping")
        return []
    func = [None, plot1d_var, plot2d_var, plot3d_var][nplot]
    return func(simdata=simdata, varname=varname, it=it, iz=iz, iy=iy, ix=ix, ax=ax, *args, **kwargs)

def plot1d_var(simdata:"SimData", varname:str, it:Union[int,None]=None, iz:Union[int,None]=None, iy:Union[int,None]=None, ix:Union[int,None]=None, ax:Union[plt.Axes,None]=None, *args, **kwargs) -> List[plt.Line2D]:
    """
    Plot one variable of simulation data at given indices as a line plot.

    If defined, use ``simdata.mask`` to mask the data.

    :param simdata: simulation log needed for metadata
    :param varname: which variable of simdata.vars to draw
    :param it: index in time
    :param iz: index in z
    :param iy: index in y
    :param ix: index in x
    :param ax: matplotlib 2D axes for plotting
    :param args: will be passed to ax.plot
    :param kwargs: will be passed to ax.plot

    :return: the lines that have been drawn
    """
    if not ax:
        ax = plt.gca()
    assert(ax.name == "rectilinear")

    var = simdata.vars[varname]
    ind, selected_dims, ax_dims = match_indices_with_shape([it, iz, iy, ix], var.shape)

    if len(ax_dims) != 1:
        raise ValueError(f"Exactly three indices have to be given, the other one None:\nit, iz, iy, ix = {ind}")

    var = np.where(simdata.mask[tuple(ind[1:])], var[tuple(ind)], np.nan)

    title = f"variable ${varname}$"
    ax.set_ylabel(title)
    for d in selected_dims:
        title += format_dim_label(d, ", ${n}=$ {v} {u}", v=simdata.linspace(d)[ind[d]])
    ax.set_title(title)
    ax.set_xlabel(format_dim_label(ax_dims[0]))

    if kwargs.get("vmin", None) and kwargs.get("vmax", None):
        ax.set_ylim(kwargs["vmin"], kwargs["vmax"])
    if "vmin" in kwargs:
        del kwargs["vmin"]
    if "vmax" in kwargs:
        del kwargs["vmax"]

    return ax.plot(simdata.linspace(ax_dims[0]), var, *args, **{"c": "b", **kwargs})

def plot2d_var(simdata:"SimData", varname:str, it:Union[int,None]=None, iz:Union[int,None]=None, iy:Union[int,None]=None, ix:Union[int,None]=None, ax:Union[plt.Axes,None]=None, *args, **kwargs) -> Tuple["matplotlib.colorbar.Colorbar", "matplotlib.image.AxesImage"]:
    """
    Plot a slice of one variable of simulation data at given indices.

    If defined, use ``simdata.mask`` to mask the data.

    :param simdata: simulation log needed for metadata
    :param varname: which variable of simdata.vars to draw
    :param it: index in time
    :param iz: index in z
    :param iy: index in y
    :param ix: index in x
    :param ax: matplotlib 2D axes for plotting
    :param args: will be passed to :py:meth:`ax.imshow`
    :param kwargs: will be passed to :py:meth:`ax.imshow`

    :return: the used colorbar and the drawn quadmesh

    Example: plot a slice through "u" at a given point in time and z value::

        import ithildin
        import matplotlib.pyplot as plt
        simdata = ithildin.SimData.from_stem("series_7")
        ith.plot.plot2d_var(simdata, "u", it=10, iz=30)
        plt.show()
    """
    if not ax:
        ax = plt.gca()
    assert(ax.name == "rectilinear")

    var = simdata.vars[varname]
    ind, selected_dims, ax_dims = match_indices_with_shape([it, iz, iy, ix], var.shape)

    if len(ax_dims) != 2:
        raise ValueError(f"Exactly two indices have to be given, the other two None:\nit, iz, iy, ix = {ind}")

    var = np.where(simdata.mask[tuple(ind[1:])], var[tuple(ind)], np.nan)

    title = f"variable ${varname}$"
    for d in selected_dims:
        title += format_dim_label(d, ", ${n}=$ {v} {u}", v=simdata.linspace(d)[ind[d]])

    ax_dims = list(reversed(ax_dims))

    ax.set_title(title)
    ax.set_xlabel(format_dim_label(ax_dims[0]))
    ax.set_ylabel(format_dim_label(ax_dims[1]))

    p0 = simdata.origin
    p1 = np.add(p0, simdata.physical_size)
    extent = (p0[ax_dims[0]], p1[ax_dims[0]], p0[ax_dims[1]], p1[ax_dims[1]])
    image = ax.imshow(var, *args, extent=extent, **{"origin":"lower", "aspect":"auto", "interpolation":"none", **kwargs})
    colorbar = ax.get_figure().colorbar(image, ax=ax)
    return colorbar, image


def plot3d_var(simdata:"SimData", varname:str, iso:float=0.5, it:Union[int,None]=None, iz:Union[int,None]=None, iy:Union[int,None]=None, ix:Union[int,None]=None, ax:Union[plt.Axes,None]=None, *args, **kwargs) -> Tuple["matplotlib.contour.QuadContourSet"]:
    """
    Plot contours of isosurface of one variable of simulation data at given
    indices.

    If defined, use ``simdata.mask`` to mask the data.

    :param simdata: simulation log needed for metadata
    :param varname: which variable of simdata.vars to draw
    :param iso: isovalue to use to get contour
    :param it: index in time
    :param iz: index in z
    :param iy: index in y
    :param ix: index in x
    :param ax: matplotlib 2D axes for plotting
    :param args: ignored
    :param kwargs: ignored

    :return: the used colorbar and the drawn quadmesh
    """
    if not ax:
        fig = plt.gcf()
        ax = fig.add_subplot(projection="3d")
    assert(ax.name == "3d")

    var = simdata.vars[varname]
    ind, selected_dims, ax_dims = match_indices_with_shape([it, iz, iy, ix], var.shape)

    if len(ax_dims) != 3:
        raise ValueError(f"Exactly one index has to be given, the other three None:\nit, iz, iy, ix = {ind}")

    var = np.where(simdata.mask[tuple(ind[1:])], var[tuple(ind)], np.nan)

    title = f"variable ${varname}$"
    for d in selected_dims:
        title += format_dim_label(d, ", ${n}=$ {v} {u}", v=simdata.linspace(d)[ind[d]])
    ax.set_title(title)
    ax.set_zlabel(format_dim_label(ax_dims[0]))
    ax.set_ylabel(format_dim_label(ax_dims[1]))
    ax.set_xlabel(format_dim_label(ax_dims[2]))

    Z, Y, X = simdata.meshgrid(*ax_dims, indexing="ij")
    ax.set_xlim(X[0,0,0], X[-1,-1,-1])
    ax.set_ylim(Y[0,0,0], Y[-1,-1,-1])
    ax.set_zlim(Z[0,0,0], Z[-1,-1,-1])
    zmin, zmax = Z[0,0,0], Z[-1,-1,-1]

    contours = []
    for i in range(0, len(X), max(len(X)//40, 1)):
        x, y, z = X[i], Y[i], Z[i][0,0]
        v = (var[i] > iso) - 0.5
        contours.append(ax.contour(x, y, v + z, zdir="z", levels=[z], vmin=zmin, vmax=zmax))
    return contours

def add_points(x:List[float], frac:float=0.25) -> List[float]:
    """
    Increase number of points in list by adding points representing the edges between the faces.
    For example::

        add_points([0, 1, 2])
        # returns [-0.25,  0.  ,  0.75,  1.  ,  1.75,  2.  ,  2.75]

    :param x: list of grid points
    :param frac: fraction between points at which to add the point representing an edge

    :return: list with the additional points

    .. seealso::
        Method :py:meth:`merge`
            combine arrays representing points, edges, and faces, to plot as a mesh
    """
    xx = []
    x.sort()
    xx.append(x[0] - frac*(x[1] - x[0]))
    xx.append(x[0])
    for x0, x1 in zip(x[:-1], x[1:]):
        dx = x1 - x0
        xx.append(x0 + (1 - frac)*dx)
        xx.append(x1)
    xx.append(x1 + (1 - frac)*dx)
    return np.array(xx)

def merge(*arrays:List[np.ndarray]) -> np.ndarray:
    """
    Combine arrays that represent values at points, edges, and faces into one merged array.

    :param arrays: arrays to merge, following binary order:

        * in 1D: `points0`, `edges1`
        * in 2D: `points00`, `edges01`, `edges10`, `faces11`
        * in 3D: `points000`, `edges001`, `edges010`, `faces011`, `edges100`, `faces101`, `faces110`, `volume111`

    :return: merged array with twice as many points in each dimension

    .. seealso::
        Method :py:meth:`add_points`
            add points representing the edges to a list of points, to plot as a mesh

    Example: using :py:meth:`merge` and :py:meth:`add_points` to plot data on edges::

        import matplotlib.pyplot as plt
        import numpy as np
        from ithildin.plot import merge, add_points
        x = np.linspace(-np.pi, np.pi, 20)
        y = np.linspace(0, 2, 10)
        dx = x[1] - x[0]
        dy = y[1] - y[0]
        X, Y = np.meshgrid(add_points(x), add_points(y))
        x, y = np.meshgrid(x, y)
        f = np.sin(x) + (x**2+y)**.5
        df = np.gradient(f, dy, dx, axis=(0, 1))
        Z = merge(f, *df) # f at points, df[0] at y edges, df[1] at x edges
        plt.pcolormesh(X, Y, Z)
        plt.show()

    """
    shape = next(a.shape for a in arrays if isinstance(a, np.ndarray))
    ndim = len(shape)
    y = np.empty([2*s for s in shape])
    y[:] = np.nan
    for i, x in enumerate(arrays):
        y[tuple(slice(int(d), None, 2) for d in np.binary_repr(i, width=ndim))] = x
    return y

def colorline(x:List[float], y:List[float], c:List[float], vmin:Union[float,None]=None, vmax:Union[float,None]=None, ax:Union[plt.Axes,None]=None, setlims:bool=False, *args, **kwargs) -> LineCollection:
    """
    Plot a fancy line ``(x, y)`` that changes colour according to to ``c``.

    :param x: positions of the vertices of the line in the first dimension
    :param y: positions of the vertices of the line in the second dimension
    :param c: colours to be used for the sections between subsequent vertices
    :param vmin: minimum value for colour map
    :param vmax: maximum value for colour map
    :param ax: matplotlib 2D axes for plotting
    :param setlims: if True, set visible area of axes to min/max of x and y
    :param args: will be passed to :py:class:`LineCollection`
    :param kwargs: will be passed to :py:class:`LineCollection`

    :return: collection of the lines that have been drawn

    Example: Coloured using the derivative of the sine function::

        import ithildin as ith
        import matplotlib.pyplot as plt
        import numpy as np
        x = np.linspace(0, 10, 1000)
        y = np.sin(x)
        c = (y[1:] - y[:-1])/(x[1:] - x[:-1])
        ith.plot.colorline(x, y, c, cmap="plasma", setlims=True)
        plt.show()

    """
    assert(len(x) == len(y))
    assert(len(c) >= len(x) - 1)
    if ax is None:
        ax = plt.gca()
    vmin = np.nanmin(c) if vmin is None else vmin
    vmax = np.nanmax(c) if vmax is None else vmax

    if setlims:
        ax.set_xlim(np.nanmin(x), np.nanmax(x))
        ax.set_ylim(np.nanmin(y), np.nanmax(y))

    segments = np.transpose([[x[:-1], y[:-1]], [x[1:], y[1:]]], (2, 0, 1))
    lc = LineCollection(segments, *args, norm=plt.Normalize(vmin, vmax), path_effects=[matplotlib.patheffects.Stroke(capstyle="round")], **kwargs)
    lc.set_array(c)
    lc.set_linewidth(2)
    ax.add_collection(lc)
    return lc

def periodic_plot(x:List[float], y:List[float], tau:float=2*np.pi, ax:Union[plt.Axes,None]=None, *args, **kwargs) -> List[plt.Line2D]:
    """
    Plot a function whose value is periodic.

    :param x: input values of the function
    :param y: output values of the function
    :param tau: the period of the function's value, :math:`\\tau=2\\pi` by default
    :param ax: matplotlib 2D axes for plotting
    :param args: will be passed to :py:class:`plt.plot`
    :param kwargs: will be passed to :py:class:`plt.plot`

    :return: collection of the lines that have been drawn

    Example: Plot a constantly increasing angle::

        import ithildin as ith
        import matplotlib.pyplot as plt
        import numpy as np
        t = np.linspace(0, 10, 100)
        omega = 3.
        alpha = omega*t
        ith.plot.periodic_plot(t, alpha)
        plt.show()

    """
    if ax is None:
        ax = plt.gca()
    x_ = np.copy(x).tolist()
    y_ = np.mod(y, tau).tolist()
    for pos in reversed(np.where(np.abs(np.diff(np.floor(y/tau))) >= 0.5)[0] + 1):
        xt = np.interp(np.round(y[pos-1]/tau)*tau, y[pos-1:pos+1], x[pos-1:pos+1])

        x_.insert(pos, xt)
        x_.insert(pos, xt)
        x_.insert(pos, xt)

        if y[pos-1] < y[pos]:
            y0, y1 = 0, tau
        else:
            y1, y0 = 0, tau

        y_.insert(pos, y0)
        y_.insert(pos, np.nan)
        y_.insert(pos, y1)

    return ax.plot(x_, y_, *args, **kwargs)

def pcolordiagmesh(x, y, cx, cy, vmin=None, vmax=None, ax:Union[plt.Axes,None]=None, *args, **kwargs) -> "matplotlib.collections.Collection":
    """
    Create a pseudocolor plot on a diagonal mesh, i.e. the dual mesh. This can
    be useful, for instance, to draw quantities that "live" on the edges
    connecting the vertices of a grid.

    :param x: positions of the vertices of the line in the first dimension
    :param y: positions of the vertices of the line in the second dimension
    :param cx: colours to be used for the grid offset in x direction
    :param cy: colours to be used for the grid offset in y direction
    :param vmin: minimum value for colour map
    :param vmax: maximum value for colour map
    :param ax: matplotlib 2D axes for plotting
    :param args: will be passed to :py:meth:`ax.tripcolor`
    :param kwargs: will be passed to :py:meth:`ax.tripcolor`

    :return: the collection drawn by :py:meth:`ax.tripcolor`

    Example: Randomly coloured grid::

        import numpy as np
        import matplotlib.pyplot as plt
        from ithildin.plot import pcolordiagmesh

        x = np.linspace(0, 1, 7)
        y = np.linspace(0, 1, 5)
        xg, yg = np.meshgrid(x, y)
        dx, dy = x[1] - x[0], y[1] - y[0]
        sx = (np.roll(xg**2, -1, axis=1) - xg**2)/dx
        sy = (np.roll(yg**2, -1, axis=0) - yg**2)/dy
        sx[:,-1] = np.nan
        sy[-1] = np.nan

        pcdm = pcolordiagmesh(x, y, sx, sy)
        plt.colorbar(pcdm)
        plt.show()

    """
    assert(cx.ndim == 2)
    assert((*y.shape, *x.shape) == cx.shape == cy.shape)
    if ax is None:
        ax = plt.gca()

    # double resolution of arrays
    X, Y = np.meshgrid(add_points(x, 0.5), add_points(y, 0.5))

    # create array triangles which contains indices of points of triangles
    ind = np.full(np.add(X.shape, 4), -1, dtype=int)
    ind[2:-2,2:-2] = np.reshape(np.arange(np.prod(X.shape)), X.shape)
    r = lambda i, j: np.roll(ind.T, (i, j), axis=(0,1))[::2,::2]
    triangles = np.transpose([
        [r(0,0), r( 0, 2), r( 1, 1)],
        [r(0,0), r(-1, 1), r( 0, 2)],
        [r(0,0), r( 1, 1), r( 2, 0)],
        [r(0,0), r( 2, 0), r( 1,-1)],
    ], (0, 2, 3, 1)).reshape(-1, 3)
    triangles = triangles[np.min(triangles, axis=1) >= 0]

    # how to color the triangles
    color = np.ravel([cx.T, np.roll(cx, 1, axis=1).T, cy.T, np.roll(cy, 1, axis=0).T])
    # color = np.ravel([cx.T, np.roll(cx, 1, axis=1).T])

    # convert coordinates and triangle indices into drawable triangles
    triang = tri.Triangulation(X.flatten(), Y.flatten(), triangles=triangles)

    # # debug plots
    # ax.triplot(triang, c="k")
    # ax.scatter(X.flatten(), Y.flatten(), c="r", marker=".")
    # ax.scatter(x.flatten(), y.flatten(), c="r", marker="o")

    # actually draw the triangles
    return ax.tripcolor(triang, color, *args, shading='flat',
            vmin=np.nanmin(color) if vmin is None else vmin,
            vmax=np.nanmax(color) if vmax is None else vmax,
            **kwargs
    )

def imshowvec(x:np.ndarray, y:np.ndarray, vx:np.ndarray, vy:np.ndarray, R:Union[float,None]=None, colorspace:Union[str,None]=None, black_offset:Union[float,None]=None, invert:bool=False, legend:bool=False, ax:Union[plt.Axes,None]=None, fig:Union[plt.Figure,None]=None, **kwargs):
    '''
    Draw a two dimensional vector field :math:`(x, y) \\to (vx, vy)` using
    colours with magnitude and hue depicting the length and direction of the
    vector :math:`(vx, vy)`.

    The vector first gets converted into a colour space and then
    :py:meth:`plt.imshow` is used to display the resulting image.

    :param x: positions x of the vector field
    :param y: positions y of the vector field
    :param vx: x component of the vectors at each point
    :param vy: y component of the vectors at each point
    :param R: radius of maximum magnitude of the vector
    :param colorspace: which colour space to use for the plot, options:

        * ``hls``: (default) HSL colour space
        * ``hsv``: HSV colour space
        * ``husl``: HUSL colour space. This one arguably models the intuitive
          colour space of humans most accurately. (requires optional dependency
          ``seaborn``)

    :param black_offset: offset to add to the magnitude
    :param invert: if true, the magnitude will be inverted before drawing
    :param legend: whether to create a legend
    :param ax: axes to draw on
    :param fig: figure to draw on
    :param kwargs: passed on to :py:meth:`plt.imshow`

    Source: MSc thesis of Desmond Kabus, 2019 (adapted)

    Example::

        import ithildin as ith
        import matplotlib.pyplot as plt
        import numpy as np
        x, y = np.meshgrid(*(np.linspace(-1,1,100) for i in range(2)))
        artists = ith.plot.imshowvec(x, y, x, y, legend=True)
        x, y = np.meshgrid(*(np.linspace(-1,1,10) for i in range(2)))
        plt.quiver(x, y, x, y)
        plt.show()

    '''
    if ax is None:
        ax = plt.gca()
    if fig is None:
        fig = ax.get_figure()

    @np.vectorize
    def polar2col(r:float, phi:float, one:float=1, colorspace:Union[str,None]=None) -> Tuple[int,...]:
        phi = phi - 0.25
        phi = phi % 1.
        if np.isnan(r):
            return (1., 1., 1.)
        r = np.clip(r, 0, 1)
        if colorspace is None or colorspace == 'hls':
            return colorsys.hls_to_rgb(phi, r, one)
        elif colorspace == 'hsv':
            return colorsys.hsv_to_rgb(phi, one, r)
        elif colorspace == 'husl':
            import seaborn # optional dependency
            return tuple(seaborn.palettes.husl.husl_to_rgb(
                np.clip(360*phi, 0, 360),
                np.clip(90*one, 0, 100),
                np.clip(100*r, 0, 100)))
        else:
            raise ValueError("colorspace must be either None or 'hls' or 'hsv' or 'husl'")

    def cart2polar(x:float, y:float) -> Tuple[float,float]:
        r = (x**2 + y**2)**.5
        phi = np.arctan2(x, y)
        return r, phi

    def cart2col(x:float, y:float, **kwargs) -> Tuple[int,...]:
        return pol2col(*cart2polar(x, y), **kwargs)

    def pol2col(r:float, phi:float, one:float=1, R:Union[float,None]=None, colorspace:Union[str,None]=None, black_offset:float=0, invert:bool=False) -> Tuple[int,...]:
        if R is None:
            R = np.max(r)
        r = r / R
        phi = 0.5*phi/np.pi + 0.5
        if black_offset is not None:
            r = (1 - black_offset)*r + black_offset
        if invert:
            r = 1 - r
        return np.moveaxis(polar2col(r, phi, one=one, colorspace=colorspace), 0, -1)

    vr, vphi = cart2polar(vx, vy)

    R_max = np.nanmax(vr)
    R_avg = np.nanmean(vr)
    R_std = np.nanstd(vr)

    if R is None:
        R = R_max

    artists = []

    kw = dict(
        R=R,
        colorspace=colorspace,
        black_offset=black_offset,
        invert=invert
    )
    artists.append(ax.imshow(
        pol2col(vr, vphi, **kw),
        extent=(np.min(x),np.max(x),np.min(y),np.max(y)),
        origin='lower',
        **kwargs
    ))

    if legend:
        shape = (3, 3)
        gridspec = plt.GridSpec(shape[0], shape[1])
        subplotspec = gridspec.new_subplotspec((0, 2))
        lax = fig.add_subplot(subplotspec, label='linear', anchor='NE')
        lax.axis('off')

        lx, ly = np.meshgrid(*(np.linspace(-1, 1, 200) for _ in range(2)))
        lx, ly = R*lx, R*ly
        pr, pp = cart2polar(lx, ly)
        C = cart2col(lx, ly, **kw)
        C = np.dstack((*np.moveaxis(C, -1, 0), pr<R))
        artists.append(lax.imshow(C, extent=(-R,R,-R,R), origin='lower'))

        levels = R*np.array([0.25, 0.5, 0.75])
        cont = lax.contour(lx, ly, pr,
            levels,
            colors=pol2col(levels, 0, one=0, **kw),
            linewidths=0.5,
        )
        artists.append(cont)
        lax.clabel(cont, inline=1, fontsize=5, colors='black')

        circle = plt.Circle((0, 0), R, color='k', fill=False)
        lax.add_artist(circle)
        artists.append(circle)

    plt.sca(ax)

    return artists
