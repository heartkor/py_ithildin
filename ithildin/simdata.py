#!/bin/env python3
"""
Simulation result data
----------------------

The SimData class is used to store the results of a simulation of ithildin.
"""

from glob import glob
from itertools import chain
from typing import List, Union, Tuple, Dict
import datetime
import numpy as np
import os
import pandas
import re
import tempfile
import warnings
import yaml
import zipfile

from . import filament as fil
from .log import Log
from .plot import plot_var
from .phase import phase_simdata
from .defect import defect_simdata, defect_edge_simdata
from .varfile import read_var, write_var
from .xdmf import xdmf_simdata

Sensors = Dict[Tuple[int,...],pandas.DataFrame]

class SimData:
    """
    This class holds simulation data from ithildin.

    :param log: filename of the log file to read or a Log object or None (use default parameters)
    :param filaments: filename of the filaments file to read or list of filaments
    :param sensors: list of filenames of the sensor files to read or dictionary of sensors (sensor positions in index space mapping to a :py:class:`pandas.DataFrame` containing the sensor measurements)
    :param skip_read_var: passed on to read_vars as skip_read,
        if True only metadata of the var files will be read
    :param upgrade_arrays_to_vars: if True, call :py:meth:`upgrade_arrays_to_vars`
    :param files: other key word arguments are filenames of array files to read,
        for example::

            SimData(..., u="mydata_u.var", v="mydata_v.var", egm="mydata_egm.var"")

        The keys that are also in the list `log.varnames` are read in as :py:attr:`vars`,
        as well as the variable named `inhom` as :py:attr:`inhom`,
        the other ones are read in as :py:attr:`arrays`.

    :var log: contents of logfile
    :vartype log: Log
    :var vars: dictionary of contents of varfiles as numpy arrays;
        using the variable names ``u``, ``v``, ``w``, ... and ``inhom`` as
        dictionary keys. The shape of those arrays is:

        .. math::

            (N_t, N_z, N_y, N_x)

        where
        :math:`N_t`: number of frames,
        :math:`N_z`: number of points in z,
        :math:`N_y`: number of points in y, and
        :math:`N_z`: number of points in x

    :vartype vars: dict[str, numpy.ndarray(shape=(Nfr, Nz, Ny, Nx))]
    :var arrays: dictionary of contents of additional numpy arrays in arbitrary shape
    :vartype arrays: dict[str, numpy.ndarray()]
    :var filaments: list of filaments read from filament file
    :vartype filaments: list[ithildin.Filament]
    :var sensors: dictionary of sensors: Sensor positions in index space mapping to a :py:class:`pandas.DataFrame` containing the sensor measurements.
    :vartype sensors: Dict[Tuple[int,...],pandas.DataFrame]
    :var dim: the number of spatial dimensions with length > 1
        (excluding time, i.e. 0th dimension)
    :vartype dim: int
    :var shape: the shape of the first variable in :py:attr:`vars`
    :vartype shape: list[int]
    :var origin: origin of grid coordinate system, i.e. the point which the
        zero-indices refer to (in KJI order)
    :vartype origin: list[float]
    :var deltas: grid spacing [dt, dz, dy, dx]
        (in KJI order, same as shape)
    :vartype deltas: list[float]
    :var times: points in time in milliseconds, when the frames of the
        variables have been recorded
    :vartype times: list[float]
    """
    def __init__(self, log:Union[str,Log,None]=None, filaments:Union[str,List[fil.Filament],None]=None, sensors:Union[List[str],Sensors,None]=None, skip_read_var:bool=False, upgrade_arrays_to_vars:bool=False, **files):
        if log is None:
            warnings.warn("No log file specified! Using default values.")
            self.log = Log(data={
                "Name": "default",
                "Start date": str(datetime.datetime.now()),
                "Geometry parameters": {"Voxel size": [1., 1.]},
                "Simulation parameters": {"Frame duration": "1 = 1 timesteps",
                "Time of first frame": "0 = 0 frame durations"},
            })

        elif isinstance(log, Log):
            self.log = log

        else:
            self.log = Log(log)

        self.vars = dict()
        self.arrays = dict()
        for varname, varpath in files.items():
            if varname == "inhom":
                self.vars[varname] = read_var(varpath, skip_read=skip_read_var, dtype=np.int32)
            elif len(self.log.varnames) == 0 or varname in self.log.varnames:
                self.vars[varname] = read_var(varpath, skip_read=skip_read_var)
            else:
                self.arrays[varname] = read_var(varpath, skip_read=skip_read_var)
        if upgrade_arrays_to_vars:
            self.upgrade_arrays_to_vars()
        self.check_all_shapes()

        self.filaments = fil.read_filaments(filaments) if isinstance(filaments, str) else filaments
        self.trajectories = None

        self.sensors = dict()
        if sensors is None:
            pass
        elif isinstance(sensors, list):
            for filename in sensors:
                m = re.match('.*_history_([0-9_]+).csv$', filename)
                if m:
                    idx = tuple(int(i) for i in m.groups()[0].split('_'))
                    self.sensors[idx] = pandas.read_csv(filename)
                else:
                    warnings.warn(f"Filename of sensor file does not match the required pattern: '{filename}'!")

        if np.any([s is None for s in self.log.shape]) and np.all([s is not None for s in self.shape]):
            self.log.shape = self.shape

    def check_all_shapes(self):
        """
        Make sure that all variables in :py:attr:`vars` have the correct shape.
        """
        shape = self.shape
        while len(shape) < 4:
            shape = (shape[0], 1, *shape[1:])
        self.shape = shape
        if 'inhom' in self.vars:
            self.vars['inhom'].shape = (1, *shape[1:])

    def upgrade_arrays_to_vars(self) -> int:
        """
        Upgrade arrays in :py:attr:`arrays` to :py:attr:`vars` if they have the correct shape.

        :return: number of upgraded variables
        """
        selected = []
        for key, array in self.arrays.items():
            if array.shape[1:] == self.shape[1:]:
                selected.append(key)
        for key in selected:
            self.vars[key] = self.arrays[key]
            del self.arrays[key]
        return len(selected)

    def __str__(self) -> str:
        dimnames = "xyzabcdefgh"
        dim = max(3, self.dim)
        s = lambda shape: dict(zip(["Nt", *["N"+l for l in reversed(dimnames[:dim])]], shape))
        d = dict(
                dim=dim, shape=s(self.shape), log=self.log.path,
                vars={k: dict(type=type(v).__name__, dtype=str(v.dtype), shape=s(v.shape)) for k, v in self.vars.items()},
                arrays={k: dict(type=type(v).__name__, dtype=str(v.dtype), shape=s(v.shape)) for k, v in self.arrays.items()},
            )
        if self.filaments:
            d["num_filaments"] = len(self.filaments)
        if self.trajectories:
            d["num_trajectories"] = len(self.trajectories)
        return yaml.safe_dump(dict(SimData=d))

    @property
    def varfiles(self):
        raise NotImplementedError('The dictionary of filenames has been removed.')

    @property
    def dtype(self) -> type:
        "data type of the variable fields"
        return next(v for k, v in self.vars.items() if k != "inhom").dtype
    @dtype.setter
    def dtype(self, dtype:type):
        for k, v in self.vars.items():
            if k != "inhom":
                v.dtype = dtype

    @property
    def dim(self) -> int:
        "the number of spatial dimensions with more than one point"
        return sum(1 for s in self.shape[1:] if s > 1)

    @property
    def shape(self) -> Tuple[int,...]:
        "shape of the variable fields"
        keys = list(k for k in self.vars.keys() if k != "inhom")
        if len(keys) >= 1:
            return self.vars[keys[0]].shape
        else:
            return self.log.shape
    @shape.setter
    def shape(self, shape:Tuple[int,...]):
        self.log.shape = shape
        for k, v in self.vars.items():
            if k != "inhom":
                v.shape = shape
            else:
                v.shape = shape[1:]

    @property
    def shape_space(self) -> Tuple[int,...]:
        return self.shape[1:]
    @shape_space.setter
    def shape_space(self, shape_space:Tuple[int,...]):
        self.shape = (self.shape[0], *shape_space)

    @property
    def origin(self) -> Tuple[float,...]:
        "position of the point with index zero"
        return self.log.origin
    @origin.setter
    def origin(self, value):
        self.log.origin = value

    @property
    def physical_size(self) -> Tuple[float,...]:
        "physical size of the domain"
        return self.log.physical_size

    @property
    def deltas(self) -> Tuple[float,...]:
        "grid step size in the dimensions"
        return self.log.deltas
    @deltas.setter
    def deltas(self, deltas:Tuple[float,...]):
        self.log.deltas = deltas

    @property
    def dzyx(self) -> Tuple[float,...]:
        "grid step size in the dimensions"
        return self.log.dzyx
    @dzyx.setter
    def dzyx(self, dzyx:Tuple[float,...]):
        self.log.dzyx = dzyx
    deltas_space = dzyx

    @property
    def times(self) -> List[float]:
        "times for the frames in milliseconds"
        return self.linspace(0)

    @property
    def mask(self) -> np.ndarray:
        "boolean mask, interior points are True, based on `self.vars['inhom']`, shape of one frame (Nz,Ny,Nx)"
        if "inhom" in self.vars:
            return self.vars["inhom"][0] > 0.5
        else:
            return np.ones(self.shape[1:], dtype=bool)

    def linspace(self, dim:int) -> List[float]:
        """
        List of coordinates for a given dimension in physical units.

        :param dim: dimension t, z, y, or x (KJI order)
        :return: the physical coordinates that correspond to the grid points
        """
        return self.origin[dim] + np.arange(self.shape[dim])*self.deltas[dim]

    def meshgrid(self, *dims, **kwargs) -> List[np.ndarray]:
        """
        Build an n-dimensional mesh-grid from the coordinates for the given
        dimensions in physical units.

        :param dims: dimensions t, z, y, or x (KJI order)
        :param kwargs: passed on to :py:func:`np.meshgrid`
        :return: coordinate matrices for the given dimensions
        """
        return np.meshgrid(*[self.linspace(d) for d in dims], **kwargs)

    @classmethod
    def from_stem(cls, stem:str, **kwargs) -> 'SimData':
        """
        Convenience constructor. Read data according to given filename stem.

        :param stem: stem to use to construct filenames of files to read.
        :param kwargs: keyword arguments are passed on to ``__init__``
        :return: new :py:class:`SimData` instance

        Filenames are constructed this way::

            log = f"{stem}_log.txt"
            filaments = f"{stem}_tipdata.txt"
            vars["u"] = f"{stem}_u.var"
            vars["u"] = f"{stem}_u.npy"
            vars["inhom"] = f"{stem}_inhom.var"
            vars["inhom"] = f"{stem}_inhom.npy"
            ...

        .. note::

            If a file with the extension ``.npy`` is found, it takes
            precedence over a ``.var`` file of the same name.

        .. note::

            If stem ends with ``.npz``, :py:meth:`from_npz` will be called instead.

        Example: load simulation data using a filename stem::

            import ithildin
            simdata = ithildin.SimData.from_stem("results/myseries_7")
            # loads all of these files:
            # results/myseries_7_inhom.var
            # results/myseries_7_inhom.npy
            # results/myseries_7_log.yaml
            # results/myseries_7_log.txt
            # results/myseries_7_tipdata.txt
            # results/myseries_7_u.var
            # results/myseries_7_v.npy
        """
        if stem.endswith('.npz'):
            return cls.from_npz(stem)
        var_ijk, var_kji = dict(), dict()

        m = re.match(r'^(.*)_([^_]+)\.([^\.]+)$', stem)
        if m:
            stem, varname, ext = m.groups()

        stem_base = os.path.basename(stem)
        for filename in chain(glob(f"{stem}_*.var"), glob(f"{stem}_*.npy")):
            label = re.sub(r'(\.kji|\.txyz)?\.(var|npy)$', '', os.path.basename(filename)) # remove ".var"
            label = re.sub(r"^" + stem_base + r"_", "", label) # remove stem
            if filename[-4:] == ".var":
                var_ijk[label] = filename
            else:
                var_kji[label] = filename
        var = {**var_ijk, **var_kji}
        return cls(
                log=f"{stem}_log.yaml" if os.path.isfile(f"{stem}_log.yaml") else f"{stem}_log.txt" if os.path.isfile(f"{stem}_log.txt") else None,
                filaments=f"{stem}_tipdata.yaml" if os.path.isfile(f"{stem}_tipdata.yaml") else f"{stem}_tipdata.txt" if os.path.isfile(f"{stem}_tipdata.txt") else None,
                sensors=glob(f"{stem}_history_*.csv"),
                upgrade_arrays_to_vars=True,
                **var, **kwargs)

    def to_stem(self, stem:str, as_varfiles:bool=False, order_txyz:bool=False):
        """
        Write self to a set of files.

        :param stem: stem to use to construct filenames of files
        :param as_varfiles: if True, use the internal varfile format; else use NumPy NPY files

        Example: write simulation data using a filename stem::

            simdata.to_stem("results/myseries_7")
            # writes these files:
            # results/myseries_7_log.yaml
            # results/myseries_7_u.npy
            # results/myseries_7_v.npy
            # results/myseries_7_inhom.npy
        """
        with open(f"{stem}_log.yaml", "w") as f:
            f.write(self.log.to_yaml())

        for varname, var in self.vars.items():
            filename = f"{stem}_{varname}.{'var' if as_varfiles else 'txyz.npy' if order_txyz else 'npy'}"
            if as_varfiles:
                if varname == "inhom":
                    write_var(filename, var, dtype=np.int32)
                else:
                    write_var(filename, var)
            elif order_txyz:
                var = np.transpose(var, (0, 3, 2, 1))
                np.save(filename, np.ascontiguousarray(var))
            else:
                np.save(filename, np.ascontiguousarray(var))

        for key, array in self.arrays.items():
            filename = f"{stem}_{key}.npy"
            np.save(filename, np.ascontiguousarray(array))

    def to_npz(self, filename:str, compressed:bool=False):
        """
        Write self to a NPZ file.

        :param filename: path where to write to
        :param compressed: if True, compress the NPZ file
        """
        saver = np.savez_compressed if compressed else np.savez
        saver(filename,
              log=self.log.to_yaml().encode(),
              filaments=yaml.safe_dump(self.filaments).encode(),
              **{k: np.ascontiguousarray(v) for k, v in self.vars.items()},
              **{k: np.ascontiguousarray(v) for k, v in self.arrays.items()},
        )

    @classmethod
    def from_npz(cls, filename:str, memmap_in_temp_dir:bool=True) -> 'SimData':
        """
        Construct SimData from an NPZ file.

        :param filename: path to read
        :param memmap_in_temp_dir: if True, unpack the NPZ file into a temporary directory to load it in as memory maps
        """
        f = NPZ_Memmap(filename) if memmap_in_temp_dir else np.load(filename, mmap_mode='r')
        log = Log(data=yaml.safe_load(bytes(f['log']).decode()))
        filaments = yaml.safe_load(bytes(f['filaments']).decode()) if 'filaments' in f else []
        sd = cls(log=log, filaments=filaments)
        sd.arrays = {k: v for k, v in f.items() if k not in ['log', 'filaments']}
        sd.upgrade_arrays_to_vars()
        sd.check_all_shapes()
        return sd

    def to_xdmf(self, *args, **kwargs) -> str:
        """
        Convert to XDMF self using :py:func:`ithildin.xdmf.xdmf_simdata`,
        where all arguments are passed on.
        """
        return xdmf_simdata(self, *args, **kwargs)

    def to_sappho(self, varname:str='u') -> "sappho.OpticalData":
        """
        Convert one variable of self into an instance of :py:class:`sappho.OpticalData`.

        :param varname: the key of the variable to convert
        :return: the exported instance of OpticalData
        """
        import sappho
        assert(self.dim == 2)
        return sappho.OpticalData(
            header={
                'DataXsize': self.shape[-1] + 28,
                'DataYsize': self.shape[-2],
                'acquisition_date': self.log.date,
                'creator': f'Ithildin SimData with stem {self.log.stem}',
                'dx': self.deltas[-1],
                'dy': self.deltas[-2],
                'frame_number': self.shape[0],
                'interval': self.deltas[0]*1000,
                'sample_time': self.deltas[0],
                'start_frame': 0,
                'version': 3.0,
            },
            background=np.zeros(self.shape[-2:], dtype=np.float32),
            frames=self.vars[varname].reshape((self.shape[0], *self.shape[-2:])).astype(np.float32, copy=True),
            dtype=np.float32,
        )

    def calc_trajectories(self, *args, **kwargs) -> List[List[fil.Filament]]:
        """
        Calculate trajectories from :py:attr:`filaments`
        using :py:func:`ithildin.filament.filaments_to_trajectories`,
        and store them in :py:attr:`trajectories`

        :return: :py:attr:`trajectories`
        """
        self.trajectories = fil.filaments_to_trajectories(self.filaments, *args, **kwargs)
        return self.trajectories

    def plot(self, varname, *args, **kwargs):
        """
        Plot a variable using :py:func:`ithildin.plot.plot_var`,
        where all arguments are passed on.
        """
        return plot_var(self, varname, *args, **kwargs)

    def phase(self, *args, **kwargs) -> np.ndarray:
        """
        Use :py:func:`ithildin.phase.phase_simdata` to calculate the phase
        where all arguments are passed on.
        """
        return phase_simdata(self, *args, **kwargs)

    def phase_defect(self, *args, **kwargs) -> np.ndarray:
        """
        Use :py:func:`ithildin.defect.defect_simdata` to calculate the phase defect
        where all arguments are passed on.
        """
        return defect_simdata(self, *args, **kwargs)

    def phase_edge_defect(self, *args, **kwargs) -> List[np.ndarray]:
        """
        Use :py:func:`ithildin.defect.defect_edge_simdata` to calculate the phase defect on edges
        where all arguments are passed on.
        """
        return defect_edge_simdata(self, *args, **kwargs)

    def __getitem__(self, slices) -> 'SimData':
        '''
        Use slicing operations to get a subset of the SimData.
        '''
        def apply_slices_to_idx(idx:tuple[int,...], slices:list[slice]) -> tuple[int,...]:
            return tuple((i - s.start)//s.step for i, s in zip(idx, slices))

        shape = self.shape
        N = len(shape)
        if slices == Ellipsis or isinstance(slices, int) or isinstance(slices, slice):
            slices = [slices]
        slices = list(slices)
        if len(slices) < N:
            slices.append(Ellipsis)
        slices = list(chain(*[[slice(s, s+1)] if isinstance(s, int) else ([slice(None) for _ in range(N - len(slices) + 1)] if s == Ellipsis else [s]) for s in slices]))
        slices = [slice(
            0 if s.start is None else s.start % N,
            None if s.stop is None else s.stop % N,
            1 if s.step is None else s.step,
        ) for N, s in zip(shape, slices)]
        assert len(slices) == N
        sd = SimData(log=Log(data=self.log.full_data))
        for varname, v in self.vars.items():
            if varname == 'inhom':
                sd.vars[varname] = v[(0, *slices[1:])]
            else:
                sd.vars[varname] = v[tuple(slices)]
        sd.log.shape = sd.shape
        sd.origin = tuple(o + s.start*delta for o, s, delta in zip(self.origin, slices, self.deltas))
        sd.deltas = tuple([dx*s.step for dx, s in zip(self.deltas, slices)])
        sd.filaments = None if self.filaments is None else [f for f in self.filaments if sd.log.tfirst <= f.time and f.time <= sd.log.tlast]
        sd.sensors = {apply_slices_to_idx(idx, slices[1:]): df for idx, df in self.sensors}
        sd.arrays = self.arrays
        for hist in sd.log.fields('History'):
            hist['Sensor'] = apply_slices_to_idx(hist['Sensor'], list(reversed(slices[1:])))
        return sd

class NPZ_Memmap(dict):
    '''
    Open an NPZ file as memory maps.
    This is done by:

    1. unpacking it into a temporary directory
    2. opening the NPY arrays in the NPZ archive as memmaps
    3. returning a dictionary, much like numpy.load
    4. deleting the temporary dictionary with the destructor

    :param path: path to the NPZ file to open
    '''
    def __init__(self, path:str):
        super().__init__()
        self.tempdir = tempfile.mkdtemp()

        with zipfile.ZipFile(path, 'r') as archive:
            archive.extractall(self.tempdir)
            for filename in os.listdir(self.tempdir):
                if filename.endswith('.npy'):
                    key = os.path.splitext(filename)[0]
                    path = os.path.join(self.tempdir, filename)
                    self[key] = read_var(path)

    def __del__(self):
        if hasattr(self, 'tempdir') and os.path.exists(self.tempdir):
            for filename in os.listdir(self.tempdir):
                os.remove(os.path.join(self.tempdir, filename))
            os.rmdir(self.tempdir)
