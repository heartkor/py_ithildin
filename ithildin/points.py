#!/bin/env python3
"""
Points, Lines, etc.
-------------------

This submodule implements several classes to deal with points living in
N-dimensional space. Each point might have data attached to it. The main base
class is :py:class:`Points`. A :py:class:`Line` is just some
:py:class:`Points` which are ordered.
"""

from scipy.spatial.distance import pdist, squareform
from scipy.linalg import eig
from typing import List, Tuple, Union
import networkx as nx
import numpy as np
import yaml

from .graph import clip_longest_path

def dict_tolist(d:dict) -> dict:
    'helper function for YAML serialisation'
    result = dict()
    for k, v in d.items():
        if isinstance(v, dict):
            result[k] = dict_tolist(v)
        elif hasattr(v, 'tolist'):
            result[k] = v.tolist()
        else:
            result[k] = v
    return result

class YAML_Mapping:
    @classmethod
    def yaml_class_name(cls) -> str:
        return '!ithildin.'+cls.__name__

    @classmethod
    def yaml_register(cls):
        yaml.add_representer(cls, cls.yaml_represent, yaml.SafeDumper)
        yaml.add_constructor(cls.yaml_class_name(), cls.yaml_construct, yaml.SafeLoader)

    @classmethod
    def yaml_represent(cls, dumper, data):
        return dumper.represent_mapping(
            cls.yaml_class_name(),
            data.to_dict() if hasattr(data, 'to_dict') else data.__dict__,
        )

    @classmethod
    def yaml_construct(cls, loader, node):
        d = loader.construct_mapping(node, deep=True)
        return cls(**d)

MaybeFloat = Union[float,None]
MaybeDict = Union[dict,None]
Maybe1DArray = Union[None,float,List[float],np.ndarray]
Maybe2DArray = Union[None,List[List[float]],np.ndarray]

class Points(YAML_Mapping):
    """
    This class implements a list of points that live on a rectangular grid.
    Internally, the positions of the points are stored in a float Numpy array
    with the shape :math:`(N, D)` where :math:`N` is the number of points and
    :math:`D` the number of dimensions, typically :math:`D=2` or :math:`D=3`.

    This internal array :py:attr:`points_grid` uses grid units, i.e. the
    positions are stored in multiples of the grid lengths :math:`\\Delta x`,
    :math:`\\Delta y`, etc. The grid lengths are called "resolution" and stored
    in :py:attr:`res`.

    The points in physical coordinates can be accessed using the property
    :py:attr:`points`.  These are calculated through multiplication with the
    resolution. (Note: The origin of the grid space is assumed to be at the
    zero vector in physical space.)

    As a line is just an ordered list of points, this class is also the base
    class for a line.  If :py:attr:`ordered` is true, then :py:class:`Points`
    is implementing a :py:class:`Line`.

    This class can store arbitrary data in the :py:attr:`data` dictionary.
    When indexing or slicing this object, all resulting elements will have a
    copy of this dictionary.

    This class can also store arbitrary "payload" information associated with a
    point. When indexing or slicing this object, this payload will also be
    properly split into the resulting elements or subsets, respectively. This
    information is stored in the array :py:attr:`payload`.

    The coordinates of the points may be given in exactly one of three different
    arguments:

    :param points_grid: list of points in (x, y, z) in grid coordinates (preferred way to set coordinates)
    :param points: list of points in (x, y, z) in physical coordinates
    :param points_grid_or_physical: list of points in (x, y, z) in either grid or physical coordinates
    :param in_physical_units: flag whether ``points_grid_or_physical`` is in physical coordinates (True) or grid coordinates (False). Note that this is only used if ``points_grid_or_physical`` is set.

    .. note:: An error is thrown if point coordinates are given multiple times.

    :param res: grid size in physical coordinates in (x, y, z)
    :param time: optional time at which this point cloud exists
    :param ordered: boolean whether points can be thought of as:

        * True: one curve (i.e. subsequent points can be connected with an edge) or
        * False: an unordered list of points (default)

    :param data: dictionary of arbitrary objects
    :param payload: arbitrary data, dictionary of arrays that all have the same length as number of points

    :var points_grid: list of points in (x, y, z) in grid coordinates
    :vartype points_grid: numpy.ndarray(shape=(-1, 3), dtype=np.float64)
    :var res: grid size in physical coordinates in (x, y, z)
    :vartype res: list[float]
    :var time: optional time at which this point cloud exists
    :vartype time: float
    :var ordered: ordered or unordered list of points
    :vartype ordered: bool

    """
    def __init__(self, points_grid_or_physical:Maybe2DArray=None, res:Maybe1DArray=1., time:MaybeFloat=None, ordered:bool=False, in_physical_units:bool=False, points:Maybe2DArray=None, points_grid:Maybe2DArray=None, data:MaybeDict=None, **payload):
        # figure out how the coordinates are given
        types_of_points_given = dict(auto=points_grid_or_physical is not None, phys=points is not None, grid=points_grid is not None)
        if sum(types_of_points_given.values()) > 1:
            raise ValueError('Coordinates must be given once across these three arguments: points, points_grid, or points_grid_or_physical!')
        else:
            points_given_as = next((type_of_points for type_of_points, given in types_of_points_given.items() if given), "grid")

        # reduce the general case
        if points_given_as == 'auto':
            points_grid_or_physical = np.array(points_grid_or_physical)
            if in_physical_units:
                points = points_grid_or_physical
                points_given_as = 'phys'
            else:
                points_grid = points_grid_or_physical
                points_given_as = 'grid'

        def check_default_res(p:Maybe2DArray, res:Maybe1DArray) -> Tuple[np.ndarray,np.ndarray]:
            p = np.array(p)
            if p.ndim < 2 and sum(p.shape) == 0:
                p = np.zeros((0, 2), dtype=np.float32)
            return p, np.array([res]*p.shape[1] if isinstance(res, float) else res, dtype=np.float32)

        # convert physical units to grid units and set resolution
        if points_given_as == 'phys':
            points, self.res = check_default_res(points, res)
            points_grid = points/self.res
        else:
            points_grid, self.res = check_default_res(points_grid, res)

        # set the member variable for the points and verify shapes
        self.points_grid = np.array(points_grid, dtype=np.float32)
        if len(self.points_grid) == 0:
            self.points_grid.shape = (0, len(self.res))
        else:
            assert(self.points_grid.ndim == 2)
        assert(self.res.shape == self.points_grid.shape[1:])

        # set other member variables
        self.time = time
        self.ordered = ordered

        # set data
        self.data = dict() if data is None else data

        # set and verify payload
        self.payload = {k: np.array(v) for k, v in payload.items() if v is not None}
        remove = []
        for k, p in self.payload.items():
            if len(p.shape) == 0:
                remove.append(k)
            elif p.shape[0] != len(self):
                raise AssertionError(f"The length of payload '{k}' ({p.shape[0]}) must be the same as the number of points ({len(self)})!")
        for k in remove:
            del self.payload[k]

    @property
    def shape(self) -> Tuple:
        return self.points_grid.shape

    def __len__(self) -> int:
        return self.shape[0]

    def __str__(self) -> str:
        return (f"line with {len(self)} points" if self.ordered else f"{len(self)} unordered points") + f" in {self.ndim}D" + (f" at time {self.time}" if self.time else "")

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}(**{self.to_dict()})'

    def to_dict(self) -> dict:
        d = dict_tolist(self.__dict__)
        for k, v in d['payload'].items():
            assert k not in d, f'duplicate key in payload: {k}'
            d[k] = v
        del d['payload']
        return d

    @property
    def ndim(self) -> int:
        return self.res.shape[0]

    @property
    def points(self) -> np.ndarray:
        "list of points in (x, y, z) in physical coordinates"
        return self.points_grid*self.res

    @points.setter
    def points(self, points_phys:np.ndarray):
        points_phys = np.array(points_phys, dtype=np.float32)
        self.points_grid = points_phys/self.res

    def sub(self, indices:List[int], ordered:bool=False, cls=None, **kwargs) -> "Points":
        """
        Construct a subset only based on the points with the given indices.

        :param indices: list of indices of the points to consider
        :param ordered: specifies whether the subset is already ordered
        :param cls: if used by an inherited class, this is the class that should be constructed
        :param kwargs: keyword arguments to pass to the class constructor

        :return: subset of only the selected points
        """
        if len(self) == 0:
            return self
        return (Points if cls is None else cls)(**{
            'ordered': ordered,
            'res': self.res,
            'time': self.time,
            'data': {k: v for k, v in self.data.items()},
            'points_grid': self.points_grid[indices],
            **{k: v[indices] for k, v in self.payload.items()},
            **kwargs,
        })

    def flip(self):
        """
        Reverse the order of all arrays.
        """
        self.points_grid = np.flip(self.points_grid, axis=0)
        self.payload = {k: np.flip(v, axis=0) for k, v in self.payload.items()}

    def __getitem__(self, idx:Union[int,slice,set,list,np.ndarray]) -> "Points":
        """
        Get subset of points using indexing.
        """
        if isinstance(idx, int):
            idx = slice(idx, idx+1)
        if isinstance(idx, slice):
            idx = range(len(self))[idx]
        if isinstance(idx, set):
            idx = list(idx)
        return self.sub(idx)

    def graph(self, maxdist:float=1.9) -> nx.Graph:
        """
        Construct a graph based on grid distances between points.

        :param maxdist: maximum distance of points to connect with an edge
            (in grid units), default value:

            .. math::

                d_{\\mathrm{max}} = 1.9 > \\sqrt 3

        :return: graph of the points
        """
        pd = pdist(self.points_grid)
        return nx.Graph(squareform((pd<=maxdist)*pd))

    def break_apart(self) -> List["Points"]:
        """
        Find connected components in the graph of representing the points.

        :return: list of connected components
        """
        return [self.sub(list(indices)) for indices in nx.connected_components(self.graph())]

    def longest_path(self) -> Tuple["Points","Points"]:
        """
        Find longest path in the minimal spanning tree based on :py:meth:`graph`.
        Turn this path into an ordered list of points.
        Remaining branches are also returned as unordered points.

        :return: longest path, left overs
        """
        path, G = clip_longest_path(self.graph())
        return self.sub(path, ordered=True), self.sub(G.nodes)

Points.yaml_register()

Line = Points
Lines = List[Line]

def break_apart_all(lines:Lines) -> Lines:
    """
    Turn list of multiple unordered and unconnected lists of points
    into list of its unordered but connected components.

    .. note::
        Typically, this method is used for lists of lines, hence the parameter
        name ``lines`` for the list of multiple instances of
        :py:class:`Points`.

    :param lines: list of lines to break apart

    :return: merged list of connected components
    """
    splits = []
    for line in lines:
        splits.extend(line.break_apart())
    return splits

def longest_path_all(lines:Lines) -> Lines:
    """
    Turn list of unordered but connected lists of points
    into list of ordered lines.

    :param lines: list of unordered, connected lines

    .. warning:: The ``lines`` will be modified!

    :return: list of ordered lines
    """
    ordered = []
    while len(lines) > 0:
        line = lines.pop()
        path, line = line.longest_path()
        ordered.append(path)
        if len(line) > 0:
            lines.extend(line.break_apart())
    return ordered

def clockwise(loop:Points) -> bool:
    """
    Determine whether a closed loop is going around clockwise or counter-clockwise.

    :param loop: a closed loop, i.e. a sorted list of points, where the first and last point are connected

    :return: True if the loop is clockwise
    """
    assert(isinstance(loop, Points))
    x = loop.points
    v = np.roll(x, 1, axis=0) - x
    p = np.unwrap(np.arctan2(v[:,0], v[:,1]))
    return p[-1]/(2*np.pi) > 0

def pca(data:Union[np.ndarray,Points]) -> Tuple[np.ndarray,np.ndarray]:
    """
    Perform a *principal component analysis* (PCA) for the given points.

    :param data: N points in D dimensions, shape (N, D)
    :returns: the eigenvalues and principal components (eigenvectors)
    """
    data = data.points if isinstance(data, Points) else data
    x = np.subtract(data, np.mean(data, axis=0))
    eigvals, eigvecs = eig(x.T@x)
    return eigvals, eigvecs.T
