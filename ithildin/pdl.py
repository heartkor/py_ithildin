#!/bin/env python3
"""
Phase Defect Lines (PDLs)
-------------------------

For a periodic process in a medium, for which a phase can be defined, we call
(large) discrete jumps from one value to another value *phase defect*. A *phase
defect line* (PDL) is a line at such an interface in 2D. In 3D, we would
observe *phase defect surfaces* (PDSs). The edge case of a PDL of length 0 is
equivalent to a so-called *phase singularity* (PS).
"""

from typing import List, Union
from warnings import warn
import numpy as np

from .phase import difference_phases as difference_radians
from .points import Points, longest_path_all, clockwise, pca
from .graph import delete_border, marching_squares
from .topology import contours_of_mask_in_other_mask
from .filament import find_trajectories
from .plot import plot_pdl

class PDL(Points):
    """
    This class describes a phase defect line (PDL) in 2D which is a list of
    ordered :py:class:`Points`, i.e. a line.  The PDL instance exists at only one
    point in time. The evolution of a PDL over time is a PDL trajectory, i.e. a
    list of PDLs.

    This class adds these parameters and variables to :py:class:`Points`:

    :param phase_head: phase values in front of the head branch point :math:`\\phi_{\\mathrm{head}}`
    :var phase_head: phase values in front of the head branch point :math:`\\phi_{\\mathrm{head}}`
    :vartype phase_head: float

    :param phase_tail: phase values behind the tail branch point :math:`\\phi_{\\mathrm{tail}}`
    :var phase_tail: phase values behind the tail branch point :math:`\\phi_{\\mathrm{tail}}`
    :vartype phase_tail: float

    :param phase_left: phase values on the left side of each of the points :math:`\\phi_{\\mathrm{left}}`
    :var phase_left: phase values on the left side of each of the points :math:`\\phi_{\\mathrm{left}}`
    :vartype phase_left: np.ndarray

    :param phase_right: phase values on the right side of each of the points :math:`\\phi_{\\mathrm{right}}`
    :var phase_right: phase values on the right side of each of the points :math:`\\phi_{\\mathrm{right}}`
    :vartype phase_right: np.ndarray

    :param kwargs: passed on to :py:class:`Points`

    .. note ::
        In contrast to  :py:class:`Points`,
        :py:attr:`ordered` specifies whether the sub PDL is already ordered,
        and can take three values:

        0. the PDL is not ordered, and can be thought of as a point cloud
        1. the PDL is ordered, and can be thought of as a line
        2. the PDL is ordered and has a direction, the point with index 0
           corresponds to the *head branch point*, the last point is the *tail
           branch point*

    Example: Show some info about a PDL::

        import matplotlib.pyplot as plt
        import ithildin as ith

        pdl = ith.PDL([
                [0.5, 0.], [7., 4.5], [12.5, 9.], [17., 14.5],
                [20.5, 21.], [23., 28.5], [24., 36.5]
            ],
            res=[0.3, 0.3], time=125.5, ordered=2,
            phase_head=0.62, phase_tail=5.41,
            phase_left=[6.28, 6.28, 6.28, 6.28, 6.28, 6.28, 6.28],
            phase_right=[1.24, 2.38, 3.37, 4.17, 4.78, 5.02, 5.41],
        )

        print(pdl)
        print("orientation beta =", pdl.angle())
        print("charge q =", pdl.charge())
        print("physical length L =", pdl.arc_length())

        # show a graphical representation of the PDL
        pdl.plot()
        plt.show()

        # flipping changes the angle by pi but the charge remains the same
        pdl.flip()
        print("flipped orientation beta =", pdl.angle())
        print("flipped charge q =", pdl.charge())

        # subindexing
        subpdl = pdl[:3] # uses the `sub()` method
        print("physical length L of subset =", subpdl.arc_length())

    """
    def __init__(self, points_grid:List[List[float]], res:Union[List[float],float]=1., time:Union[float,None]=None, ordered:int=1, phase_head:Union[float,None]=None, phase_tail:Union[float,None]=None, phase_left:Union[List[float],None]=None, phase_right:Union[List[float],None]=None, **kwargs):
        self.phase_head = phase_head or np.nan
        self.phase_tail = phase_tail or np.nan
        Points.__init__(self, points_grid=points_grid, res=res, time=time, ordered=bool(ordered), phase_left=np.nan if phase_left is None else phase_left, phase_right=np.nan if phase_right is None else phase_right, **kwargs)
        self.ordered = ordered

    def __str__(self) -> str:
        return ("unordered " if not self.ordered else "") + f"PDL with {len(self)} points in {self.ndim}D" + ("" if self.time is None else f" at time {self.time}") + (f", charge={self.charge()}" if np.isfinite(self.phase_head) else "")

    @property
    def phase_left(self) -> np.ndarray: return self.payload["phase_left"]
    @phase_left.setter
    def phase_left(self, value:np.ndarray): self.payload["phase_left"] = value

    @property
    def phase_right(self) -> np.ndarray: return self.payload["phase_right"]
    @phase_right.setter
    def phase_right(self, value:np.ndarray): self.payload["phase_right"] = value

    def sub(self, indices:List[int], ordered:int=0) -> "PDL":
        """
        Construct a PDL only based on the points with the given indices.

        :param indices: list of indices of the points to consider
        :param ordered: specifies whether the sub PDL is already ordered

        .. note::

            While for ``phase_left`` and ``phase_right``, the subset can be
            properly selected, this is not possible for ``phase_head`` and
            ``phase_tail``. These values are set to ``None`` after taking a
            subset with this method.

        :return: sub PDL for only the selected points
        """
        return super().sub(indices=indices, ordered=ordered, cls=PDL, time=self.time, phase_head=None, phase_tail=None)

    def flip(self):
        """
        Reverse the order of all arrays in place.
        """
        super().flip()
        self.phase_head, self.phase_tail = self.phase_tail, self.phase_head
        self.phase_left, self.phase_right = self.phase_right, self.phase_left

    def arc_length(self) -> float:
        """
        Calculate the length :math:`L` along the curve of the PDL in physical coordinates.

        :return: the arc length :math:`L`
        """
        d = self.points_grid[1:] - self.points_grid[:-1]
        return np.sum(np.linalg.norm(d*self.res, axis=1))

    def contour(self, algo:int=0) -> Points:
        """
        Find the contour around the PDL, a closed loop, i.e. a list of points.

        :param algo: which algorithm to use to find the contour

        0. For each tuple of three points find the points halving the angle (default).
            Using this algorithm yields exactly :math:`2N+2` points, namely:

            .. math ::

                    [{\\vec x}_{\\mathrm{head}},
                    {\\vec x}_{\\mathrm{left}, 0}, ..., {\\vec x}_{\\mathrm{left}, N-1},
                    {\\vec x}_{\\mathrm{tail}},
                    {\\vec x}_{\\mathrm{right}, N-1}, ..., {\\vec x}_{\\mathrm{right}, 0}]

        1. Using marching squares.
            While the number of points might be different, it leads to a
            *prettier* contour.

        :return: the contour
        """
        if algo == 1:
            (x0, y0) = np.nanmin(self.points_grid, axis=0).astype(int) - 4
            (x1, y1) = np.nanmax(self.points_grid, axis=0).astype(int) + 3

            b = np.zeros(((y1-y0), (x1-x0)))
            x = self.points_grid[:,0] - x0
            y = self.points_grid[:,1] - y0
            b[(y.astype(int), x.astype(int))] = True
            b = np.sum([
                b,
                np.roll(b, -1, axis=0),
                np.roll(b, -1, axis=1),
                np.roll(b,  1, axis=0),
                np.roll(b,  1, axis=1),
            ], axis=0)

            # import matplotlib.pyplot as plt
            # plt.imshow(b)
            # plt.show()

            contours = marching_squares(np.arange(b.shape[1]), np.arange(b.shape[0]), b, 0.5)
            if len(contours) != 1:
                warn("Marching squares returned more than one contour.")
            return Points(np.add(contours[0], [x0, y0]), res=self.res, ordered=1)

        else:
            shape = (len(self), self.ndim)
            p = np.full((shape[0] + 2, shape[1]), np.nan)
            p[1:-1] = self.points_grid
            p[0] = p[1] - (p[2] - p[1])
            p[-1] = p[-2] - (p[-3] - p[-2])

            pa, pb, pc = p[:-2], p[1:-1], p[2:]
            x1, y1 = np.transpose(pa - pb + 1e-10) # to avoid edge cases
            x2, y2 = np.transpose(pc - pb)
            alpha1 = np.arctan2(y1, x1)
            alpha2 = np.arctan2(y2, x2)
            dot = x1*x2 + y1*y2
            det = x1*y2 - y1*x2
            theta = alpha1 + np.arctan2(det, dot)/2
            dalpha = difference_radians(alpha1, alpha2)
            theta[dalpha < 0] += np.pi
            theta = np.where(alpha1 == alpha2, alpha1 + np.pi/2, theta)

            d = 2.*np.transpose([np.cos(theta), np.sin(theta)])
            return Points(np.concatenate([p[:1], pb + d, p[-1:], np.flip(pb - d, axis=0)]), res=self.res, ordered=1)

    def store_nearby_phase(self, phase:np.ndarray):
        """
        Store phase values on the contour around the PDL in the variables:

        * :py:attr:`phase_head`  :math:`\\phi_{\\mathrm{head}}`
        * :py:attr:`phase_right` :math:`\\phi_{\\mathrm{right}}`
        * :py:attr:`phase_tail`  :math:`\\phi_{\\mathrm{tail}}`
        * :py:attr:`phase_left`  :math:`\\phi_{\\mathrm{left}}`

        :param phase: the two-dimensional phase on the same grid as the PDL
        """
        assert(phase.ndim == 2)
        idx = self.contour().points_grid.T.astype(int)
        phase = phase[np.clip(idx[1], 0, phase.shape[1]-1), np.clip(idx[0], 0, phase.shape[0]-1)]
        N = len(self)
        self.phase_head = phase[0]
        self.phase_right = phase[1:N+1]
        self.phase_tail = phase[N+1]
        self.phase_left = np.flip(phase[N+2:])

        ql = charge(self.phase_left, rounded=False)
        qr = charge(self.phase_right, rounded=False)
        q = ql if abs(ql) > abs(qr) else qr
        if q < 0:
            self.flip()

        self.ordered = 2

    @property
    def nearby_phase(self) -> np.ndarray:
        """
        phase at the :math:`2N+2` points on the contour around the PDL in exactly this order:

        .. math ::

                [\\phi_{\\mathrm{head}},
                \\phi_{\\mathrm{left}, 0}, ..., \\phi_{\\mathrm{left}, N-1},
                \\phi_{\\mathrm{tail}},
                \\phi_{\\mathrm{right}, N-1}, ..., \\phi_{\\mathrm{right}, 0}]
        """
        return np.concatenate([[self.phase_head], self.phase_right, [self.phase_tail], np.flip(self.phase_left)])

    def charge(self, phase:Union[np.ndarray,None]=None, *args, **kwargs) -> int:
        """
        Calculate the topological charge of a PDL.

        :param phase: a two-dimensional array with the same grid resolution as ``res``.
            If this is ``None``, try to use the stored ``nearby_phase``;
            else, the ``nearby_phase`` will be read from the grid and stored in the PDL.
        :param args: passed on to :py:meth:`charge()`
        :param kwargs: passed on to :py:meth:`charge()`
        :return: the topological phase charge
        """
        if phase is not None:
            self.store_nearby_phase(phase)
        return charge(self.nearby_phase, *args, **kwargs)

    def angle(self, return_ellipticity:bool=False) -> float:
        """
        Use a principal component analysis PCA (:py:func:`pca`) to find the axis
        along which the PDL varies the most and return its angle :math:`\\beta` in radians.

        .. note:: This function only works in 2D.

        :param return_ellipticity: if this is true, return tuple ``beta, ellipticity``. The flatness of ellipse fit using the PCA is a measure of the certainty of the measured angle.
        :return: the angle :math:`\\beta` of the PDL
        """
        assert(self.ndim == 2)
        p = self.points
        eigvals, eigvecs = pca(p)
        i = np.argmax(np.abs(eigvals))
        beta = np.arctan2(*eigvecs[i]) # PCA angle
        beta_ = np.arctan2(p[0,0] - p[-1,0], p[0,1] - p[-1,1]) # tail to head angle
        if abs(difference_radians(beta, beta_)) > np.pi/2:
            beta = np.mod(beta + np.pi, 2*np.pi)
        if return_ellipticity:
            b, a = np.sort(np.abs(eigvals))
            ell = (a-b)/a
            return beta, ell
        else:
            return beta

    def plot(self, *args, **kwargs):
        """
        Plot self using :py:func:`ithildin.plot.plot_pdl`,
        where all arguments are passed on.
        """
        return plot_pdl(self, *args, **kwargs)

PDL.yaml_register()

def pdls_harp(mask_rho:np.ndarray, mask_active:np.ndarray, x:List[float], y:List[float], **kwargs) -> List[PDL]:
    """
    Find PDLs :math:`P` as the parts of the contour of active region :math:`A`
    where the vertex-based phase defect is larger than a threshold:

    .. math::

        P = \\{ (x, y) \\in \\partial A \\;|\\; \\rho(x, y) > \\rho_0 \\}

    :param mask_rho: boolean mask :math:`\\rho(x, y) > \\rho_0`, where the phase defect is larger than a threshold
    :param mask_active: boolean mask :math:`A`, where the tissue is considered activated
    :param x: list of coordinates in x-direction
    :param y: list of coordinates in y-direction
    :param kwargs: passed on to the constructor of :py:class:`PDL`
    :return: list found PDLs :math:`P`
    """
    lines, _ = contours_of_mask_in_other_mask(mask_active, mask_rho, x, y)
    pdls = [PDL(l, ordered=1, **kwargs) for l in lines]
    return sorted([p for p in pdls if len(p) > 1], key=lambda p: len(p), reverse=True)

def pdls_edge(masks_sigma:List[np.ndarray], x:List[float], y:List[float], dx:Union[float,None]=None, dy:Union[float,None]=None, **kwargs) -> List[PDL]:
    """
    Find PDLs :math:`P` as the edges
    where the edge-based phase defect is larger than a threshold:

    .. math::

        P = \\{ (x, y) \\;|\\; \\sigma(x, y) > \\sigma_0 \\}

    :param masks_sigma: list of boolean masks :math:`\\sigma_a(x, y) > \\sigma_0` for the axes :math:`a \\in \\{x, y\\}`, where the phase defect is larger than a threshold
    :param x: list of coordinates in x-direction
    :param y: list of coordinates in y-direction
    :param dx: step size in x-direction, if None: ``x[1] - x[0]``
    :param dy: step size in y-direction, if None: ``y[1] - y[0]``
    :param kwargs: passed on to the constructor of :py:class:`PDL`
    :return: list found PDLs :math:`P`

    This method works roughly like this:

        1. Put points where :math:`\\sigma` is above a threshold into graph
        2. Turn graph into tree and trim short branches
        3. The longest paths in that tree are the PDLs
    """

    x = np.array(x)
    y = np.array(y)
    assert(1 == x.ndim == y.ndim)
    for m in masks_sigma:
        assert(m.dtype == bool)
        assert(m.shape == (y.shape[0], x.shape[0]))
    if dx is None: dx = x[1] - x[0]
    if dy is None: dy = y[1] - y[0]

    grid = np.meshgrid(x, y)

    points = np.concatenate([
        np.transpose([(coord + np.roll(coord, -1, axis=a))[mask]/2 for coord in grid])
        for a, mask in enumerate(reversed(masks_sigma))
    ])

    pdl = PDL(np.divide(points, [dx, dy]), res=[dx, dy], ordered=False, **kwargs)
    pdls = longest_path_all(pdl.break_apart())
    return sorted([p for p in pdls if len(p) > 1], key=lambda p: len(p), reverse=True)

def pdls_ridge(rho:np.ndarray, x:List[float], y:List[float], dx:Union[float,None]=None, dy:Union[float,None]=None, threshold:float=0.1, **kwargs) -> List[PDL]:
    """
    Find PDLs :math:`P` as the local maxima along x- and y-direction
    where also the vertex-based phase defect is larger than a threshold:

    .. math::

        M_x &= \\{ (x, y) \\;|\\; \\exists y' \\in Y : \\rho(x, y=y') \\textup{ is maximal} \\} \\\\
        M_y &= \\{ (x, y) \\;|\\; \\exists x' \\in X : \\rho(x=x', y) \\textup{ is maximal} \\} \\\\
        T   &= \\{ (x, y) \\;|\\; \\rho(x, y) > \\rho_0 \\} \\\\
        P   &= ( M_x \\cup M_y ) \\cap T

    :param rho: the vertex-based phase defect field :math:`\\rho(x, y)`
    :param x: list of coordinates in x-direction
    :param y: list of coordinates in y-direction
    :param dx: step size in x-direction, if None: ``x[1] - x[0]``
    :param dy: step size in y-direction, if None: ``y[1] - y[0]``
    :param threshold: the threshold iso-value :math:`\\rho_0`
    :param kwargs: passed on to the constructor of :py:class:`PDL`
    :return: list found PDLs :math:`P`

    .. note::

        This method computes a edge-based mask and then calls :py:meth:`pdls_edge` to construct the PDLs.
    """
    assert(rho.shape == (len(y), len(x)))
    if dx is None: dx = x[1] - x[0]
    if dy is None: dy = y[1] - y[0]
    return pdls_edge([
        np.where(rho < threshold, False, 0 < np.maximum(-np.roll(drho, 1, axis=a)*drho, 0))
        for a, drho in enumerate(np.gradient(rho, dy, dx, axis=(0, 1)))
    ], x=x, y=y, dx=dx, dy=dy, **kwargs)

def charge(phase:List[float], rounded:bool=True) -> Union[int,float]:
    """
    Calculate the topological charge for a list of phases.

    :param phase: a one-dimensional array of the phases
    :param rounded: if True, round to nearest integer; else return float

    :return: the topological phase charge
    """
    charge = np.nansum(difference_radians(phase, np.roll(phase, 1)))/(2*np.pi)
    if rounded:
        charge = round(charge)
    return charge
