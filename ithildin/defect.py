#!/bin/env python3
"""
Phase defect fields
-------------------

A *phase defect*
is a (more or less) discontinuous jump in phase. In the context of cardiology,
conduction blocks, for instance, can lead to such phase defect. They can be
observed in the center of a vortex.

This submodule contains algorithms to calculate phase defect fields
from phases :math:`\\phi(t, \\vec x)`, as can be obtained from
:py:class:`ithildin.simdata.SimData` with the submodule
:py:mod:`ithildin.phase`.

We differentiate between two kinds of phase defect fields:

* :math:`\\rho(t, \\vec x)` aligned to grid points
* :math:`\\sigma_a(t, \\vec x)` aligned to edges between grid points in positive :math:`x_a`-direction, often merged into a vector :math:`\\vec\\sigma = \\{\\sigma_a\\}`

"""
import numpy as np
import warnings
from typing import List, Tuple, Union
from .pulse import time_elapsed
from .phase import difference_phases

from . import graph
neighbourhood = graph.neighbourhood
delete_border = graph.delete_border

# === DERIVATIVE BASED METHODS ===

def defect_cpg(phase:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored) -> np.ndarray:
    """
    Calculate phase defect :math:`\\rho(t, \\vec x)` using the maximal complex phase gradient (CPG):

    .. math::

        \\rho = ||\\nabla z||

    with the complex phase :math:`z = \\mathrm e^{\\mathrm i \\phi}`.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase

    .. seealso::
        Method :py:meth:`defect_edge_cpg`:
            related phase defect centered on edges
        Method :py:meth:`defect_rpg`:
            related phase defect using the real phase
    """
    phase = np.exp(1j*phase) # use complex phase
    deltas = [1.]*len(axes) if deltas is None else deltas
    defect = np.abs(sum(((np.roll(phase, -1, axis=axis) - phase)/delta)**2 for axis, delta in zip(axes, deltas))**.5)
    defect = delete_border(defect, axes=axes, width=1)
    return defect
defect_mpg = defect_cpg

def defect_rpg(phase:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored) -> np.ndarray:
    """
    Calculate phase defect :math:`\\rho(t, \\vec x)` using the maximal real phase gradient (RPG):

    .. math::

        \\rho = \\rho_a = 1/N \\sum_{b\\in\\mathcal N_a} \\sigma_{ab}

    using the edge based real phase gradient method :math:`\\sigma_{ab}`

    Source: Arno L, Quan J, Nguyen NT, Vanmarcke M, Tolkacheva EG, Dierckx H. A Phase Defect Framework for the Analysis of Cardiac Arrhythmia Patterns. Front Physiol. 2021;12. doi:10.3389/fphys.2021.690453.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase

    .. seealso::
        Method :py:meth:`defect_edge_cpg`:
            related phase defect centered on edges
        Method :py:meth:`defect_rpg`:
            related phase defect using the real phase
    """
    return np.mean(defect_edge_rpg(phase, axes=axes, deltas=deltas), axis=0)

def defect_ip(phase, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored):
    """
    Calculate phase defect :math:`\\rho` using the interior point method (IP):

    .. math::

        \\rho = \\max_a\\{ \\sigma_{a} \\}

    using the related phase defect :math:`\\sigma_{a}` centered on edges (:py:meth:`defect_edge_ip`)
    for each vertex with neighbours :math:`a`.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase

    .. seealso::
        Method :py:meth:`defect_edge_ip`:
            related phase defect :math:`\\sigma` centered on edges
    """
    sigma = defect_edge_ip(phase=phase, axes=axes, deltas=deltas)
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', r'All-NaN (slice|axis) encountered')
        return delete_border(np.nanmax(sigma, axis=0), axes=axes, width=2)
defect_mph = defect_ip

def defect_spatial_vec_field(phase:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored) -> np.ndarray:
    """
    Calculate phase defect :math:`\\rho` using the spatial vector field method:

    .. math::

        \\vec \\rho = \\nabla \\times \\nabla z

    with the complex phase :math:`z = \\mathrm e^{\\mathrm i \\phi}`.

    In two dimensions, :math:`\\rho` is the z-component of :math:`\\vec \\rho`.
    In three dimensions, :math:`\\rho` is the length of :math:`\\vec \\rho`.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase
    """
    phase = np.exp(1j*phase)
    deltas = [1.]*len(axes) if deltas is None else deltas
    grad = np.gradient(phase, *deltas, axis=axes)

    if len(axes) == 3:
        rotgrad = np.zeros_like(grad)
        for i in range(3):
            i, j, k = i, (i+1)%3, (i+2)%3
            rotgrad[k] = np.gradient(grad[i], deltas[j], axis=axes[j]) - np.gradient(grad[j], deltas[i], axis=axes[i])
        defect = np.linalg.norm(rotgrad, axes=0)
    elif len(axes) == 2:
        rotgrad = np.gradient(grad[0], deltas[1], axis=axes[1]) - np.gradient(grad[1], deltas[0], axis=axes[0])
        defect = np.abs(rotgrad)
    else:
        raise ValueError("Dimension must be 2 or 3, i.e. must give 2 or 3 axes!")

    defect = delete_border(defect, axes=axes, width=2)
    return defect

# === NEIGHBOURHOOD AVERAGING METHODS ===

def defect_cos(phase:np.ndarray, axes:List[int]=[-2,-1], **ignored) -> np.ndarray:
    """
    Calculate phase defect :math:`\\rho = \\frac 1 {N_a} \\sum_a\\sigma_a` using the cosine method,
    where :math:`\\sigma_a` is the edge-based phase defect using the cosine method in :math:`a` direction.

    Source: Tomii N, Yamazaki M, Ashihara T, Nakazawa K, Shibata N, Honjo H, et al. Spatial phase discontinuity at the center of moving cardiac spiral waves. Computers in Biology and Medicine. 2021;130.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes in which to look for neighbours
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase

    .. seealso::
        Method :py:meth:`defect_edge_cos`:
            related phase defect :math:`\\sigma` centered on edges
    """
    return np.mean(defect_edge_cos(phase, axes=axes), axis=0)

def defect_pc(phase:np.ndarray, axes:List[int]=[-2,-1], neighbours:Union[float,List[List[int]]]=1.9, **ignored) -> np.ndarray:
    """
    Calculate phase defect :math:`\\rho = |1-\\tilde\\rho|` using the phase coherence method:

    .. math::

        \\tilde \\rho(t, \\vec x) = \\frac 1 N \\left|\\sum_{\\vec x_n\\in\\mathcal N(\\vec x)} \\mathrm e^{\\mathrm i \\phi(t, \\vec x_n) } \\right|

    for a neighbourhood of :math:`N` other grid points :math:`\\vec x_n \\in \\mathcal N(\\vec x)` around each point :math:`\\vec x`.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes in which to look for neighbours
    :param neighbours: the neighbourhood :math:`\\mathcal N` is given as a list of tuples of index shifts in the given ``axes`` which will be used to find the neighbours of each point :math:`\\vec x`. If instead a float is given, this float will be passed as the distance ``dist`` to :py:func:`neighbourhood` to construct a neighbourhood of all points in the selected dimension up to this distance.
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase

    .. seealso::
        Method :py:meth:`defect_edge_pc`:
            related phase defect :math:`\\vec\\sigma` centered on edges
    """
    phase = np.exp(1j*phase)
    neighbours = neighbours if isinstance(neighbours, list) else neighbourhood(dim=len(axes), dist=neighbours)
    sum_coh = np.zeros_like(phase)

    for shifts in neighbours:
        sum_coh += np.roll(phase, shifts, axes)

    rho = np.abs(sum_coh)/len(neighbours)
    defect = np.abs(1-rho)
    defect = delete_border(defect, axes=axes, width=int(np.max(np.linalg.norm(neighbours, axis=1))))
    return defect

def defect_dipole(phase:np.ndarray, axes:List[int]=[-2,-1], neighbours:Union[float,List[List[int]]]=1.9, **ignored) -> np.ndarray:
    """
    Calculate phase defect :math:`\\rho = || \\vec p ||` using the dipole moment:

    .. math::

        \\vec p(t, \\vec x) = \\sum_{\\vec x_n\\in\\mathcal N(\\vec x)} [ \\vec x_n - \\vec x ] z(t, \\vec x_n)

    with the complex phase :math:`z = \\mathrm e^{\\mathrm i \\phi}`
    for a neighbourhood of :math:`N` other grid points :math:`\\vec x_n \\in \\mathcal N(\\vec x)` around each point :math:`\\vec x`.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes in which to look for neighbours
    :param neighbours: the neighbourhood :math:`\\mathcal N` is given as a list of tuples of index shifts in the given ``axes`` which will be used to find the neighbours of each point :math:`\\vec x`. If instead a float is given, this float will be passed as the distance ``dist`` to :py:func:`neighbourhood` to construct a neighbourhood of all points in the selected dimension up to this distance.
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase
    """
    phase = np.exp(1j*phase)
    neighbours = neighbours if isinstance(neighbours, list) else neighbourhood(dim=len(axes), dist=neighbours)
    mom = np.zeros((len(axes), *phase.shape), dtype=phase.dtype)

    for shifts in neighbours:
        charge = np.roll(phase, shifts, axes)
        for i, shift in enumerate(shifts):
            mom[i] += charge*shift # TODO grid steps!

    defect = np.linalg.norm(mom, axis=0)
    defect = delete_border(defect, axes=axes, width=int(np.max(np.linalg.norm(neighbours, axis=1))))
    return defect

def defect_quadrupole(phase:np.ndarray, axes:List[int]=[-2,-1], neighbours:Union[float,List[List[int]]]=1.9, **ignored) -> np.ndarray:
    """
    Calculate phase defect :math:`\\rho = - | \\sum_i Q_{ii} |` using the quadrupole moment:

    .. math::

        Q_{ij}(t, \\vec x) = \\sum_{\\vec x_n\\in\\mathcal N(\\vec x)} \\left[ 3 x_{ni} x_{nj} - \\left| \\vec x \\right|^2 \\delta_{ij} \\right] z(t, \\vec x_n)

    with the complex phase :math:`z = \\mathrm e^{\\mathrm i \\phi}`
    for a neighbourhood of :math:`N` other grid points :math:`\\vec x_n \\in \\mathcal N(\\vec x)` around each point :math:`\\vec x`.

    .. warning:: The quadrupole method should not be used anymore. Use dipole method instead.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes in which to look for neighbours
    :param neighbours: the neighbourhood :math:`\\mathcal N` is given as a list of tuples of index shifts in the given ``axes`` which will be used to find the neighbours of each point :math:`\\vec x`. If instead a float is given, this float will be passed as the distance ``dist`` to :py:func:`neighbourhood` to construct a neighbourhood of all points in the selected dimension up to this distance.
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input phase

    .. seealso:: Dipole method :py:meth:`defect_dipole`
    """
    warnings.warn("The quadrupole method should not be used anymore. Use phase coherence (PC) instead.")

    phase = np.exp(1j*phase)
    neighbours = neighbours if isinstance(neighbours, list) else neighbourhood(dim=len(axes), dist=neighbours)
    mom = np.zeros((len(axes), len(axes), *phase.shape), dtype=phase.dtype)

    for shifts in neighbours:
        charge = np.roll(phase, shifts, axes)
        dist = np.linalg.norm(shifts)
        for i, si in enumerate(shifts):
            for j, sj in enumerate(shifts):
                mom[i,j] += charge*(mom.shape[0]*si*sj - (i==j)*dist**2) # TODO grid steps!

    defect = np.abs(np.trace(mom))
    defect = delete_border(defect, axes=axes, width=int(np.max(np.linalg.norm(neighbours, axis=1))))
    return defect

# === OTHER METHODS ===

def defect_glat(u:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, u_iso:Union[float,None]=None, **ignored) -> np.ndarray:
    """
    Calculate defect :math:`\\rho` using the gradient of the local arrival time:

    .. math::

        \\rho = || \\vec\\sigma ||_2

    where :math:`\\vec\\sigma = \\{\\sigma_x, \\sigma_y, ...\\}` is the edge based GLAT phase defect based on :py:meth:`ithildin.phase.defect_edge_glat`.

    :param u: variable :math:`u(t, \\vec x)` in the state space to calculate the arrival time for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param u_iso: passed on to :py:meth:`defect_edge_glat`
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input ``u``

    .. seealso::
        Method :py:meth:`defect_edge_glat`:
            phase defect :math:`\\vec\\sigma` centered on edges, which this method is based on
    """
    return np.linalg.norm(defect_edge_glat(u=u, axes=axes, deltas=deltas, u_iso=u_iso), axis=0)

def defect_topo_charge(u:np.ndarray, v:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored) -> np.ndarray:
    """
    Calculate defect :math:`\\rho` using the classical topologial charge, also known as the angular momentum:

    .. math::

        \\vec \\rho = \\nabla u \\times \\nabla v

    in two dimensions, :math:`\\rho` is the z-component of :math:`\\vec \\rho`.
    in three dimensions, :math:`\\rho` is the length of :math:`\\vec \\rho`.

    :param u: first variable :math:`u(t, \\vec x)` in the state space
    :param v: second variable :math:`v(t, \\vec x)` in the state space
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: non-negative phase defect field :math:`\\rho(t, \\vec x)` of same shape as input ``u``
    """
    assert(len(axes) in [2, 3])
    deltas = [1.]*len(axes) if deltas is None else [deltas[ax] for ax in axes]
    charge = np.cross(np.gradient(u, *deltas, axis=axes), np.gradient(v, *deltas, axis=axes), axis=0)
    defect = np.linalg.norm(charge, axes=0) if len(axes) == 3 else np.abs(charge)
    defect = delete_border(defect, axes=axes, width=1)
    return defect

# === EDGE BASED METHODS ===

def defect_edge_cpg(phase:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored) -> List[np.ndarray]:
    """
    Calculate phase defect :math:`\\vec\\sigma = \\{\\sigma_a\\}` on the edges using the maximal complex phase gradient (CPG):

    .. math::

        \\sigma_a = \\nabla_a z

    with the complex phase :math:`z = \\mathrm e^{\\mathrm i \\phi}`
    and the gradient in positive :math:`x_a` direction.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: list of non-negative phase defect fields :math:`\\vec\\sigma(t, \\vec x)` of same shape as input phase, one field :math:`\\sigma_a` for each axis :math:`a`

    .. seealso::
        Method :py:meth:`defect_cpg`:
            related phase defect centered on points
        Method :py:meth:`defect_edge_rpg`:
            related phase defect using real phase
    """
    return [np.roll(defect_cpg(phase, axes=[a], deltas=deltas), 1, axis=a) for a in axes]
defect_edge_mpg = defect_edge_cpg

def defect_edge_rpg(phase:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored) -> List[np.ndarray]:
    """
    Calculate phase defect :math:`\\vec\\sigma = \\{\\sigma_x, \\sigma_y, ...\\}` on the edges using the real phase gradient (RPG):

    .. math::

        \\sigma_{ab} = | U(\\phi_a, \\phi_b) |/\\Delta x

    for phases at nodes a and b which are a distance of :math:`\\Delta x` apart.
    Here, :math:`U(\\phi_a,\\phi_b)` is the phase difference as obtained from
    :py:meth:`phase.difference_phases`

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes to calculate defects for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: list of non-negative phase defect fields :math:`\\vec\\sigma(t, \\vec x)` of same shape as input phase, one field :math:`\\sigma_{x_d}` for each axis :math:`x_d`

    .. seealso::
        Method :py:meth:`defect_rpg`:
            related phase defect centered on points
        Method :py:meth:`defect_edge_cpg`:
            related phase defect using complex phase
    """
    deltas = [1.]*len(axes) if deltas is None else [deltas[ax] for ax in axes]
    return [delete_border(np.abs(difference_phases(np.roll(phase, 1, axis=a), phase))/dx, axes=[a], width=1) for dx, a in zip(deltas, axes)]

def defect_edge_ip(phase:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, **ignored) -> List[np.ndarray]:
    """
    Calculate phase defect :math:`\\vec\\sigma = \\{\\sigma_a\\}` on the edges using the interior point method (IP):

    .. math::
        \\sigma_a(t, \\vec x) = \\begin{cases} g(t, \\vec x) & \\mathrm{ if } H(t, \\vec x) H(t, \\vec x_a) < 0\\\\ 0 & \\mathrm{ else }\\end{cases}

    where

    .. math::
        H(t, \\vec x) = \\sum_{ij} g_i g_j H_{ij}

    with the :math:`2\\pi` periodicity respecting gradient normal vector
    :math:`g_i \\propto \\partial_i \\phi`, its absolute value :math:`g`,
    and Hessian :math:`H_{ij} = \\partial_i \\partial_j \\phi`.
    :math:`\\vec x_a` is the neighbour in positive direction for each dimension :math:`a` around each point :math:`\\vec x`.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param ignored: other keyword arguments are ignored
    :return: list of non-negative phase defect fields :math:`\\vec\\sigma(t, \\vec x)` of same shape as input phase, one field :math:`\\sigma_a` for each axis :math:`a`

    .. seealso::
        Method :py:meth:`defect_ip`:
            related phase defect :math:`\\rho` centered on points
    """
    H, g = hessian_in_dir_of_grad(phase, axes=axes, deltas=deltas, return_gradient=True)
    return [delete_border(np.where(np.roll(H, 1, axis=a)*H > 0, 0, g), axes=axes, width=2) for a in axes]

def defect_edge_cos(phase:np.ndarray, axes:List[int]=[-2,-1], **ignored) -> List[np.ndarray]:
    """
    Calculate phase defect :math:`\\vec\\sigma = \\{\\sigma_a\\}` on the edges using the cosine method:

    .. math::

        \\sigma_a = | 1 - \\cos(\\phi(\\vec x) - \\phi(\\vec x_a)) |

    for the neighbours :math:`\\vec x_a` in positive direction for each dimension :math:`a` around each point :math:`\\vec x`.

    Source??? TODO

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes in which to look for neighbours
    :param ignored: other keyword arguments are ignored
    :return: list of non-negative phase defect fields :math:`\\vec\\sigma(t, \\vec x)` of same shape as input phase, one field :math:`\\sigma_a` for each axis :math:`a`

    .. seealso::
        Method :py:meth:`defect_cos`:
            related phase defect :math:`\\rho` centered on points
    """
    return [0.5*delete_border(1. - np.cos(np.roll(phase, 1, axis=a) - phase), axes=[a], width=1) for a in axes]

def defect_edge_pc(phase:np.ndarray, axes:List[int]=[-2,-1], **ignored) -> List[np.ndarray]:
    """
    Calculate phase defect :math:`\\vec\\sigma = \\{\\sigma_a\\}` on the edges using the phase coherence method for each axis :math:`a`.

    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for
    :param axes: indices of spatial axes in which to look for neighbours
    :param ignored: other keyword arguments are ignored
    :return: list of non-negative phase defect fields :math:`\\vec\\sigma(t, \\vec x)` of same shape as input phase, one field :math:`\\sigma_a` for each axis :math:`a`

    .. seealso::
        Method :py:meth:`defect_pc`:
            related phase defect :math:`\\rho` centered on points
    """
    return [defect_pc(phase, axes=[a], neighbours=[(0,),(1,)]) for a in axes]

def defect_edge_glat(u:np.ndarray, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, u_iso:Union[float,None]=None, **ignored) -> List[np.ndarray]:
    """
    Calculate defect :math:`\\vec\\sigma = \\{\\sigma_a\\}` on the edges using the gradient of the local arrival time:

    .. math::

        \\sigma_a = | \\nabla_a t_\\text{elapsed} |

    using the local arrival time :math:`t_\\text{elapsed}(t, \\vec x)` as calculated by :py:meth:`ithildin.phase.time_elapsed`.
    :math:`\\nabla_a` denotes the derivative in direction :math:`a`.
    At the wave front, i.e. where the elapsed time is equal to zero, we additionally set :math:`\\sigma = 0`.

    :param u: variable :math:`u(t, \\vec x)` in the state space to calculate the arrival time for
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for the selected axes
    :param u_iso: passed on to :py:meth:`time_elapsed`
    :param ignored: other keyword arguments are ignored
    :return: list of non-negative phase defect fields :math:`\\vec\\sigma(t, \\vec x)` of same shape as input phase, one field :math:`\\sigma_a` for each axis :math:`a`

    .. seealso::
        Method :py:meth:`defect_glat`:
            related phase defect :math:`\\rho` centered on points
    """
    deltas = [1.]*len(axes) if deltas is None else [deltas[ax] for ax in axes]
    t = time_elapsed(u) if u_iso is None else time_elapsed(u, u_iso=u_iso)
    def diff(t, a, dx):
        t_ = np.roll(t, 1, axis=a)
        with np.errstate(invalid="ignore"):
            dt = t_ - t
        dt[t ==0.] = 0.
        dt[t_==0.] = 0.
        return np.abs(dt)/dx
    return [delete_border(diff(t, a, dx), axes=[a], width=1) for a, dx in zip(axes, deltas)]

# === HELPER FUNCTIONS ===

def threshold(v:np.ndarray, f:float=0.5) -> np.ndarray:
    """
    Set values below threshold to nan. The threshold for this is calculated as follows:

    .. math::

        v < \\min v + f [ \\max v - \\min v ]

    :param v: any data
    :param f: fraction :math:`0 \\le f \\le 1`
    :return: modified array ``v``
    """
    mi, ma = np.nanmin(v), np.nanmax(v)
    return np.where(v > mi + f*(ma - mi), v, np.nan)

def gradient_periodic(phi:np.ndarray, axes:List[int], deltas:List[int]) -> np.ndarray:
    """
    Calculate the gradient in a way that respects the mod :math:`2\\pi` periodicity of ``phi``.

    :param phi: array to compute the gradient for
    :param axes: list of axes of ``phi`` to compute the gradient along
    :param deltas: grid sizes along the selected axes
    :return: gradient along the given axes, the shape of this array is ``(len(axes), *phi.shape)``
    """
    return np.array([difference_phases(np.roll(phi, -1, axis=axis), np.roll(phi, +1, axis=axis))/(2*delta) for axis, delta in zip(axes, deltas)])

def hessian_in_dir_of_grad(f, axes:List[int]=[-2,-1], deltas:Union[List[float],None]=None, operator_gradient:callable=gradient_periodic, epsilon:float=1e-20, return_gradient:bool=False) -> Union[np.ndarray,Tuple[np.ndarray,np.ndarray]]:
    """
    Calculate the Hessian in direction of the gradient, i.e. the second derivative:

    .. math::

        \\sum_{ij} g_i g_j H_{ij}

    with the :math:`2\\pi` periodicity respecting gradient normal vector
    :math:`g_i \\propto \\partial_i \\phi`
    and Hessian :math:`H_{ij} = \\partial_i \\partial_j \\phi`.

    :param f: array of which to calculate Hessian
    :param axes: indices of spatial axes to calculate the derivatives for
    :param deltas: grid steps for all the axes
    :param operator_gradient: function that calculates the gradient with this signature:

        g(f:np.ndarray, axis:List[int], deltas:List[float]) -> List[np.ndarray]

    :param epsilon: small positive number to avoid division by zero
    :param return_gradient: if this parameter is set to True, return the Hessian and the absolute value of the gradient
    :return: array(s) of the same shape as input ``f`` Hessian, and maybe the absolute value of the gradient
    """
    deltas = [1.]*len(axes) if deltas is None else deltas
    g = operator_gradient(f, axes, deltas)
    H = np.empty((len(axes), len(axes))+f.shape, dtype=f.dtype)

    for i, g_i in enumerate(g):
        for j, H_ij in enumerate(operator_gradient(g_i, axes, deltas)):
            H[i,j,:,:] = H_ij

    absgrad = np.linalg.norm(g, axis=0)
    g /= absgrad + epsilon
    hidog = np.einsum("i...,j...,ij...->...", g, g, H)
    if return_gradient:
        return hidog, absgrad
    else:
        return hidog

def defect_simdata(simdata:"SimData", algo:Union[str,None]=None, phase:Union[str,np.ndarray]="default", **kwargs) -> np.ndarray:
    """
    Calculate the phase defect field :math:`\\rho(t, \\vec x)` based on simulation data.

    :param simdata: simulation data for which to calculate the phase defect
    :param algo: the algorithm to use to calculate the phase defect
    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for. If this is the string, it describes the algorithm to use to calculate phase and will be passed on to ``simdata.phase``.
    :param kwargs: passed on to the method to calculate the phase defect
    :return: the phase defect field :math:`\\rho(t, \\vec x)`, an array of the same size as ``phase``

    .. seealso::
        Method :py:meth:`defect_simdata`:
            related phase defect :math:`\\vec\\sigma` centered on edges
    """
    algorithms = dict(
            default="pc",
            mpg="cpg", pg="cpg", cpg="cpg",
            rpg="rpg",
            mph="ip", ip="ip",
            pc="pc",
            cos="cos", cosine="cos",
            glat="glat", lat="glat",
            svf="svf", spatialvecfield="svf", spatial_vec_field="svf",
            tc="tc", topo="tc", topocharge="tc", topo_charge="tc",
            di="di", dipole="di",
            qu="qu", quadrupole="qu",
    )
    if algo not in algorithms:
        raise ValueError(f"Invalid algorithm {algo}! Must be one of {list(algorithms.keys())}.")
    algo = algorithms[algo or "default"]

    if isinstance(phase, str) and algo not in ["glat", "tc"]:
        phase = simdata.phase(phase)

    axes = np.arange(-simdata.dim, 0)
    deltas = [simdata.deltas[axis] for axis in axes]
    kwargs = {"axes":axes, "deltas":deltas, **kwargs}

    if algo == "rpg":
        return defect_rpg(phase, **kwargs)
    elif algo == "cpg":
        return defect_cpg(phase, **kwargs)
    elif algo == "ip":
        return defect_ip(phase, **kwargs)
    elif algo == "pc":
        return defect_pc(phase, **kwargs)
    elif algo == "cos":
        return defect_cos(phase, **kwargs)
    elif algo == "svf":
        return defect_spatial_vec_field(phase, **kwargs)
    elif algo == "glat":
        warnings.warn("Using glat defect. Using `u=simdata.vars[u]` and ignoring argument `phase`.")
        u = kwargs.get("u", "u")
        if "u" in kwargs: del kwargs["u"]
        u = (1 - simdata.vars[u[2:]] if u[:2] == "1-" else simdata.vars[u]) if isinstance(u, str) else u
        return defect_glat(u, **kwargs)
    elif algo == "tc":
        warnings.warn("Using topo phase defect. Using `u=simdata.vars[u]` and `v=simdata.vars[v]` and ignoring argument `phase`.")
        u = kwargs.get("u", "u")
        v = kwargs.get("v", "v")
        if "u" in kwargs: del kwargs["u"]
        if "v" in kwargs: del kwargs["v"]
        u = (1 - simdata.vars[u[2:]] if u[:2] == "1-" else simdata.vars[u]) if isinstance(u, str) else u
        v = (1 - simdata.vars[v[2:]] if v[:2] == "1-" else simdata.vars[v]) if isinstance(v, str) else v
        return defect_topo_charge(u, v, **kwargs)
    elif algo == "di":
        return defect_dipole(phase, **kwargs)
    elif algo == "qu":
        return defect_quadrupole(phase, **kwargs)
    else:
        raise ValueError

def defect_edge_simdata(simdata:"SimData", algo:Union[str,None]=None, phase:Union[str,np.ndarray]="default", **kwargs) -> List[np.ndarray]:
    """
    Calculate the phase defect field :math:`\\vec\\sigma = \\{\\sigma_a\\}` on edges based on simulation data.

    :param simdata: simulation data for which to calculate the phase defect
    :param algo: the algorithm to use to calculate the phase defect
    :param phase: phase :math:`\\phi(t, \\vec x)` to calculate defects for. If this is the string, it describes the algorithm to use to calculate phase and will be passed on to ``simdata.phase``.
    :param kwargs: passed on to the method to calculate the phase defect
    :return: list of non-negative phase defect fields :math:`\\vec\\sigma(t, \\vec x)` of same shape as input phase, one field :math:`\\sigma_a` for each axis :math:`a`

    .. seealso::
        Method :py:meth:`defect_simdata`:
            related phase defect :math:`\\rho` centered on points
    """
    algorithms = dict(
            default="pc",
            mpg="cpg", pg="cpg", cpg="cpg",
            rpg="rpg",
            ip="ip", mph="ip", ph="ip",
            cos="cos",
            pc="pc",
            glat="glat", lat="glat",
    )
    if algo not in algorithms:
        raise ValueError(f"Invalid algorithm {algo}! Must be one of {list(algorithms.keys())}.")
    algo = algorithms[algo or "default"]

    if isinstance(phase, str) and algo not in ["glat"]:
        phase = simdata.phase(phase)

    axes = np.arange(-simdata.dim, 0)
    kwargs = {"axes":axes, "deltas":simdata.deltas, **kwargs}

    if algo == "rpg":
        return defect_edge_rpg(phase, **kwargs)
    elif algo == "cpg":
        return defect_edge_cpg(phase, **kwargs)
    elif algo == "ip":
        return defect_edge_ip(phase, **kwargs)
    elif algo == "pc":
        return defect_edge_pc(phase, **kwargs)
    elif algo == "cos":
        return defect_edge_cos(phase, **kwargs)
    elif algo == "glat":
        warnings.warn("Using glat edge defect. Using `u=simdata.vars['u']` and ignoring argument `phase`.")
        return defect_edge_glat(simdata.vars["u"], **kwargs)

    else:
        raise ValueError
