#!/bin/env python3
"""
Helper functions: graphs
------------------------

Collection of functions for some basic graph analysis.

This builds upon the :py:mod:`networkx` module.
"""

from collections import deque
from scipy.spatial.distance import pdist, squareform
from typing import List, Tuple, Union
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

def distances(graph:nx.Graph, start:Union[int,None]=None) -> List[float]:
    """
    Calculate distances from start node to all other nodes using a breadth first approach.

    :param graph: undirected, weighted tree
    :param start: integer index of the node to start the search from
    :return: list of distances to all nodes
    """
    if start is None:
        start = list(graph.nodes)[0]
    dist = np.full(max(graph.nodes)+1, np.nan, dtype=np.float64)
    dist[start] = 0.
    queue = deque()
    queue.append(start)
    while queue:
        i = queue.popleft()
        for j in graph.adj[i]:
            if np.isnan(dist[j]):
                dist[j] = dist[i] + graph[i][j]["weight"]
                queue.append(j)
    return dist

def longest_path(graph:nx.Graph) -> Tuple[int, int, float]:
    """
    Find longest path in an undirected, weighted tree.

    :param graph: undirected, weighted tree
    :return: index of start node, index of end node, distance between nodes
    """
    dist = distances(graph) # distances from node 0
    start = np.nanargmax(dist) # start is farthest node from node 0
    dist = distances(graph, start) # distances from start node
    end = np.nanargmax(dist) # end is farthest node from start
    return start, end, dist[end]

def clip_longest_path(graph:nx.Graph) -> Tuple[List[int], nx.Graph]:
    """
    Remove nodes that are in the longest path from the path and return that
    longest path and the clipped graph.

    :param graph: any undirected graph, will be turned into its minimum spanning tree
    :return: indices of the nodes of the longest path, and the modified graph
    """
    graph = nx.algorithms.tree.mst.minimum_spanning_tree(graph, weight="weight")
    start, end, _ = longest_path(graph)
    path = next(nx.shortest_simple_paths(graph, start, end))

    for i, j in zip(path, path[1:]):
        graph.remove_edge(i, j)
    graph.remove_nodes_from(node for node, degree in dict(graph.degree()).items() if degree == 0)

    return path, graph

def clip_longest_path_directed(graph:nx.DiGraph) -> Tuple[List[int], nx.DiGraph]:
    """
    Remove nodes that are in the longest path from the path and return that
    longest path and the clipped graph.

    :param graph: any directed acyclic graph
    :return: indices of the nodes of the longest path, and the modified graph
    """
    path = nx.dag_longest_path(graph)

    for i, j in zip(path, path[1:]):
        graph.remove_edge(i, j)
    graph.remove_nodes_from(node for node, degree in dict(graph.degree()).items() if degree == 0)

    return path, graph

Segment = Tuple[Tuple[float,float],Tuple[float,float]]
def segments_in_square(case:int, x0:float=0, x1:float=1, y0:float=0, y1:float=1) -> List[Segment]:
    """
    Get list of all contour segments going through the square [x0,x1]*[y0,y1] for the given contour case.

    :param case: the binary values of the four edges encoded as one integer::

        case = 2**n*b_n for n in [top left, top right, bottom right, bottom left]

    :param x0: left x value of the square
    :param x1: right x value of the square
    :param y0: bottom y value of the square
    :param y1: top y value of the square

    :return: list of line segments composing the contours,
        i.e. list of starting and end points ((xa, ya), (xe, ye)) for each segment

    :seealso: :py:meth:`marching_squares`
    """
    segments = []
    if case in (0, 15):
        return []
    xm = (x0 + x1)/2
    ym = (y0 + y1)/2
    if case in (1, 14, 10):
        segments.append([(x0, ym), (xm, y1)])
    if case in (2, 13, 5):
        segments.append([(xm, y1), (x1, ym)])
    if case in (3, 12):
        segments.append([(x0, ym), (x1, ym)])
    if case in (4, 11, 10):
        segments.append([(xm, y0), (x1, ym)])
    elif case in (6, 9):
        segments.append([(xm, y1), (xm, y0)])
    elif case in (7, 8, 5):
        segments.append([(xm, y0), (x0, ym)])
    return segments

def merge_segments(segments:List[Segment], maxdist:float=1e-20) -> List[np.ndarray]:
    """
    Connect 2D segments of lines together to lines in 2D.

    :param segments: list of line segments ((xs, ys), (xe, ye))
    :param maxdist: maximum distance for two points to be considered approximately the same point
    :return: list of lines, i.e. list of list of points (x, y)
    """
    if len(segments) == 0:
        return segments
    segments = np.array(segments)
    assert(segments.ndim == 3)
    assert(segments.shape[1:] == (2, 2))
    points = segments.reshape((-1, 2))

    # link points on same vertex together
    dist = squareform(pdist(points) < maxdist)

    # link points in input lines together
    i = np.arange(dist.shape[0])
    dist[i,i+1-2*(i%2)] = True

    # find longest paths for all connected components
    graph = nx.Graph(dist)
    merged = []
    for comp in nx.connected_components(graph):
        subgraph = graph.subgraph(comp)
        start, end, _ = longest_path(subgraph)
        for path in nx.shortest_simple_paths(subgraph, start, end):
            merged.append(points[path])

    return merged

def marching_squares_old(x:np.ndarray, y:np.ndarray, data:np.ndarray, threshold:float) -> List[np.ndarray]:
    """
    The marching square algorithm can be used to find the contour at which ``data == threshold``.

    :param x: 1D list of x values of the 2D rectangular grid
    :param y: 1D list of y values of the 2D rectangular grid
    :param data: 2D data at the vertices of the 2D rectangular grid
    :param threshold: value at which to place the contour
    :return: list of contour lines, i.e. list of list of points (x, y)

    :seealso: :py:meth:`segments_in_square`
    """
    assert(x.ndim == 1)
    assert(y.ndim == 1)
    assert(data.ndim == 2)
    assert(data.shape == (*y.shape, *x.shape))

    segments = []
    values = [np.roll(data, shifts, (0, 1)) for shifts in [(-1, 0), (-1, -1), (0, -1), (0, 0)]]
    cases = sum(2**n*(v > threshold) for n, v in enumerate(values))
    for j in range(data.shape[0] - 1):
        for i in range(data.shape[1] - 1):
            segments.extend(segments_in_square(cases[j,i], x[i], x[i+1], y[j], y[j+1]))

    return merge_segments(segments)

def marching_squares_matplotlib(x:np.ndarray, y:np.ndarray, data:np.ndarray, threshold:float) -> List[np.ndarray]:
    """
    The marching square algorithm can be used to find the contour at which ``data == threshold``.

    :param x: 1D list of x values of the 2D rectangular grid
    :param y: 1D list of y values of the 2D rectangular grid
    :param data: 2D data at the vertices of the 2D rectangular grid
    :param threshold: value at which to place the contour
    :return: list of contour lines, i.e. list of list of points (x, y)

    .. note: ``x`` and ``y`` can also be a meshed grid

    Usage example::

        import numpy as np
        import matplotlib.pyplot as plt
        from ithildin.graph import marching_squares
        x = np.linspace(0, 5, 30)
        y = np.linspace(0, 10, 30)
        xx, yy = np.meshgrid(x, y)
        example = np.sin(xx)*np.cos(xx**2+yy)
        plt.pcolormesh(x, y, example, shading="nearest")
        for line in marching_squares(x, y, example, 0.2):
            plt.plot(line[:,0], line[:,1], "k-")
        plt.show()
    """
    fig, ax = plt.subplots()
    c = ax.contour(x, y, data, levels=[threshold])
    c = [np.array(ci) for ci in c.allsegs[0]]
    plt.close(fig)
    del fig, ax
    return c

marching_squares = marching_squares_matplotlib

def neighbourhood(dim:int=2, dist:float=1.9, return_all:bool=False) -> List[Tuple[int,...]]:
    """
    Calculate index offsets of the nearest neighbours in a square grid in
    ``dim`` dimensions up to a grid distance of ``dist``.

    :param dim: dimension of the grid to consider, e.g. 1D, 2D, 3D, ...
    :param dist: maximum distance of points to return, by default consider direct and diagonal neighbours.

        * set ``dist = 1.`` to only consider direct neighbours (4 neighbours in 2D, 6 in 3D)
        * set ``dist > 2**.5`` to also consider diagonal neighbours (8 neighbours in 2D, 26 in 3D)

    :param return_all: if this flag is enabled, do not filter by distance
    :return: list of index offsets of neighbours, tuples of length ``dim``
    """
    indices = list(range(-int(dist),int(dist)+1))
    if dim == 1:
        neighbours = [(i,) for i in indices]
    else:
        neighbours = []
        for neighbour in neighbourhood(dim=dim-1, dist=dist, return_all=True):
            for i in indices:
                neighbours.append((*neighbour, i))
    if not return_all:
        neighbours = [neighbour for neighbour in neighbours if np.linalg.norm(neighbour) <= dist and np.any(neighbour)]
    return neighbours

def delete_border(v:np.ndarray, axes:List[int], width:int=1) -> np.ndarray:
    """
    Set values at the border to nan (to mark as invalid).

    .. warning::

        This function modifies the array ``v``.

    :param v: array of at least ``dim`` dimensions
    :param axes: axes to set the border to nan for
    :param width: number of grid points at the border to set to nan
    :return: modified array ``v``
    """
    for axis in axes:
        np.moveaxis(v, axis, 0)[range(-width, width)] = np.nan
    return v
