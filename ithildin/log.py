#!/bin/env python3
"""
Log files
---------

Parse ithildin log files. The class Log is used to make the contents of a logfile easily accessible.
"""

from typing import Dict, Tuple, List, Union
import datetime
import re
import os
import yaml
import numpy as np
import warnings

def log_to_dict(s:str) -> Dict:
    """
    Convert the contents of a logfile to YAML, parse YAML and return the resulting dict.

    :param s: contents of a logfile

    :return: dictionary representing the contents of the logfile
    """
    # if "was run on" is on the first line, use ithildin log format, else it already is YAML
    n_was_run_on = len([m.start() for m in re.finditer(" was run on ", s)])
    if n_was_run_on > 1:
        raise Exception("Found the line '... was run on ...' multiple times in the log file!")
    elif n_was_run_on == 1:
        new_item_markers = [
            ("Source at time", 0),
            ("S2", 1),
        ]
        indents = ["", " "*4, " "*8]

        # fixes
        s = re.sub(r"sawtooth", "\tsawtooth", s) # missing tab before sawtooth
        s = re.sub(r":rectangular", ":\trectangular", s) # missing tab
        s = re.sub(r":spherical", ":\tspherical", s) # missing tab

        # convert to yaml
        s = re.sub(r" *\t[ \t]*", r"\t", s) # clean up whitespace
        s = re.sub(r"(\t*\r?\n)+", r"\n", s) # clean up end of line
        s = re.sub(r"\n([^\n:]*):?\n-*\n", r"\n#\1:\n", s) # mark headlines with '#'
        s = re.sub(r"^Inhom\s*([0-9]+):\s*([^\n]+)\n", indents[1]+r"Inhom \1:\n"+indents[2]+r"type: \2\n", s, flags=re.MULTILINE) # header inhomogenity
        s = re.sub(r"^\s+([a-z0-9]+)\s+([-0-9.]+)\n", indents[2]+r"\1: \2\n", s, flags=re.MULTILINE) # indented key value
        s = re.sub(r"^\s+([a-z0-9]+)\s*,\s*([a-z0-9]+)\s+([-0-9.]+)\s+([-0-9.]+)\n", indents[2]+r"\1: \3\n"+indents[2]+r"\2: \4\n", s, flags=re.MULTILINE) # indented key, key value value
        s = re.sub(r"^\s+([a-z0-9]+)\s*,\s*([a-z0-9]+)\s*,\s*([a-z0-9]+)\s+([-0-9.]+)\s+([-0-9.]+)\s+([-0-9.]+)\n", indents[2]+r"\1: \4\n"+indents[2]+r"\2: \5\n"+indents[2]+r"\3: \6\n", s, flags=re.MULTILINE) # indented key, key, key value value value
        s = re.sub(r"^([^:\t\n]+):?\t", indents[1]+r"\1: ", s, flags=re.MULTILINE) # key value (without indent in front)
        s = re.sub(r"\t", r" ", s) # clean up whitespace
        s = re.sub(r"^\s*#", "", s, flags=re.MULTILINE) # dedent headline
        s = re.sub(r"^\s*([^\n]*) was run on ([^\n]*)\n", r"Name: \1\nStart date: \2\n", s) # first line
        s = re.sub(r"(-?[0-9.]+) +(?=[-0-9])", r"\1, ", s) # commas for arrays
        s = re.sub(r" (-?[0-9.]+, [-0-9., ]+)\n", r" [\1]\n", s) # brackets around arrays
        s = re.sub(r"\n[^:]*$", "", s, flags=re.MULTILINE) # delete lines without ':'

        # convert date formats
        while True:
            m = re.search(r'date: (\w{3} \w{3}  ?\d{1,2},? \d{2}:\d{2}:\d{2},? \d{4})', s)
            if m:
                try:
                    date = datetime.datetime.strptime(m.group(1).replace(',', ''), '%a %b %d %H:%M:%S %Y')
                except ValueError as e:
                    warnings.warn(f"Date can not be parsed: {e}")
                    break
                s = s[:m.start(1)] + date.strftime('%Y-%m-%d %H:%M:%S') + s[m.end(1):]
            else:
                break

        # multi models
        pos = s.find("Number of models")
        if pos >= 0:
            s = re.sub(r"\n\s*Number of models([^\n]*)\n", r"\nNumber of models\1\nModels:\n", s)
            s = re.sub(r"\nModel parameters:\n", r"\n  -\n", s)

        # mark beginning of new item in a list
        for m, c in new_item_markers:
            s = re.sub(r"^\s*(" + m + r")", r"  -\n    \1", s, flags=re.MULTILINE, count=c)

    return yaml.load(s, Loader=yaml.SafeLoader)

def value_value_unit(s:str) -> Tuple[float, float, str]:
    "split a string like '0.9 = 99. timesteps' into its parts"
    t = re.sub(r"\s+", " ", s).split(" ")
    return float(t[0]), float(t[2]), t[3]

def fill_to_length(x:list, length:int, value=None) -> list:
    if isinstance(x, float) or isinstance(x, int):
        return fill_to_length([x], length, value)
    while len(x) < length:
        x.append(value)
    return x

def fill_to_length_rev(x:list, length:int, value=None) -> list:
    return list(reversed(fill_to_length(x, length, value)))

class Log:
    """
    Contents of a logfile.

    :param path: path of the logfile to read
    :param data: parsed data assigned to groups and fields, overrides data in logfile

    :var text: full contents of the logfile
    :vartype text: str
    :var full_data: parsed data assigned to groups and fields (full_data[group][key] = value)
    :vartype full_data: dict[dict]
    """
    def __init__(self, path:Union[str,None]=None, data:Union[dict,None]=None):
        self.path = path
        if data is None:
            data = dict()
        if path:
            with open(path, "r") as f:
                self.text = f.read()
            self.full_data = {**log_to_dict(self.text), **data}
        else:
            self.full_data = data
        assert self.version >= 1, "Log file version must be at least 1."
        assert self.version <= 3, "Log file version must be at most 3."

    def __str__(self) -> str:
        attrs = [p for p in dir(self) if p[0].isalpha() and not p in ["text", "yaml", "to_yaml", "fields", "add_field", "add_inhom", "add_model", "add_source", "filter_modelparams"]]
        attrs = dict(zip(attrs, [getattr(self, a) for a in attrs]))
        return yaml.safe_dump(dict(Log=attrs))

    def __repr__(self) -> str:
        return f"Log(data={self.full_data})"

    def __getitem__(self, key):
        return self.full_data[key]

    def _get(self, default, *keys):
        ret = self.full_data
        for k in keys:
            if isinstance(k, int):
                if k < len(ret):
                    ret = ret[k]
                else:
                    return default
            else:
                if k in ret:
                    ret = ret[k]
                else:
                    return default
        return ret

    def _set(self, value, key, *keys, cur=None):
        if cur is None:
            cur = self.full_data

        if len(keys) == 0:
            cur[key] = value
        else:
            if not key in cur.keys():
                cur[key] = dict()
            self._set(value, *keys, cur=cur[key])

    def _fields(self, key:str) -> List[Tuple[int,dict]]:
        assert(self.version >= 2)
        pairs = []
        for k, field in self.full_data.items():
            m = re.match(r'^'+key+r' ?([0-9]*)', k)
            if m:
                k = m.groups()[0]
                k = int(k) if len(k) > 0 else 0
                pairs.append((k, field))
        pairs.sort(key=lambda pair: pair[0])
        return pairs

    def fields(self, key:str):
        """
        Return all fields in ``full_data`` that begin with the given key and
        sort by numeric value that follows.

        :param key: beginning of the field label
        :return: sorted list of the fields matching this label
        """
        pairs = self._fields(key)
        return [field for _, field in pairs]

    def add_field(self, key:str, value:dict):
        """
        Append another field to the list of fields of a given key (as in :py:meth:`fields`).

        :param key: beginning of the field label
        :param value: value to set the field to
        """
        existing = self._fields(key)
        if len(existing) == 0:
            self.full_data[key] = value
        else:
            highest_index = existing[-1][0]
            self.full_data[f'{key} ({highest_index+1})'] = value

    def to_yaml(self) -> str:
        "Convert the logfile to YAML format."
        return yaml.safe_dump(self.full_data)

    @property
    def yaml(self) -> str:
        "the logfile in YAML format"
        return self.to_yaml()

    @property
    def version(self) -> int:
        "version of the log file"
        return self._get(1, "Ithildin log version")

    @property
    def version_lib(self) -> Union[None,str]:
        "version of the library used to generate log"
        return self._get(None, 'Simulation parameters', 'Ithildin library version')

    @property
    def simseries(self) -> str:
        "name of the simulation series"
        return self._get("ithildin", "Simulation parameters", "Name of simulation series")
    @simseries.setter
    def simseries(self, value:str):
        self._set(str(value), "Simulation parameters", "Name of simulation series")

    @property
    def serialnr(self) -> int:
        "serial number of the simulation series"
        return self._get(1, "Simulation parameters", "Serial number")
    @serialnr.setter
    def serialnr(self, value:int):
        self._set(int(value), "Simulation parameters", "Serial number")

    @property
    def stem(self) -> str:
        "stem for filenames containing series name and serial number"
        return f"{self.simseries}_{self.serialnr}"
    @stem.setter
    def stem(self, stem:str):
        s = stem.split('_')
        self.serialnr = int(s[-1])
        self.simseries = '_'.join(s[:-1])

    @property
    def date(self) -> str:
        "date and time when the simulation was started"
        return self._get(None, "Start date")
    @date.setter
    def date(self, value:str):
        self._set(str(value), "Start date")

    @property
    def nproc(self) -> int:
        "number of used MPI processes during simulation"
        if self.version >= 3:
            return self._get(0, "MPI variables", "Number of processes")
        else:
            return self._get(0, "MPI variables", "Number of slaves")
    @nproc.setter
    def nproc(self, value:int):
        if self.version >= 3:
            self._set(int(value), "MPI variables", "Number of processes")
        else:
            self._set(int(value), "MPI variables", "Number of slaves")
    nslaves = nproc # for backwards compatibility

    @property
    def inhoms(self) -> List[dict]:
        "list of inhomogenities"
        if self.version >= 2:
            return self.fields("Inhom")
        else:
            keys = sorted(k for k in self.geometry.keys() if k.split(" ")[0] == "Inhom")
            r = [self.geometry[k] for k in keys]
            assert(self.geometry.get("Number of inhomog.", 0) == len(r))
            return r

    def add_inhom(self, inhom:dict):
        self.add_field("Inhom", inhom)

    @property
    def geometry(self) -> dict:
        "geometry parameters"
        key = "Geometry parameters"
        if not key in self.full_data:
            self.full_data[key] = {}
        return self.full_data[key]

    def _geometry_split_type_and_files(self):
        "geometry parameters"
        s = self.geometry.get("Anisotropy type", None)
        if s is None:
            return
        m = re.match(r'^(.*) files(.*)$', s)
        if m:
            self.geometry["Anisotropy type"] = m.groups()[0]
            self.geometry["File"] = os.path.basename(m.groups()[1])

    @property
    def geomtype(self) -> Union[None,str]:
        "type of geometry used in the simulation"
        self._geometry_split_type_and_files()
        return self.geometry.get("Anisotropy type", None)
    @geomtype.setter
    def geomtype(self, value:str):
        self._geometry_split_type_and_files()
        self.geometry["Anisotropy type"] = value

    @property
    def geomfile(self) -> Union[None,str]:
        "base names of the file used as the geometry used in the simulation"
        self._geometry_split_type_and_files()
        return self.geometry.get("File", None)
    @geomfile.setter
    def geomfile(self, value:str):
        self._geometry_split_type_and_files()
        self.geometry["File"] = value

    @property
    def boundary_condition(self) -> Union[None,str]:
        "boundary conditions used in the simulation"
        return self.geometry.get("Boundary conditions", None)
    @boundary_condition.setter
    def boundary_condition(self, value:str):
        self.geometry["Boundary conditions"] = value

    @property
    def sources(self) -> List[dict]:
        "list of sources"
        if self.version >= 2:
            return self.fields("Source")
        else:
            key = "Source(s)"
            if not key in self.full_data:
                self.full_data[key] = []
            sources = [s for s in self.full_data[key] if "Source at time" in s]
            for s in sources:
                badkey = "Source geometry"
                if badkey in s:
                    s["Source"] = s[badkey]
            return sources

    def add_source(self, source:dict):
        self.add_field("Source", source)

    @property
    def sourcetimes(self) -> List[float]:
        "times at which the sources have been added"
        key = "Source at time" if self.version < 2 else "Time"
        return [s[key] for s in self.sources]

    @property
    def sourcetypes(self) -> List[str]:
        "types of each of the sources"
        key = "Type" if self.version >= 2 else "Source"
        return [s[key] for s in self.sources]

    @property
    def sourcefile(self) -> Union[None,str]:
        "file "
        if len(self.sources) == 0:
            return None
        s = self.sources[0].get("Values copied from", None)
        if s is not None:
            s = re.sub(r'^Data copied .* from ', '', s)
        return s

    @property
    def model(self) -> Union[dict,None]:
        "parameters of the used model"
        models = self.models
        if len(models) > 0:
            return self.models[0]

    @property
    def models(self) -> List[dict]:
        "list of all models"
        if self.version >= 2:
            return self.fields("Model parameters")
        elif "Models" in self.full_data:
            return self.full_data["Models"]
        else:
            key = "Model parameters"
            if not key in self.full_data:
                self.full_data[key] = {}
            return [self.full_data[key]]

    @property
    def varnames(self) -> List[str]:
        if self.model is None:
            warnings.warn('log.varnames is not defined!')
            return []
        if 'Variable names' not in self.model:
            warnings.warn('log.varnames is not defined!')
        return self.model.get('Variable names', [])
    @varnames.setter
    def varnames(self, varnames:List[str]):
        self.model['Variable names'] = varnames

    def add_model(self, model:dict):
        self.add_field("Model parameters", model)

    def filter_modelparams(self, model:dict) -> dict:
        """
        Create a dictionary of all the parameters of the model
        which are stored in the log.
        :param model: dictionary describing a  model, see :py:meth:`models`
        :return: filtered dictionary containing only parameters
        """
        return {key: value for key, value in model.items() if key not in ["Model type", "Number of vars", "Variable names", "Tipline variables", "Tipline isovalues"]}

    @property
    def modelparams(self) -> dict:
        "get the key value pairs of the model's parameters"
        return self.filter_modelparams(self.model)

    @property
    def modeltype(self) -> str:
        "the name of the used model"
        return self.model.get("Model type", None)
    @modeltype.setter
    def modeltype(self, value:str):
        for model in self.models:
            model["Model type"] = str(value)

    @property
    def nvars(self) -> int:
        "number of variables in the model"
        return self.model.get("Number of vars", None)
    @nvars.setter
    def nvars(self, value:int):
        for model in self.models:
            model["Number of vars"] = value

    @property
    def ndim(self) -> int:
        "number of dimensions in space, i.e. 1, 2 or 3"
        return self.geometry.get("Number of dimensions", 3)
    @ndim.setter
    def ndim(self, value:int):
        self.geometry["Number of dimensions"] = int(value)

    @property
    def nframes(self) -> int:
        "number of frames to take"
        return self._get(None, "Simulation parameters", "Number of frames to take")
    @nframes.setter
    def nframes(self, value:int):
        self._set(int(value), "Simulation parameters", "Number of frames to take")

    @property
    def nsteps(self) -> int:
        "number of steps to take"
        n = self._get(None, "Simulation parameters", "Number of total steps")
        if n is None:
            n = value_value_unit(self._get("0 = 0 timesteps", "Simulation parameters", "Duration of simulated process"))[1]
        return round(n)
    @nsteps.setter
    def nsteps(self, value:int):
        self._set(int(value), "Simulation parameters", "Number of total steps")

    @property
    def nfirst(self) -> int:
        "index of first frame"
        return self._get(0, "Simulation parameters", "Index of first frame")
    @nfirst.setter
    def nfirst(self, value:int):
        assert(self.version >= 2)
        # assert(value >= 0)
        return self._set(int(value), "Simulation parameters", "Index of first frame")

    @property
    def domainsize(self) -> Tuple[int,...]:
        "number of grid points in each of the spatial dimensions, size of one frame, (Nz, Ny, Nx)"
        return tuple(fill_to_length_rev(self._get([None]*max(3,self.ndim), "Geometry parameters", "Domain size"), max(3,self.ndim), 1))
    @domainsize.setter
    def domainsize(self, values:Tuple[int,...]):
        assert(len(values) == max(3,self.ndim))
        self._set(tuple(reversed(values)), "Geometry parameters", "Domain size")

    @property
    def shape(self) -> Tuple[int,...]:
        "total shape of a variable, (Nfr, Nz, Ny, Nx)"
        return tuple([self.nframes, *list(self.domainsize)])
    @shape.setter
    def shape(self, values:Tuple[int,...]):
        self.nframes = values[0]
        self.domainsize = values[1:]

    @property
    def shape_space(self) -> Tuple[int,...]:
        return self.shape[1:]
    @shape_space.setter
    def shape_space(self, shape_space:Tuple[int,...]):
        self.shape = (self.shape[0], *shape_space)

    @property
    def tfirst(self) -> float:
        "time of first frame in milliseconds"
        if self.version >= 2:
            return self.nfirst*self.tframe
        else:
            return value_value_unit(self._get("0 = 0 frame durations", "Simulation parameters", "Time of first frame"))[0]
    @tfirst.setter
    def tfirst(self, value:float):
        if self.version >= 2:
            self.nfirst = value/self.tframe
        else:
            value = float(value)
            self._set(f"{value} = {value/self.tframe} frame durations", "Simulation parameters", "Time of first frame")

    @property
    def tlast(self) -> float:
        "time of last frame in milliseconds"
        return self.tfirst + self.shape[0]*self.tframe

    @property
    def tend(self) -> float:
        "duration of the simulated process"
        if self.version >= 2:
            return self.nsteps*self.dt
        else:
            s = self._get(None, "Simulation parameters", "Duration of simulated process")
            if s: return value_value_unit(s)[0]
            else: return np.nan
    @tend.setter
    def tend(self, value:float):
        if self.version >= 2:
            self.nsteps = int(value/self.dt)
        value = float(value)
        self._set(f"{value} = {value/self.dt} timesteps", "Simulation parameters", "Duration of simulated process")

    @property
    def tframe(self) -> float:
        "duration of a frame"
        s = self._get(None, "Simulation parameters", "Frame duration")
        if self.version >= 2: return s
        elif s: return value_value_unit(s)[0]
        else: return np.nan
    @tframe.setter
    def tframe(self, value:float):
        if self.version >= 2:
            self._set(float(value), "Simulation parameters", "Frame duration")
        else:
            value = float(value)
            self._set(f"{value} = {value/self.dt} timesteps", "Simulation parameters", "Frame duration")
            self.tfirst = float(self.tfirst)

    @property
    def tsim(self) -> float:
        "amount of time used to simulate"
        if self.version >= 2:
            return self._get(None, "End of simulation", "Duration of simulation in seconds")
        else:
            return self._get(None, "Simulation time", "In seconds")
    @tsim.setter
    def tsim(self, value:float):
        if self.version >= 2:
            self._set(float(value), "End of simulation", "Duration of simulation in seconds")
        else:
            self._set(float(value), "Simulation time", "In seconds")

    @property
    def dt(self) -> float:
        "time step dt"
        return self._get(None, "Simulation parameters", "Timestep dt")
    @dt.setter
    def dt(self, value:float):
        self._set(float(value), "Simulation parameters", "Timestep dt")
        if self.version <= 1:
            self.tframe = float(self.tframe)
            self.tfirst = float(self.tfirst)
            self.tend = float(self.tend)

    @property
    def dzyx(self) -> Tuple[float,...]:
        "grid resolution (dz, dy, dx)"
        return tuple(fill_to_length_rev(self._get([None]*max(3,self.ndim), "Geometry parameters", "Voxel size"), max(3,self.ndim), 1.))
    @dzyx.setter
    def dzyx(self, values:Tuple[float,...]):
        assert(len(values) == max(3,self.ndim))
        self._set(tuple(reversed(values)), "Geometry parameters", "Voxel size")
    deltas_space = dzyx

    @property
    def deltas(self) -> Tuple[float,...]:
        "grid step size in the dimensions"
        return tuple([self.tframe, *self.dzyx])
    @deltas.setter
    def deltas(self, values:Tuple[float,...]):
        self.tframe = values[0]
        self.dzyx = values[1:]

    @property
    def origin(self) -> Tuple[float,...]:
        "position of the point with index zero in the saved variable fields"
        return tuple([self.tfirst, *self.origin_space])
    @origin.setter
    def origin(self, values:Tuple[float,...]):
        self.tfirst = values[0]
        self.origin_space = values[1:]

    @property
    def origin_space(self) -> Tuple[float,...]:
        "position of the point with index zero in the saved variable fields for a frame"
        return tuple(fill_to_length_rev(self._get([0]*max(3,self.ndim), "Geometry parameters", "Origin"), max(3,self.ndim), 0.))
    @origin_space.setter
    def origin_space(self, values:Tuple[float,...]):
        self._set(tuple(reversed(values)), "Geometry parameters", "Origin")

    @property
    def physical_size(self) -> Tuple[float,...]:
        "physical size of the domain"
        return tuple(dx*nx for dx, nx in zip(self.deltas, self.shape))

    @property
    def tiplinevars(self) -> Tuple[int,...]:
        "indices of the variables used for the tip tracking algorithm"
        return self.model.get("Tipline variables", [])

    @property
    def tiplineisovalues(self) -> Tuple[float,...]:
        "isovalues for the tip tracking algorithm"
        return self.model.get("Tipline isovalues", [])

    @property
    def diffusmain(self) -> Tuple[float,...]:
        "main diffusivities"
        return self.geometry.get("Main diffusivities", [])
    @diffusmain.setter
    def diffusmain(self, values:Tuple[float,...]):
        self.geometry["Main diffusivities"] = values

    @property
    def diffusmat(self) -> Tuple[float,...]:
        "diffusion matrix"
        key = "Diffusion matrix"
        if self.version <= 1: key += " [P]"
        return self.geometry.get(key, [])
    @diffusmat.setter
    def diffusmat(self, values:Tuple[float,...]):
        key = "Diffusion matrix"
        if self.version <= 1: key += " [P]"
        self.geometry[key] = values

    @property
    def username(self) -> str:
        "name of the user that ran the simulation"
        return self._get(None, "Simulation parameters", "Username")
    @username.setter
    def username(self, value:str):
        self._set(value, "Simulation parameters", "Username")

    @property
    def hostname(self) -> str:
        "name of the computer that ran the simulation"
        return self._get(None, "Simulation parameters", "Hostname")
    @hostname.setter
    def hostname(self, value:str):
        self._set(value, "Simulation parameters", "Hostname")
